import numpy as np
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.dates as md
import pylab as pl
import datetime

import opening_file
import playing_with_data_north
import selecting
import constants
import out_north
import adv_diff


import diffusivity_all_methods
import pretreatment
import KS_test


#///////////////////////////////////////////////////////////////////////////////#
#-------------------------------------------------------------------------------#
										#	
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# I] what area do you want to study ?   					#
										#
ocean = 'pacific'                        # atlantic or pacific			#
										#
lat_range = 30.				 					#
										#
long_e = -75.									#
										#
long_w = 120.									#
										#
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# II] download the corresponding file and give its path				#
										#
#path = '../Pacific-20+20_boundaries/interpolated_gld.20170424_065237'		#
path = '../interpolated_gld.20170705_040435' #
										#
										#
percent_lines = 100                      # put 100 if you want all the data	#

# PART 0.1: reading data or runing adv_diff scheme
option_reading_data = 0
option_adv_scheme_tests = 1
option_reading_drogued_data = 0

# PART 0.2
option_plotting_a_few_trajectories = 0
option_adv_diff_scheme = 0

# PART II
option_one_pool = 0
option_crossing_pool = 0
option_inverse_crossing_pool = 0
option_mean_eulerian_velocity = 1
option_eulerian_using_lag = 0						#
diffusivity_fc_lat_multiplot = 0						#
diffusivity_fc_mean_one_plot_per_group = 0

# PART III
option_velocity_pic =0
option_plots_adv_diff = 0

option_write_distrib_one_time = 0

# PART IV
option_trajectories = 0
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# III] Specify these parameters, depending which options you chose		#
										#
long_e_of_study = -110								#
long_w_of_study = -170								#
										#
lat_cross = 2.        				             			#						
t_min = 4*4	      								#
trajectories_length = 4*125
										#
										#
width = 0.1									#
step_lat = 0.25             							#
lat_limit_south = -20.                    					#
										#
#-------------------------------------------------------------------------------#
#///////////////////////////////////////////////////////////////////////////////#


nb_figure=1

lat_s = -lat_range
lat_n = lat_range

#y_axis = np.arange(lat_cross+0.05,8.5,0.1)


#----------------
#--------------_______________________________________________________________________________________________________________
#    PART 0    ---------------------------------------------------------------------------------------------------------------
#--------------

if option_reading_data ==1:
  size_data = opening_file.open_all( path )

  nb_lines_to_extract = int(percent_lines*size_data/100)

  print 'We are going to work with '+str(percent_lines)+'% of the data...'

  if option_reading_drogued_data ==1 and option_crossing_pool == 0:

    nb_positions, nb_drogued_drifters, velocity_drogued, date = opening_file.extract_velocity_drogued_drifters(  path, nb_lines_to_extract, ocean, long_w, lat_range, -lat_range,long_w_of_study,long_e_of_study )
    
    

    plt.figure(nb_figure)
    fig,ax = plt.subplots()    
    ax.plot(date,velocity_drogued[:,0],'.',markersize=5)
    
    years    = mdates.YearLocator()   # every year
    yearsFmt = mdates.DateFormatter('%y-%m-%d')
    
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.xaxis.set_major_locator(years)    
    
    #ax.set_xlim(datetime.date(1979, 1, 1), datetime.date(2016, 4, 1))
    
    ##ax.format_xdata = mdates.DateFormatter('%Y-%m-%d')
    
    #ax.grid(True)

    #xfmt = md.DateFormatter('%y-%m-%d')
    #ax.xaxis.set_major_formatter(xfmt)

    fig.autofmt_xdate()
    plt.savefig('test_hump.png')
    nb_figure = nb_figure+1


  else:
    size_data_extracted, nb_drifters, drifters_array, nb_data_per_drifters, t_max =                                        \
                                            opening_file.extract_subset(  path, nb_lines_to_extract, ocean, long_w)

    drifters_array = out_north.remove_blank( drifters_array, nb_drifters, nb_data_per_drifters, t_max)

    if ocean == 'pacific' and long_w > 0:
      long_w = -180-(180-long_w)
      if long_w_of_study > 0:
         long_w_of_study = -180-(180-long_w_of_study)


  




if option_adv_scheme_tests == 1:

   
   width = 0.1
   index_snapshot = 125 #0 (2.5days) 1 (37,5days) 2 (50) 3 (58) 4 (66) or 5 (125) 
   lat_cross0=np.array([-6,-2,-1,1.,2.,6.,7.,8.,9.,10.,11.,12.,13.,14.,15.])
   lat_cross05=np.array([0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,14.5])
   lat_cross1=np.array([-1.,-2.,-3.,-4.,-5.,-6.,-7.,-8.,-9.,-10.,-11.,-12.,-13.,-14.,-15.])
   lat_cross15=np.array([-0.5,-1.5,-2.5,-3.5,-4.5,-5.5,-6.5,-7.5,-8.5,-9.5,-10.5,-11.5,-12.5,-13.5,-14.5])
   
   nb_test=np.linspace(0,30,40)
   nb_diff_distrib = 30
   option05 = 0   
   for i in range(0,10):
      print nb_test[i]
      p0,nb_figure = KS_test.distrib_at_n(int(nb_test[i]),1.,index_snapshot, 0,0, lat_limit_south, width, nb_diff_distrib, option05,nb_figure)
   '''
   width=0.5
   for i in range(3,4):
     p0,nb_figure = adv_diff.distrib_at_n(lat_cross0[i], 1.,5, 0,0, lat_limit_south, width, nb_figure)
   '''

#-------------- _______________________________________________________________________________________________________________
# end of PART 0 ---------------------------------------------------------------------------------------------------------------
#--------------

#--------------_______________________________________________________________________________________________________________
#    PART II.  ---------------------------------------------------------------------------------------------------------------
#--------------

if option_plotting_a_few_trajectories == 1 :
  #/////////////////////////option_plots_adv_diff = 1
  nb_to_plot = 100	#//
  #////////////////////////

  traj_to_plot = np.zeros((nb_to_plot,t_max,2))
  title = 'Trajectories of the '+str(nb_to_plot)+' drifters'

  for i in range(0,nb_to_plot):
      traj_to_plot[i,:,:] = drifters_array[int(i*nb_drifters / (nb_to_plot+1)),:,:]

  nb_figure = out_north.plotting_traj(lat_s, lat_n, long_w, long_e, traj_to_plot[:,:,0], traj_to_plot[:,:,1], nb_to_plot, ocean, nb_figure, title)

  nb_figure= nb_figure + 1


#--------------
if option_adv_diff_scheme == 1:
  #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  # Advective_diffusive scheme -- 6 shnapshots..............
  width = 0.5
  p0, nb_figure = adv_diff.distrib_at_n(lat_cross, nb_figure)
  nb_points = int(-lat_limit_south/width)+1
  p0_reshape = np.zeros((6,nb_points))

  for i in range(0,nb_points):
      p0_reshape[:,i]=p0[:,i*50]*1E-3



#-------------- _______________________________________________________________________________________________________________
# end of PART I ---------------------------------------------------------------------------------------------------------------
#--------------



# - - - - - - - - - - - THE FIT - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
'''
kappa_fit = -2000*pl.tanh((y_axis-1.5)/0.5)+2000*pl.tanh((y_axis-2)/0.5)+8000*pl.tanh((y_axis-1)/0.2)-2000*pl.tanh((y_axis-5)/0.8)+9000*np.exp(-((y_axis-2.6)**2)/1.3)-2000
'''


#--------------_______________________________________________________________________________________________________________
#    PART II.  ---------------------------------------------------------------------------------------------------------------
#--------------

if option_crossing_pool == 1:
   
   cossing_pool_wrong_size, nb_cross = selecting.latitudes(drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study,option_reading_drogued_data,drifters_array[:,:,3]  )
   crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]


if option_inverse_crossing_pool == 1:
   
   cossing_pool_wrong_size, nb_cross = selecting.inverse_latitudes(drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study)
   crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]

   traj_to_plot = crossing_pool
   case = 'test_inverse'
   nb_to_plot = nb_cross
   title = 'Trajectories of drifters arriving at 1$^{o}$ after '+str(int(t_min*6/24))+' days'
   print nb_cross

   nb_data_per_drifters2 = np.zeros((nb_cross))
   nb_data_per_drifters2[:] = t_min
   nb_drifters2 = np.zeros((t_min))
   nb_drifters2[:] = nb_cross
   diffusivity = np.zeros((t_min-1,1))
   velocity = np.zeros((t_min-1,1))
   nb_drifter_mean = np.zeros((1))
   diffusivity[:,0], velocity[:,0], nb_drifter_mean[0],y_axis = diffusivity_all_methods.diffusivity_and_lagrangian_velocities(1.2, crossing_pool[:,:,0], crossing_pool[:,:,1], nb_cross, nb_data_per_drifters2, t_max, lat_cross, t_min, long_e_of_study,long_w_of_study, 'drifters_coming_from_north', 'fc_mean', step_lat )


   label =[]
   label.append('test inverse')
   nb_figure = out_north.one_plot(y_axis, diffusivity, 1, label, nb_drifters2, 'diffusivity', lat_cross, 'fc_mean', 'png', nb_figure )

   nb_figure = out_north.one_plot(y_axis, velocity, 1, label, nb_drifters2, 'velocity', lat_cross, 'fc_mean', 'png', nb_figure )



if option_one_pool == 1:

   drift_northward_pool, nb_n_pool = pretreatment.lagrangian_subset(drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, \
                                                             nb_data_per_drifters, t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study, 'same_group')



if option_mean_eulerian_velocity == 1:
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Mean eulerian velocities computed either with all the drifters in the pacific --> Eulerian_mean_velocity_ALL (m.s-1)

  #////////////////////////
  step_lat = 0.5	#//
  first_lat = lat_limit_south 	#//
  #////////////////////////

  nb_bin_of_means_whole_domain = int(abs(lat_limit_south)/step_lat)*2+1
  y_whole_domain = np.linspace(first_lat+step_lat/2, abs(lat_limit_south)-step_lat/2, nb_bin_of_means_whole_domain)
  Eulerian_mean_velocity_ALL = np.zeros((nb_bin_of_means_whole_domain,3))
  nb_drifter_per_band = np.zeros((nb_bin_of_means_whole_domain))

  
  a = 0
  lat = first_lat
  file = open('plots_velocity/eulerian_velocity_whole_range_drogued_drifters_05.txt','w') 
  while a < nb_bin_of_means_whole_domain :
     if option_reading_drogued_data ==1:
       Eulerian_mean_velocity_ALL[a,0],Eulerian_mean_velocity_ALL[a,1], nb_drifter_per_band[a]= playing_with_data_north.eulerian_velocity_drogued_drifters(velocity_drogued[:,0], velocity_drogued[:,1], lat, step_lat, nb_positions)
     else:
       Eulerian_mean_velocity_ALL[a,0],Eulerian_mean_velocity_ALL[a,1], nb_drifter_per_band[a] = playing_with_data_north.eulerian_velocity(drifters_array[:,:,0], drifters_array[:,:,1], drifters_array[:,:,2], \
                   lat, size_data_extracted, nb_drifters, nb_data_per_drifters,  step_lat, long_e_of_study, long_w_of_study,drifters_array[:,:,3] )
     file.write(str(Eulerian_mean_velocity_ALL[a,0])+'\n')
     lat = lat + step_lat
     a = a+1    
  file.close()
  
  plt.figure(nb_figure)
  plt.plot(y_whole_domain,nb_drifter_per_band)
  plt.savefig('plots_velocity/nb_drogued_drifters_per_band05.png')
  nb_figure = nb_figure+1
  
  mean_nb_data_per_band = np.mean(nb_drifter_per_band)
  '''
  txt_file = 'plots_velocity/eulerian_velocity_whole_range_drogued_drifters05.txt'
  with open(txt_file, "r") as f:
            tab = []
            for line in range(0,nb_bin_of_means_whole_domain):
                tab.append(f.readline().split())
  
  vel = np.array([x[0] for x in tab])
  Eulerian_mean_velocity_ALL[:,0]=np.array(vel)
  '''
  label = ['velocity', 'standard error', 'fit']
  
  # - - - - - - - - - - - THE FIT - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  M=int(abs(lat_limit_south)/step_lat)

  '''
  Eulerian_mean_velocity_ALL[0:M,2] = -(0.01*pl.tanh((-y_whole_domain[0:M]-0.3)/0.2)+0.02*np.exp(-((-y_whole_domain[0:M]-1)/4)**2)-0.02*np.tanh((-y_whole_domain[0:M]-13)/12) +0.023*pl.tanh((-y_whole_domain[0:M])/0.2) )

  Eulerian_mean_velocity_ALL[M+1:nb_bin_of_means_whole_domain-1,2] = (0.03*pl.tanh(y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]/0.5)+0.04*np.exp(-((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-3)/2.)**2)-0.001*np.exp(-((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]+1.5)/1.5)**3) )*1.1+ (y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-10)*(y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]+5)*(-1E-4)-0.006*pl.tanh((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-16.3)/1.5)-0.006 +0.003+0.003*pl.tanh((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-20)) +0.002+0.002*pl.tanh((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-22.5)) +0.08*pl.tanh((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-1.4)/0.2)- 0.08*pl.tanh((y_whole_domain[M+1:nb_bin_of_means_whole_domain-1]-1.4)/0.21)+0.003  
  '''
  Eulerian_mean_velocity_ALL[0:M,1] = -(0.01*pl.tanh((-y_whole_domain[0:M]-0.3)/0.2)+0.02*np.exp(-((-y_whole_domain[0:M]-1)/4)**2)-0.02*np.tanh((-y_whole_domain[0:M]-13)/12) +0.023*pl.tanh((-y_whole_domain[0:M])/0.2) )
 

  Eulerian_mean_velocity_ALL[M:nb_bin_of_means_whole_domain,1] = (0.03*pl.tanh(y_whole_domain[M:nb_bin_of_means_whole_domain]/0.5)+0.035*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]-3)/2.)**2)-0.001*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.2)- 0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.21) +0.011*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-9.2)/1.3) - 0.029*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-14.5)/3.3)-0.008

  nb_figure = out_north.one_plot(y_whole_domain, Eulerian_mean_velocity_ALL, 2, label, mean_nb_data_per_band, 'velocity', 'eulerian_whole_range_drogued', abs(lat_limit_south), 'png', nb_figure )


if option_eulerian_using_lag == 1:

  y_axis2 = np.arange(0,20 ,0.1)
  nb_points = len(y_axis2)
  mean_lagrangian_velocity = np.zeros((nb_points))
  diffusivity = np.zeros((nb_points))


  for i in range(0, nb_points):
    mean_lagrangian_velocity[i], diffusivity[i] = lagrangian_sutdy_in_narrow_band.f( drifters_array[:,:,0],      \
                                                   drifters_array[:,:,1], nb_drifters, nb_data_per_drifters,      \
                                                   t_max, y_axis2[i], 1, long_e_of_study, long_w_of_study )

  plt.figure(nb_figure)

  plt.plot(y_axis2,mean_lagrangian_velocity)
  plt.xlim([0,20])
  #plt.ylim([-0.3,0.4])
  plt.savefig('plots_velocity/lagrangian_eulerian_velocity.png')
  nb_figure=nb_figure+1



#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# idealized kappa function of latitude (based on kappa computed at every time and ploted as a function of the mean position
'''
kappa_fit = -2000*pl.tanh((y_axis-1.5)/0.5)+2000*pl.tanh((y_axis-2)/0.5)+8000*pl.tanh((y_axis-1)/0.2)-2000*pl.tanh((y_axis-5)/0.8)+9000*np.exp(-((y_axis-2.6)**2)/1.3)-2000

derivee_kappa_fit = -2000*( 1-(pl.tanh((y_axis-1.5)/0.5))**2 )/0.5 + 2000*( 1-(pl.tanh((y_axis-2)/0.5))**2 )/0.5 + 8000*(1-(pl.tanh((y_axis-1)/0.2))**2)/0.2 - 2000*( 1-(pl.tanh((y_axis-5)/0.8))**2 )/0.8   - 9000*np.exp(-((y_axis-2.6)**2)/1.3) *2*(y_axis-2.6)/1.3

'''


#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Adding Lagrangian velocities and derivative of the diffusivity --> eulerian velocities ?
'''
theoretical_eulerian_velocities = np.zeros((t_min-3))

for i in range(1, t_min-2):
   theoretical_eulerian_velocities[i-1] = mean_velocity_n[i] - (diffusivity[i+1]-diffusivity[i-1])/((mean_positions_n[i+1]-mean_positions_n[i-1])*constants.deg)

print mean_positions_n[1:20]

title='Mean velocity of the groups of drifters moving northward'
nb_figure = out_north.plotting_variable_fc_position( theoretical_eulerian_velocities, mean_positions_n[1:nb_of_time_step-2], lat_cross, nb_n_pool2, nb_figure, 'Theoretical_eulerian-velocity_'+str(lat_cross), title)
'''

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Substracting Eulerian velocity to Lagrangian velocity --> derivative of diffusivity ?
'''
Lag_minus_Eul = np.zeros((nb_bin_of_means))
int_Lag_minus_Eul = np.zeros((nb_bin_of_means))

lat = 8.5
a = nb_bin_of_means-1


while a > 0 :

     Lag_minus_Eul[a] = -playing_with_data_north.eulerian_velocity(drifters_array[:,:,0], drifters_array[:,:,1], \
                   lat, size_data_extracted, nb_drifters, nb_data_per_drifters,  step_lat, long_e_of_study, long_w_of_study) \
                   + lvelocity_per_bin[a]
     if a== nb_bin_of_means-1 :
      int_Lag_minus_Eul[a] = Lag_minus_Eul[a]*0.1*constants.deg
     else :
      int_Lag_minus_Eul[a] = int_Lag_minus_Eul[a+1]+ Lag_minus_Eul[a]*0.1*constants.deg
    
     lat = lat - step_lat
     a = a-1    

int_Lag_minus_Eul = 0.45*1E4 + int_Lag_minus_Eul
'''

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# other way to compute diffusivity. Method depends of a 'time-step'. Here I use dt=1,2,3,4 and 5 days
# this part is independant, you just need 'drifters_array'
'''
y_axis2 = np.arange(0,20 ,0.1)
nb_points = len(y_axis2)
mean_lagrangian_velocity = np.zeros((nb_points,5))
diffusivity = np.zeros((nb_points,5))

for j in range(1,2):
 for i in range(0, nb_points):
   mean_lagrangian_velocity[i,j-1], diffusivity[i,j-1] = lagrangian_sutdy_in_narrow_band.f( drifters_array[:,:,0],      \
                                                   drifters_array[:,:,1], nb_drifters, nb_data_per_drifters,      \
                                                   t_max, y_axis2[i], 4*j, long_e_of_study, long_w_of_study )

plt.figure(nb_figure)
for j in range(0,1):
 plt.plot(y_axis2,mean_lagrangian_velocity[:,j])
plt.xlim([0,20])
#plt.ylim([-0.3,0.4])
plt.savefig('plots_north/lagrangian_velocity_new_method_large_range.png')
nb_figure=nb_figure+1

plt.figure(nb_figure)
for j in range(0,1):
 plt.plot(y_axis2,diffusivity[:,j])
plt.xlim([0,20])
#plt.ylim([0,40000])
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.savefig('plots_north/diffusivity_with_lat_new_method_large_range.png')
nb_figure=nb_figure+1
'''

#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
'''
n=[10, 150, 200, 233, 266, 500]

t_index = np.zeros((6))

nb_bar = int(-lat_limit_south/width)
dy = (-lat_limit_south / nb_bar)*constants.deg
y=np.linspace( 0, -lat_limit_south, nb_bar+1 ) 
gaussian = np.zeros((t_min-1,nb_bar+1))
y_deriv = np.zeros((t_min-1,nb_bar+1))
time_derivative = np.zeros((t_min-1,nb_bar+1))

for i in range(0,1):
     #t_index[i] = int(n[i])
     nb_drifters_snapshot = int(nb_n_pool[i])-1
     gaussian[i,:], y_deriv[i,:], time_derivative[i,:] = playing_with_data_north.approx_gaussian(drift_northward_pool[:,i+1,0],drift_northward_pool[:,i+2,0], y, mean_positions_n[i],mean_positions_n[i+1], diffusivity[i],diffusivity[i+1], i+1 )

eulerian_velocity_fit = (0.03*pl.tanh(y/0.5)+0.05*np.exp(-((y-2.5)/2.5)**2)-0.001*np.exp(-((y+1.5)/1.5)**3) )*1.1+ (y-10)*(y+5)*(-1E-4)-0.006*pl.tanh(y-17)-0.006 +0.003+0.003*pl.tanh((y-20)) +0.002+0.002*pl.tanh((y-22.5))

plt.figure(nb_figure)
plt.plot(y,gaussian[0,:]*eulerian_velocity_fit)
plt.savefig('test_g*ve.png')
nb_figure=nb_figure+1

plt.figure(nb_figure)
plt.plot(y,time_derivative[0,:])
plt.savefig('test_time_deriv.png')
nb_figure=nb_figure+1

plt.figure(nb_figure)
plt.plot(y,y_deriv[0,:])
plt.savefig('test_y_deriv.png')
nb_figure=nb_figure+1
'''

'''
new_estimate_kappa =np.zeros((nb_bar+1,t_min-1))
integral_p =np.zeros((t_min-1,nb_bar+1))
zeros = 0
for n in range(0,t_min-1):
 for i in range(0,nb_bar):
    integral_p[n,i] = playing_with_data_north.integrate(time_derivative[n,:],dy,i,nb_bar)
    if y_deriv[n,i] == 0:
       new_estimate_kappa[i,n] = 0
       zeros = zeros+1
    else :
       new_estimate_kappa[i,n] = (eulerian_velocity_fit[i]*gaussian[n,i]-integral_p[n,i])/y_deriv[n,i]
       
print new_estimate_kappa[5,:]
new_estimate_kappa_av = np.mean(new_estimate_kappa,axis=1)
print np.shape(new_estimate_kappa_av)

subfig = 321

plt.figure(nb_figure)
plt.plot(y,new_estimate_kappa_av)
#plt.ylim([0,1E5])
plt.savefig('plots_north/new_estimate_kappa_av.png')
nb_figure=nb_figure+1

plt.figure(nb_figure)
for i in range(0,6):
    #t_index[i] = n[i]
    subfig = out_north.subplotting_pdf(gaussian[i*9,:],gaussian[i*9+1,:], y, -lat_limit_south,subfig) 
plt.savefig('plots_north/distrib_pdfs.png')

#diff_density
'''


# ************************************************************************************************
# ************************************************************************************************
# diffusivity and lagrangian velocity for different group of drifters, arrays are function of latitude
if diffusivity_fc_lat_multiplot == 1:
   #///////////////////////////////////////////////////////////////
   y_axis = np.arange(0+step_lat/2,10-step_lat/2,step_lat)	#//
								#//
   nb_bin_of_means = len(y_axis)				#//
   nb_pools = 5							#//
   lat_first_pool = 1.						#//
   extra_focus_on = 2.						#//
   #///////////////////////////////////////////////////////////////

   diffusivity = np.zeros((nb_bin_of_means,nb_pools))
   velocity = np.zeros((nb_bin_of_means,nb_pools))
   nb_drifter_mean = np.zeros((nb_pools))
   starting_lat = np.zeros((nb_pools))

   for i in range(0,nb_pools):
      starting_lat[i] = int(lat_first_pool+i*1)
      diffusivity[:,i], velocity[:,i],nb_drifter_mean[i],y_axis = diffusivity_all_methods.diffusivity_and_lagrangian_velocities(1.2, drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, nb_data_per_drifters, t_max, starting_lat[i], t_min, long_e_of_study,long_w_of_study, 'different_drifters_at_north', 'fc_lat', step_lat )


   nb_figure = out_north.one_plot(y_axis, diffusivity, nb_pools, starting_lat, nb_drifter_mean, 'diffusivity', 'met1', 'fc_lat', 'png', nb_figure )

   nb_figure = out_north.one_plot(y_axis, velocity, nb_pools, starting_lat, nb_drifter_mean, 'velocity', 'met1', 'fc_lat', 'png', nb_figure )

   if extra_focus_on >= lat_first_pool and extra_focus_on <= lat_first_pool+nb_pools:
     one_pool_starting_at = int(np.where(starting_lat == extra_focus_on)[0])

     nb_figure = out_north.one_plot(y_axis, diffusivity[:,one_pool_starting_at:one_pool_starting_at+1], 1, starting_lat[one_pool_starting_at:one_pool_starting_at+1], nb_drifter_mean[one_pool_starting_at:one_pool_starting_at+1], 'diffusivity', starting_lat[one_pool_starting_at], 'fc_lat', 'png', nb_figure )

     nb_figure = out_north.one_plot(y_axis, velocity[:,one_pool_starting_at:one_pool_starting_at+1], 1, starting_lat[one_pool_starting_at:one_pool_starting_at+1], nb_drifter_mean[one_pool_starting_at:one_pool_starting_at+1], 'velocity', starting_lat[one_pool_starting_at], 'fc_lat', 'png', nb_figure )




# ************************************************************************************************
# ************************************************************************************************
# computing diffusivity and velocity and plotting it as a function of the mea position of the group
# --> can't plot the results from the different groups on the same plot !
if diffusivity_fc_mean_one_plot_per_group == 1 :
  #////////////////////////////////////////////////
  nb_pools = 9					#//
  lat_first_pool = 1				#//
  #////////////////////////////////////////////////

  diffusivity = np.zeros((t_min-1,nb_pools))
  velocity = np.zeros((t_min-1,nb_pools))
  nb_drifter_mean = np.zeros((nb_pools))
  starting_lat = np.zeros((nb_pools))

  for i in range(0,nb_pools):
     starting_lat[i] = int(lat_first_pool+i*1)
     diffusivity[:,i], velocity[:,i],nb_drifter_mean[i],y_axis = diffusivity_all_methods.diffusivity_and_lagrangian_velocities(1.2, drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, nb_data_per_drifters, t_max, starting_lat[i], t_min, long_e_of_study,long_w_of_study, 'different_drifters_at_north', 'fc_time', step_lat )


     #one_pool_starting_at = int(np.where(starting_lat == 1.)[0])

     nb_figure = out_north.one_plot(y_axis, diffusivity[:,i], 1, starting_lat[i], nb_drifter_mean[i], 'diffusivity', starting_lat[i], 'fc_time', 'png', nb_figure )

  #nb_figure = out_north.one_plot(y_axis, velocity[:,one_pool_starting_at:one_pool_starting_at+1], 1, starting_lat[one_pool_starting_at:one_pool_starting_at+1], nb_drifter_mean[one_pool_starting_at:one_pool_starting_at+1], 'velocity', starting_lat[one_pool_starting_at], 'fc_mean', 'pdf', nb_figure )

#--------------  _______________________________________________________________________________________________________________
# end of PART II ---------------------------------------------------------------------------------------------------------------
#--------------

#--------------_______________________________________________________________________________________________________________
#    PART III  ---------------------------------------------------------------------------------------------------------------
#--------------

if option_velocity_pic == 1:
  
  width = 0.5
  nb_bar = 2*int(-lat_limit_south/width)
  x=np.linspace( lat_limit_south, -lat_limit_south, nb_bar+1 ) 
  
  fact = 30
  position_pic_n = np.zeros((t_min/fact-1,1))
  velocity_pic_n = np.zeros((t_min/fact-1,1))
  nb_drifter_fc_t = np.zeros((t_min/fact-1,1))
  nb_drifter_fc_t[:] = nb_cross
  label =[]
  label.append(lat_cross)

  position_pic_n[:,0], velocity_pic_n[:,0] = playing_with_data_north.velocity_pic(crossing_pool[:,:,0], nb_cross, x, lat_cross, nb_bar, t_min/fact, fact, width)
  print position_pic_n[0:10]
  nb_figure = out_north.one_plot(position_pic_n, velocity_pic_n, 1, label, nb_drifter_fc_t, 'velocity', 'track_pic', 'fc_mean', 'png', nb_figure )



if option_plots_adv_diff == 1 :
  n=[4*5, 4*15, 4*30, 4*50, 4*80, 4*125]
  subfig = 321
  width = 0.5 

  fig=plt.figure(nb_figure)
  fig.suptitle('$y_{0}=$'+str(lat_cross)+'$^{o}$ \n distribution at different times ('+str(np.mean(nb_n_pool))+' drifters in average)',    \
                   fontsize=16)

  t_index = np.zeros((6))
  nb_drifter_snapshot = np.zeros((6))
  
  # test a effectuer : drift_northward_pool[:,int(t_index[i]),0] et enlever le -1 de la boucle
  for i in range(0,6):
     t_index[i] = int(n[i])
     nb_drifter_snapshot[i] = int(nb_n_pool[int(t_index[i])])-1
     subfig = out_north.subplotting_distrib_norm(drift_northward_pool[1:,int(t_index[i]),0], p0_reshape[i,:], 0,0, lat_cross, int(nb_drifter_snapshot[i]), int(t_index[i]), -lat_limit_south, width, subfig)
 
  plt.subplots_adjust(top=0.85, bottom=0.1, left=0.10, right=0.95, hspace=0.5, wspace=0.35)  

  plt.savefig('plots_adv_diff/distrib_'+str(lat_cross)+'.png')

  nb_figure = nb_figure+1




if option_write_distrib_one_time == 1 and option_6_distrib_plots==0:
   index_t = 3
   t = index_t*6/24
   width = 0.5
   nb_drifter_snapshot = int(nb_n_pool[index_t])
   nb_bar = int(-lat_limit_south/width)
   x=np.linspace( 0, -lat_limit_south, nb_bar+1 )        
   height = np.zeros((nb_bar+1))
        
   for i in range(0,nb_drifter_snapshot):
     index = np.where(np.logical_and(drift_northward_pool[i,index_t,0]>=x, drift_northward_pool[i,index_t,0]<x+width))
     height[index]=height[index]+1
	 
   height=height/(nb_drifter_snapshot*width*constants.deg)


   file = open('plots_adv_diff/real_distribution_at_'+str(int(t))+'_days_starting_at_'+str(lat_cross)+'.txt','w') 
   
   for i in range(0,len(height)):
     file.write(str(height[i])+'\n') 

 
   file.close() 


if option_trajectories == 1:

  nb_figure = out_north.plotting_traj(lat_s, lat_n, long_w, long_e, traj_to_plot[:,:,0], traj_to_plot[:,:,1], nb_to_plot, case, nb_figure, title)

  nb_figure= nb_figure + 1





