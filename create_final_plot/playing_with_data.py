import numpy as np

import constants

#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx
  yo = np.sum(v)

  return yo

#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~ 

def seperate_drifters_at_eq( cross_drift, nb_cross, t_min,lat_cross ):

   same_hemisphere_drifters=np.zeros((nb_cross,t_min,3))

   nb_drifters_at_t = np.zeros(( t_min,1 ))

 
   a=0

   if lat_cross >0:
    for i in range(0,t_min):
       for j in range(0, nb_cross):
           if cross_drift[j,i,0] > 0:
              same_hemisphere_drifters[a,i,:] = cross_drift[j,i,:]
              nb_drifters_at_t[i,0]=nb_drifters_at_t[i,0]+1
              a=a+1
       a=0
   elif lat_cross<0:
    for i in range(0,t_min):
       for j in range(0, nb_cross):
           if cross_drift[j,i,0] < 0:
              same_hemisphere_drifters[a,i,:] = cross_drift[j,i,:]
              nb_drifters_at_t[i,0]=nb_drifters_at_t[i,0]+1
              a=a+1
       a=0


   return same_hemisphere_drifters, nb_drifters_at_t




#----------------------------------------------------------------------------


def lagrangian_mean(Lat, velocity, nb_drifters, t_min):

   mean_velocity = np.zeros((t_min))
   mean_position = np.zeros((t_min))
   nb_correct_values = 0   

   for i in range(0,t_min):

      mean_position[i]=np.mean(Lat[:,i])

      for n in range(0,nb_drifters):
        if velocity[n,i] != np.nan and velocity[n,i]<900:
          mean_velocity[i]=mean_velocity[i]+velocity[n,i]
          nb_correct_values = nb_correct_values +1 
      mean_velocity[i] = mean_velocity[i]/nb_correct_values
      nb_correct_values = 0

   return mean_velocity, mean_position


def lagrangian_mean_one_side(Lat, velocity, nb_drifters_at_t, t_min):

   mean_velocity = np.zeros((t_min))
   mean_position = np.zeros((t_min))
   nb_correct_values = 0   

   for i in range(0,t_min):

      mean_position[i]=np.mean(Lat[0:nb_drifters_at_t[i]-1,i])

      for n in range(0,nb_drifters_at_t[i]):
        if velocity[n,i] != np.nan and velocity[n,i]<900:
          mean_velocity[i]=mean_velocity[i]+velocity[n,i]
          nb_correct_values = nb_correct_values +1 
      mean_velocity[i] = mean_velocity[i]/nb_correct_values
      nb_correct_values = 0

   return mean_velocity, mean_position



#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def eulerian_velocity_drogued_drifters(Lat, velocity, lat_studied,step, nb_data,date):

  buyo_days=[]
  if lat_studied < 0:
     factor = -1
  else :
     factor = 1

  MEAN_VELOCITY = 0
  MEAN_OF_SQRD = 0
  nb_drifters_band = 0
  val = 0
  for n in range(0,nb_data):
        if Lat[n]>lat_studied and Lat[n]<lat_studied+step :
           if velocity[n] != np.nan and velocity[n]<900:
             MEAN_OF_SQRD = MEAN_OF_SQRD + (1E-2*velocity[n])**2
             MEAN_VELOCITY = MEAN_VELOCITY + velocity[n]*1E-2
             nb_drifters_band=nb_drifters_band+1
             buyo_days.append(date[n])
           else :
             val=val+1
  
  if nb_drifters_band == 0 :
     MEAN_VELOCITY = np.nan
     MEAN_OF_SQRD = np.nan
  else :
     MEAN_VELOCITY = MEAN_VELOCITY/nb_drifters_band
     MEAN_OF_SQRD = MEAN_OF_SQRD/nb_drifters_band
  print len(buyo_days), len(list(set(buyo_days)))

  nb_drifters_band = len(list(set(buyo_days)))
  ste = factor * 1.96*np.sqrt(MEAN_OF_SQRD-MEAN_VELOCITY**2) /np.sqrt(nb_drifters_band/20)
  
  return MEAN_VELOCITY, ste+MEAN_VELOCITY,MEAN_VELOCITY-ste, len(list(set(buyo_days))) #nb_drifters_band


def eddy_kine(mean, Lat, velocity, lat_studied,step, nb_data,date):

  nb_drifters_band = 0
  val = 0
  MEAN_OF_SQRD = 0
  for n in range(0,nb_data):
        if Lat[n]>lat_studied and Lat[n]<lat_studied+step :
           if velocity[n] != np.nan and velocity[n]<900:
             MEAN_OF_SQRD = MEAN_OF_SQRD + (1E-2*velocity[n]-mean)**2
             nb_drifters_band = nb_drifters_band+1
           else :
             val=val+1
  
  if nb_drifters_band == 0 :
     MEAN_VELOCITY = np.nan
     MEAN_OF_SQRD = np.nan
  else :
     MEAN_OF_SQRD = MEAN_OF_SQRD/(2*nb_drifters_band)

  
  return MEAN_OF_SQRD


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def mean_drifters_in_bins(Lat,nb_drifter,index_lat_max, width):

        nb_bar = int(index_lat_max/width)
        x=np.linspace( 0, index_lat_max, nb_bar+1 )        
        bins=np.zeros((nb_bar+1))
        
        
        for i in range(0,nb_drifter):
           index = np.where(np.logical_and(Lat[i]>=x, Lat[i]<x+width))
           bins[index]=bins[index]+1

        mean = np.sum(bins*x)/(nb_drifter-1)

        return mean



