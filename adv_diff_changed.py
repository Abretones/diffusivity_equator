import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')

from mpl_toolkits.basemap import Basemap
from matplotlib.ticker import NullFormatter,MultipleLocator, FormatStrFormatter
import matplotlib.gridspec as gridspec
import scipy 
import pylab as pl
from netCDF4 import Dataset
from scipy.interpolate import Rbf
import sys
import os
from matplotlib.lines import Line2D
import constants
#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx;
  yo = np.sum(v);

  return yo


#____________________________________________________________________
def adrhs(p,v,k,y,dy):
#
# RHS of the 1D Adv-Diff equation for absolute dispersion
#
  gp=np.gradient(p,dy);
  y=-np.gradient(v*p,dy) + np.gradient(k*gp,dy);

  return y





#____________________________________________________________________
def distribution_6_different_times(y, dy, ys, py, nb_points, nb_time, dt, factor, n_index, v, kappa):

 p0 = np.zeros((nb_points))

 p0 = (1/(np.sqrt(2*np.pi)*py))*np.exp(-(((y-ys)/py)**2)/2) #initial PDF

 

 #//////////////////
 #//////////////////

 Nw=1000
 N = 251000
 p0_saved = np.zeros((N/Nw,nb_points))
 cnt=0
 
 for j in range(0,N):
   k1 = adrhs(p0,v,kappa,y,dy)
   k2 = adrhs(p0+k1*dt/2,v,kappa,y,dy)
   k3 = adrhs(p0+k2*dt/2,v,kappa,y,dy)
   k4 = adrhs(p0+k3*dt,v,kappa,y,dy)
   p0=p0+dt/6*(k1+2*k2+2*k3+k4)   
  
   if cnt*Nw==j:
     p0_saved[cnt,:] = np.copy(p0)#/trap(p2,dy)
     cnt=cnt+1
   
 return p0_saved, my_n[1:], v_n





#____________________________________________________________________
def subplotting_distrib(y, p0, height, width, time,nb_figure):

        plt.subplot(nb_figure)
        plt.plot(y,p0/trap(p0,1/200))
        plt.bar(y, height/trap(height,width), width, color='c',edgecolor='c')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.title('t='+str(time)+' days',fontsize=10)
        pl.xlim([-20,20])


        nb_figure=nb_figure+1
        return nb_figure



#____________________________________________________________________
def figure_subplots( p0, height, x, width, index_t, dt, starting_lat, nb_figure):

   fig=plt.figure(figsize=(3,9))
   subfig=321


   for i in range(0,6):    
    subfig=subplotting_distrib(x,p0[:,i],height[:,i], width, index_t[i]*dt, subfig)

   plt.subplots_adjust(top=0.85, bottom=0.1, left=0.10, right=0.95, hspace=0.5, wspace=0.35)

   plt.savefig('plots_adv_diff/distrib_scheme_'+str(starting_lat)+'.png')

   nb_figure=nb_figure+1 
   return nb_figure






#//////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////////////
#____________________________________________________________________
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

def distrib_at_n(starting_lat, nb_diff_confi, index_snapshot, option_testing_different_diff, option_superimpose_real_distrib, lat_limit_south, width, nb_figure):

 deg=110.567

 dy = 1./200        # grid spacing, in degres
 y_max = 20.
 y  = np.linspace(-y_max, y_max, 2*y_max/dy+1)
 print len(y)
 dy = dy*deg
 y_max = y_max*deg
 y = y*deg
 M = len(y)

 t0 = 0 					# starting time
 dt = 0.0005				# time step in day
 factor = 250 #150/(600*dt)
 N = 251000		# total integration (N*dt)
 

 
 #////////////////////////////////////////////////////////////////////////////////////////////
 # #-------------------------------------------------------------------------------------------
 v = np.zeros((M))


 #Eulerian velocity


 v=0.02*np.tanh(y/50)+0.08*np.exp(-((y-300)/400)**2)-0.08*np.exp(-((y+300)/400)**2)

 #v[0:int(M/2)] = -(0.01*pl.tanh((-y[0:int(M/2)]/deg-0.3)/0.2)+0.02*np.exp(-((-y[0:int(M/2)]/deg-1)/4)**2)-0.02*np.tanh((-y[0:int(M/2)]/deg-13)/12) +0.023*pl.tanh((-y[0:int(M/2)]/deg)/0.2) )
 

 #v[int(M/2)+1:M-1] = (0.03*pl.tanh(y[int(M/2)+1:M-1]/(deg*0.5))+0.04*np.exp(-((y[int(M/2)+1:M-1]/deg-3)/2.)**2)-0.001*np.exp(-((y[int(M/2)+1:M-1]/deg+1.5)/1.5)**3) )*1.1+ (y[int(M/2)+1:M-1]/deg-10)*(y[int(M/2)+1:M-1]/deg+5)*(-1E-4)+0.08*pl.tanh((y[int(M/2)+1:M-1]/deg-1.4)/0.2)- 0.08*pl.tanh((y[int(M/2)+1:M-1]/deg-1.4)/0.21)+0.003  #- 0.04*pl.tanh((y[int(M/2)+1:M-1]/deg-1)/1.6)+0.04

 plt.figure(nb_figure)
 pl.xlim([-y_max/deg, y_max/deg])

 plt.plot(y/deg,v)
 units = 'm.s$^{-1}$'
 plt.ylim([-0.15,0.15])
 plt.ylabel('velocity ('+str(units)+')')
 plt.xlabel('latitude')
 plt.savefig('plots_adv_diff/Eulerian_velocity.png')
 nb_figure=nb_figure+1
 
 v=v*86.4 #km/day


 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 kappa = np.zeros((M,4))
 label = []
   
 nb_confi = 0

 '''
 std = 2.5
 moy = 4.25

 kappa[0:int(M/2),nb_confi] = 0.4*1E4*(1/(std*np.sqrt(2*np.pi)))*np.exp(-((-y[0:int(M/2)]/deg-moy-0.25)/std)**2/2) - pl.tanh((-y[0:int(M/2)]/deg-14)/4)*0.5*1E3 + 1.9*1E3 
 kappa[int(M/2):M-1,nb_confi] = 0.7*1E4*(1/(std*np.sqrt(2*np.pi)))*np.exp(-((y[int(M/2):M-1]/deg-moy)/std)**2/2)  - pl.tanh((y[int(M/2):M-1]/deg-14)/4)*0.5*1E3  + 1.8*1E3 
 # 1.5*1E3, 1.4
 '''

 kappa[:,nb_confi] = 4*1E3 *( np.exp(-((y+400)/200)**2) + np.exp(-((y-400)/200)**2)) + 5000


 label.append('maximum at 3degres')
 
 nb_confi = nb_confi +1
 

 kappa = kappa*0.0864
 

 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 
 ys = starting_lat*deg                    # positon of initial PDF (degres)
 py = 0.2*deg				  # width of initial PDF

 n_index=[30*factor, 80*factor, 150*factor, 200*factor, 350*factor, 500*factor]
 nb_points = int(-lat_limit_south/width)+1


   
 if option_testing_different_diff == 0:
    
    nb_bar = 2*int(-lat_limit_south/width)
    xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_bar+1 )

    height_float = np.zeros((nb_bar+1,6))

    for i in range(0,6):
        txt_file = 'plots_distrib/real_distribution_at_'+str(int(n_index[i]*6/(24*factor)))+'_days_starting_at_'+str(starting_lat)+'.txt'
        with open(txt_file, "r") as f:
            tab = []
            for line in range(0,nb_bar+1):
                tab.append(f.readline().split())

        height = np.array([x[0] for x in tab])
        height_float[:,i] = np.zeros((nb_bar+1))
        for j in range(0,nb_bar+1):
              height_float[j,i] = str(height[j])
  

    plt.figure(nb_figure)
    pl.xlim([-y_max/deg, y_max/deg])
    plt.plot(y/deg,kappa/0.0864)
    units = 'm.s$^{-1}$'
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ylabel('diffusivity ('+str(units)+')')
    plt.xlabel('latitude')
    plt.savefig('plots_adv_diff/diffusivity_whole_range.png')
    nb_figure=nb_figure+1
    
    my_n = np.zeros((N/factor-1,1)) 
    v_n = np.zeros((N/factor-1,1)) 
    
    Nw = 1000
    p0_saved = np.zeros((N/Nw,M,1))
    
    p0_saved[:,:,0], my_n[:,0], v_n[:,0] = distribution_6_different_times(y, dy, ys, py, M, N, dt, factor, n_index, v, kappa[:,0])
    
    p0_reshape = np.zeros((nb_bar+1, 6 ))
    
    for j in range(0,6):
       for i in range(0,nb_bar+1):
         p0_reshape[i,j]=p0_saved[j,i*100]
    
    nb_figure = figure_subplots(p0_reshape, height_float, xaxis[:], width, n_index, dt, starting_lat, nb_figure)
    '''
    nb_figure=nb_figure+1
    plt.figure(nb_figure)
    plt.plot(my_n[:,0]/(deg*1E3),v_n[:,0])
    plt.savefig('plots_adv_diff/lagrangian'+str(starting_lat)+'.png')
    nb_figure = nb_figure +1
    # TEMPORARY
    '''
    
    txt_file = 'plots_adv_diff/real_distribution_at_150_days_'+str(starting_lat)+'.txt'
    with open(txt_file, "r") as f:
       tab = []
       for line in range(0,M):
                tab.append(f.readline().split())

       prov = np.array([x[0] for x in tab])
       for j in range(0,M):
              p0_saved[249,j,0] = str(prov[j])
    '''
    file = open('plots_adv_diff/real_distribution_at_150_days_'+str(starting_lat)+'.txt','w') 
   
    for i in range(0,M):
         file.write(str(p0_saved[249,i,0])+'\n') 

    file.close()
    '''
    P_joe = height_float[:,5]
    
    xaxis2 = np.linspace( lat_limit_south, -lat_limit_south, nb_bar/2+1 )
    P_joe2 = np.zeros((nb_bar/2+1))
    for i in range(0,nb_bar/2):
        P_joe2[i] = (P_joe[2*i]+P_joe[2*i+1])/2 
    P_joe2 = P_joe2/trap(P_joe2,1)
    dim_array = len(p0_saved[:,0,0])
    print dim_array

   
 
    fig = plt.figure(figsize=(6.5,6))

    ax = fig.add_axes([0.1,0.05,0.85,0.9])
    #ax = fig.add_axes([0.05,0.05,0.9,0.8])

    ax.plot(xaxis2,P_joe2,y/deg,p0_saved[249,:,0]/trap(p0_saved[249,:,0],dy/deg) )
    fig.savefig('plots_adv_diff/distrib125.png')


    

    
   
 return p0_saved[:,:,0]*1E-3, nb_figure
