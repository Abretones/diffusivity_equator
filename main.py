import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab as pl



import opening_file
import lagrangian
import looking_latitudes
import playing_with_data
import out
import constants
import variance_based


#///////////////////////////////////////////////////////////////////////////////#
#-------------------------------------------------------------------------------#
										#	
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# I] what area do you want to study ?   					#
										#
ocean = 'pacific'                        # atlantic or pacific			#
										#
lat_range = 30.				 					#
										#
long_e = -75.									#
										#
long_w = 120.									#
										#
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# II] download the corresponding file and give its path				#
										#
#path = '../Pacific-20+20_boundaries/interpolated_gld.20170424_065237'		#
path = '../Pacific-30+30_boundaries/interpolated_gld.20170424_094013'          #
#path = '../interpolated_gld.20170505_095831'					#
										#
percent_lines = 100                       # put 100 if you want all the data	#
										#
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# III] what do you want to look at ? See the Readme for explanations 		#
										#
option_all_traj = 0		     						#
option_E_velocity_all_drifters = 0 						#
										#
option_diff_each_lat = 0							#
option_group_drifters_each_lat = 0 						#
										#
option_focus_one_lat = 1							#
option_stat = 0								#
option_distrib_all = 0								#
										#
option_distrib_north_side = 0							#
										#
option_E_velocity_both_side = 1							#
option_L_velocity_one_side = 1							#

option_integration_dif_velocities = 0
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# IV] Specify these parameters, depending which options you chose		#
										#
long_e_of_study = -110								#
long_w_of_study = -170								#
										#
lat_cross = 0.        #option_focus_one_lat              			#						
t_min = 600	      #option_focus_one_lat|					#

t_asymptote = 500
										#
t_away_from_eq = 550								#
t_snapshot = 300								#
kappa_guess = 6*1E4								#
										#
width = 0.05
step_lat = 0.1              #option_E_velocity					#
lat_limit_south = -10.                     #option_E_velocity			#
										#
#-------------------------------------------------------------------------------#
#///////////////////////////////////////////////////////////////////////////////#



nb_figure=1

lat_s = -lat_range
lat_n = lat_range




opening_file.checking_options( option_all_traj, option_E_velocity_all_drifters, option_focus_one_lat, option_stat,     \
                               option_distrib_all, option_distrib_north_side, option_L_velocity_one_side,              \
                               option_E_velocity_both_side, ocean, lat_range )


size_data = opening_file.open_all( path )

nb_lines_to_extract = int(percent_lines*size_data/100)

print 'We are going to work with '+str(percent_lines)+'% of the data...'

size_data_extracted, nb_drifters, drifters_array, nb_data_per_drifters, nb_max =                                        \
                                            opening_file.extract_subset(  path, nb_lines_to_extract, ocean, long_w)




if ocean == 'pacific' and long_w > 0:
      long_w = -180-(180-long_w)
      if long_w_of_study > 0:
         long_w_of_study = -180-(180-long_w_of_study)


#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

if option_E_velocity_all_drifters == 1:

   nb_bin_of_means = 2*abs(lat_limit_south)/step_lat+1
   mean_drift=np.zeros((nb_bin_of_means))
   lat = lat_limit_south
   a=0

   while a < nb_bin_of_means :
     mean_drift[a] = playing_with_data.Lat_band( drifters_array[:,:,0], drifters_array[:,:,1], size_data_extracted, nb_drifters,   \
						 nb_data_per_drifters, lat, step_lat, long_e_of_study, long_w_of_study )
     lat = lat + step_lat
     a = a+1     

   title = 'Mean eulerian velocity in the region '+str(long_w_of_study)+','+str(long_e_of_study)+'$^{o}$E \n (mean '     \
           'computed into band of '+str(step_lat)+'$^{o}$ width of latitude)'			 
            
   nb_figure = nb_figure+1  # again, why does this bug happen ?
   nb_figure = out.plotting_variable_fc_lat(mean_drift*1E2, step_lat, nb_figure, title, lat_limit_south, 'Mean_velocity_all')
  
   file = open('plots/mean_eulerian_velocities.txt','w') 
   
   for i in range(0,int(nb_bin_of_means)):
     print mean_drift[i]
     file.write(str(mean_drift[i])+'\n') 

 
   file.close() 


#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~ 

if option_all_traj == 1:
   drifters_array = out.remove_blank( drifters_array, nb_drifters, nb_data_per_drifters, nb_max)
   title = 'Trajectories of the '+str(nb_drifters)+' drifters'

   
    
   nb_figure = out.plotting_traj( lat_s, lat_n, long_w, long_e, drifters_array[:,:,0], drifters_array[:,:,1], 		  \
  				  nb_drifters, ocean,'all', nb_figure, title )


#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

if option_group_drifters_each_lat == 1:

   lat_group=[0.]
   nb_group = len(lat_group)

   mean_velocity_n = np.zeros((t_min-1, nb_group))
   mean_positions_n = np.zeros((t_min, nb_group))
   mean_velocity_s = np.zeros((t_min-1, nb_group))
   mean_positions_s = np.zeros((t_min, nb_group))
   
   for i in range(0, nb_group):
       nb_n_pool, mean_velocity_n[:,i], mean_positions_n[:,i], nb_s_pool, mean_velocity_s[:,i], mean_positions_s[:,i], nb_figure = lagrangian.groupe_starting_at_the_same_lat( drifters_array, nb_drifters, nb_data_per_drifters, nb_max, lat_group[i], t_min, t_away_from_eq, long_e_of_study, long_w_of_study, lat_range, width, nb_figure)

   nb_figure = out.plotting_velocity_fc_position( mean_velocity_n*1E2, mean_positions_n[1:,:], nb_n_pool, nb_figure, t_min, t_away_from_eq, 'north' )
   nb_figure = out.plotting_velocity_fc_position( mean_velocity_s*1E2, mean_positions_s[1:,:], nb_s_pool, nb_figure, t_min, t_away_from_eq, 'south' )

#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  


if option_focus_one_lat == 1:


   cross_drift_wrong_size, nb_cross = looking_latitudes.crossing_drifters(drifters_array[:,:,0], drifters_array[:,:,1], \
                                                                           nb_drifters, nb_data_per_drifters,            \
                                                                           nb_max, lat_cross, t_min, long_e_of_study,    \
   				                                           long_w_of_study)
   cross_drift = cross_drift_wrong_size[0:nb_cross, :, :]                                     
   title = 'Trajectories of the '+str(nb_cross)+' drifters that crossed the line of latitude '				 \
            +str(lat_cross)+'$^{o}$N in the range of longitude: '+str(long_w_of_study)+','+str(long_e_of_study)+'$^{o}$' \
            '\n('+str(int(t_min*6/24))+' days trajectories)'

   print np.shape(cross_drift)
   #nb_figure = out.plotting_traj( lat_s, lat_n, long_w, long_e, cross_drift[:,:,0], cross_drift[:,:,1], nb_cross, ocean,\
   #                                'crossing_'+str(int(lat_cross*10)), nb_figure, title ) 

    
   nb_figure = nb_figure+1       #why do I need to do that ??


   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
   # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  


   if option_stat == 1:

     print 'we are going to compute the diffusivity at '+str(lat_cross)+' degres thanks to ', nb_cross,                    \
           ' drifters that crossed this latitude and stayed in our domain for around ',int(t_min*6/24),' days'

     drift_north_pool, nb_north, drift_south_pool, nb_south = playing_with_data.seperate_drifters_at_eq( cross_drift,  \
                                                                                       nb_cross, t_min ) 
     var_each_time = np.zeros((t_min))
     diff_each_time = np.zeros((t_min))

     if lat_cross == 0.:
        var_each_time_n = np.zeros((t_min))
        var_each_time_s = np.zeros((t_min))

        for i in range(1,t_min):
          var_each_time_n[i] = playing_with_data.variance( drift_north_pool[0:nb_north[i],i,0], drift_north_pool[0:nb_north[i],0,0], int(nb_north[i]) )
          var_each_time_s[i] = playing_with_data.variance( drift_south_pool[0:nb_south[i],i,0], drift_south_pool[0:nb_south[i],0,0], int(nb_south[i]) )
          var_each_time[i] = (var_each_time_n[i] + var_each_time_s[i])/2
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)

     elif lat_cross > 0.:

        for i in range(1,t_min):
          var_each_time[i] = playing_with_data.variance( drift_north_pool[0:nb_north[i],i,0], drift_north_pool[0:nb_north[i],0,0], int(nb_north[i]) )
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)
    
     elif lat_cross < 0.:

        for i in range(1,t_min):
          var_each_time[i] = playing_with_data.variance( drift_south_pool[0:nb_south[i],i,0], drift_south_pool[0:nb_south[i],0,0], int(nb_south[i]) )
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)


     nb_figure = out.plot_fc_time(var_each_time, lat_cross, t_min, nb_cross, nb_figure, ocean, 'Variance')
     nb_figure = out.plot_fc_time(diff_each_time, lat_cross, t_min, nb_cross, nb_figure, ocean, 'Diffusivity')
     #nb_figure = out.plot_fc_time(skew_each_time, lat_cross, t_min, nb_cross, nb_figure, ocean, 'Skewness')


   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
   # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

   if option_distrib_all == 1:

      n=[10, 150, 200, 233, 266, 500]
      subfig=321

      fig=plt.figure(nb_figure)

      fig.suptitle('$y_{0}=$'+str(lat_cross)+'$^{o}$ \n distribution at different times ('+str(nb_cross)+' drifters)',    \
                   fontsize=16)

      for i in range(0,6):
            subfig=out.subplotting_distrib(cross_drift[:,:,0], nb_cross, lat_cross, n[i], lat_range, 0.5, subfig)
 
      plt.subplots_adjust(top=0.85, bottom=0.1, left=0.10, right=0.95, hspace=0.5, wspace=0.35)  

      plt.savefig('plots/distrib_'+str(lat_cross)+'.png')

      nb_figure=nb_figure+1

   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
   # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

   # Eulerian velocity for this subset
   if option_E_velocity_both_side == 1:
      print 'je suis la'
      nb_bin_of_means = 2*abs(lat_limit_south)/step_lat+1
      mean_drift_subset=np.zeros((nb_bin_of_means))
      lat = lat_limit_south
      a=0
      nb_data_per_drifters_subset=np.zeros((nb_cross))
      nb_data_per_drifters_subset[:]=t_min
    
      while a < nb_bin_of_means :
        mean_drift_subset[a] = playing_with_data.Lat_band( cross_drift[:,:,0], cross_drift[:,:,1], t_min*nb_cross, nb_cross,       \
	 					 nb_data_per_drifters_subset, lat, step_lat, long_e_of_study, long_w_of_study )
        lat = lat + step_lat
        a = a+1     

      title = 'Mean eulerian velocity in the region '+str(long_w_of_study)+','+str(long_e_of_study)+'$^{o}$E \n (mean '     \
            'computed using only the '+str(nb_cross)+' drifters crossing the equator'
   
      nb_figure = out.plotting_variable_fc_lat(mean_drift_subset*1E2, step_lat, nb_figure, title, lat_limit_south, 'Mean_velocity_subset_crossing_eq')

   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
   # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
   #  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

   if option_L_velocity_one_side == 1:
     #if lat_cross !=0.:
     #   print 'option_L_velocity only work at the equator for now...'
     #   quit()
 
     drift_northward_pool, nb_n_pool, drift_southward_pool, nb_s_pool = playing_with_data.seperate_drifters( cross_drift,  \
                                                                                       nb_cross, t_min, t_away_from_eq )
     mean_velocity_n = np.zeros((t_min-1, 1))
     mean_positions_n = np.zeros((t_min, 1))
     mean_velocity_s = np.zeros((t_min-1, 1))
     mean_positions_s = np.zeros((t_min, 1))

     nb_n_pool_array = np.zeros((t_min-1))
     nb_n_pool_array[:] = nb_n_pool
     nb_s_pool_array = np.zeros((t_min-1))
     nb_s_pool_array[:] = nb_s_pool
     

     mean_velocity_n[:,0], mean_positions_n[:,0] = playing_with_data.mean_one_size(drift_northward_pool, nb_n_pool_array,  t_min)
     mean_velocity_s[:,0], mean_positions_s[:,0] = playing_with_data.mean_one_size(drift_southward_pool, nb_s_pool_array, t_min)
    
   
     nb_figure = out.plotting_velocity_fc_position( mean_velocity_n*1E2, mean_positions_n[1:], nb_n_pool, nb_figure, t_min, t_away_from_eq, 'north' )
   
     nb_figure = out.plotting_velocity_fc_position( mean_velocity_s*1E2, mean_positions_s[1:], nb_s_pool, nb_figure, t_min, t_away_from_eq, 'south' )
     

     # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  
     if option_distrib_north_side == 1:
        print nb_n_pool, np.shape(drift_northward_pool[:,:,0])
        out.plotting_distrib_norm(drift_northward_pool[:,:,0],nb_n_pool,lat_cross,t_snapshot,kappa_guess,nb_figure)
   

   if option_E_velocity_both_side == 1 and option_group_drifters_each_lat == 1 and option_integration_dif_velocities ==1:
     mean_velocity_n = mean_velocity_n[mean_positions_n.argsort()]
     mean_positions_n = np.sort(mean_positions_n)

     diff_velocity_each_lat = np.zeros((10/step_lat +1))
     lat_limit_n = step_lat
     index_diff = 0
     sum_velocities = 0
     nb_lagrangian_points = 0
     index_lagrangian_points =0
   
     
     while index_diff <10/step_lat :
           while index_lagrangian_points<t_min and mean_positions_n[index_lagrangian_points] < lat_limit_n:
                 sum_velocities = sum_velocities + mean_velocity_n[index_lagrangian_points]
                 nb_lagrangian_points = nb_lagrangian_points+1
                 index_lagrangian_points = index_lagrangian_points+1
           lat_limit_n = lat_limit_n + step_lat
           diff_velocity_each_lat[index_diff] = sum_velocities/index_lagrangian_points - mean_drift_subset[int(10/step_lat)+index_diff]
           sum_velocities = 0
           nb_lagrangian_points = 0
           index_diff = index_diff+1
   
     plt.figure(nb_figure)
     y=np.linspace(0,10,10/step_lat +1)
     plt.plot(y,diff_velocity_each_lat)
     plt.savefig('plots/test_diff_velocities.png')

#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  

if option_diff_each_lat == 1:

   nb_lat = 2*int(abs(lat_limit_south)/step_lat)+1   
   latitude = np.linspace(lat_limit_south, abs(lat_limit_south), nb_lat)
   print latitude
   diffusivity = np.zeros(( nb_lat ))


   for i in range(0,nb_lat):
       diffusivity[i] = variance_based.diffusivity( drifters_array, nb_drifters, nb_data_per_drifters, nb_max, latitude[i], t_min, t_away_from_eq, t_asymptote, long_e_of_study, long_w_of_study )


   title = 'Variance-based diffusivity'
   nb_figure = out.plotting_variable_fc_lat( diffusivity, step_lat, nb_figure, title, lat_limit_south, 'Diffusivity')



