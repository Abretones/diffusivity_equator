# README #


### What is this repository for? ###

This repository aims to extract informations from drifters of the Global Drifter Program, also from positions of thousands of drifters recorded every 6hours from 1979 up to now. 

When it will be finished, it should be able to read whatever subset of data (the subset of drifters crossing the world ocean between the 31st of december 1999 and the 1st of January 2000 or the subset of drifters hanging out in the Caribbean Sea) and eventually compute a few parameters related to the distribution of the drifters. Density of drifters, mean meridional Lagrangian velocity, mean meridional Eulerian velocity, meridional diffusivity... However all the calculations were thought for the equatorial band of the Pacific Ocean. I let to the user to decide if it makes sense to compute a mean velocity with one drifter or the meridional diffusivity in the Gulf Stream and to question the results (if the program doesn't crash before).

I am sure you will have plenty of time to test the limits of this repository... but for now, let's focus on the Pacific Ocean. Where it is supposed to work well. Let's imagine you are somewhere in the Pacific between 120°E and 75°W, 20°S and 20°N. From time to time, you come across a lonely drifter and you can't help but wonder what is the story of these drifters,.. am I right ? You want to understand why they left their friends. Do these drifters have a common goal ? Are there evil forces trying to keep them from doing it ? Then I am glad to tell you that this repository was made for you. 

I will introduce now the different options that you have.

**1) option_all_traj:**

With this option, you will only get a plot of all the drifters trajectories of your subset of data.


**2) option_focus_one_lat:**

Here you choose to look at drifters from the moment they cross a given latitude. You need to specify this latitude (lat_cross, by default it is 0°) and also how long you want to follow it (t_min, 500 time-steps by default). You will get results only if, after crossing the given latitude, the drifter stays in the domain for more than t_min time-steps... also I advise you to start with a small time period. If when you increase t_min, you get less and less drifters, it means that you need to increase the size of your domain : your drifters are running away!

Finally if you are not interested into the drifters crossing the equator along the coast, you can shorten the longitudinal range of study. This is what has been done by default: long_e_of_study=-90 , long_w_of_study = -170.
I am only looking at the eastern part of the Pacific ocean here, so the drifters are not influenced by the coast and by western boundary currents.

**3) option_stat: ** 

This option depends of the option 2. For a subset of drifters initially at the same latitude and between the longitudes of study (long_e_of_study, long_w..), this option looks at the meridional displacement of every drifters and computes a few statistics (variance, skewness) as functions of time. 

It also uses the meridional variance to plot Var(y-y0)/2t, which gives a first idea of the diffusivity if it converges (the diffusivity being the asymptotic value of this curve).

**4) option_distrib: **

**5) option_L_velocity: **

**6) option_E_velocity: **


### How do I get set up? ###

** first you need to check that you have Python 2.7.11 or a more recent version. I you don't, you will probably have to download it... but guess what, it's free ! 

if you are on sverdrup though, you only have to enter 
```
module load python
```


** download the data set:

- go to [this link](http://www.aoml.noaa.gov/envids/gld/dirkrig/parttrk_spatial_temporal.php) 

- enter the coordinates of the region you are interested in. If you want to run the default case, take western edge=120, eastern edge=-75, northern edge=20, southern edge=-20. Note that you need to spend the same range of latitude northward and southward of the equator. I called this latitudinal range 'lat_range', and also the western edge 'long_w' and the eastern edge 'long_e'...

- when you get the subset of data in your mail box (it can take 10 minutes up to a few hours), download and untar the interpolated_gld.*.zip file

** then, open the main.py file and change:

- the path to the interpolated_gld file

- the coordinates of your domain: lat_range, long_w, long_e

- 'percent_line': the idea is to choose a small number if it is your first run (let's say 5). You won't wait for ever (the program is only working with 5% of your data then) to finally discover that there is a bug...

- the options (option_all_traj, option_focus_one_lat, option_stat, option_E_velocity): put the options to 1 if you want to run these parts of the code, 0 otherwise. (see above for a description of each options)

However some options are dependant of others options. For example if you want to run option_stat, you will need option_focus_one_lat.

- depending which options you chose, you can change the parameters relative to these options or keep the default ones (long_e/w_of_study, lat_cross, t_min, step_lat, lat_limit_south)



** Finally open a terminal and run the program 
```
python main.py
```

### Contribution guidelines ###

* Joe LaCasce, Float-derived isopycnal diffusivities in the DIMES experiment, 2014

### Who do I talk to? ###

* Anaïs bretones -- anais.bretones@ens-lyon.fr
* Joe LaCasce
* MetOs (Meterologi - Oseanografi) - University of Oslo