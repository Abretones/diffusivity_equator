import numpy as np

import playing_with_data_north
import selecting
import constants
import pretreatment


# Explanations :
# method 1 consists in following one groupe of drifter, this results into : a lack of information at some latitudes 
#         (especially close to the equator) and large values at small time
 






def diffusivity_and_lagrangian_velocities(method, Lat, Long, nb_drifters, nb_data_per_drifters, nb_max, lat_cross, t_min, long_e_of_study,
                           long_w_of_study, option, representation, step_lat ):




   #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   # Array of the mean positions of drifters from t=0 until 't_min'th time-steps
   #                                                              !! first values correspond to t=6hours !!
   # --> mean_positions_n[ t_min-1 ] (in degres!)
   # --> mean_velocity_n[ t_min-1 ]  (in m.s-1)
   mean_velocity_n, mean_positions_n_wrong_size = playing_with_data_north.mean_one_size(drift_northward_pool, lat_cross, nb_n_poolws, t_min)
   mean_positions_n = mean_positions_n_wrong_size[1:]
   nb_n_pool = nb_n_poolws[1:]


   
   

   displ_squared = np.zeros((t_min-1))
   variance = np.zeros((t_min-1))
   diffusivity = np.zeros((t_min-1))




   # Method 1.1: 
   if method == 1.1:
    diffusivity = 0

   elif method == 1.2:

    #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Array of the variance as a function of time..........        !! first values correspond at t=6hours !!
    # --> variance_n[ t_min-1 ] (in m2)
    # --> diffusivity[ t_min-1 ] (in m2.s-1)
    for t in range(0,t_min-1):
      nb_drifter_one_time = int(nb_n_pool[t])
      if nb_drifter_one_time == 0:
        diffusivity[t] = np.nan
      else :
        for n in range(0, nb_drifter_one_time):
          displ_squared[t]= displ_squared[t]+((drift_northward_pool[n,t+1,0]-mean_positions_n[t])*constants.deg)**2
        variance[t] = displ_squared[t]/nb_drifter_one_time
        diffusivity[t] = variance[t]/(2*(t+1)*6*3600)


 if representation == 'fc_time':
    final_y_axis = np.arange(1,600,1)*6/24
    final_diffusivity = diffusivity
    final_mean_velocity = mean_velocity_n
    nb_drifters_mean = np.mean(nb_n_pool[np.where(nb_n_pool!=0)])
    print len(final_y_axis)
    print len(final_diffusivity)
 elif representation == 'fc_mean':

    final_diffusivity = diffusivity
    final_mean_velocity = mean_velocity_n
    final_y_axis = mean_positions_n

    nb_drifters_mean = np.mean(np.where(nb_n_pool!=0))

 elif representation == 'fc_lat':
    mean_velocity_n_tidy = mean_velocity_n[mean_positions_n.argsort()]
    diffusivity_tidy = diffusivity[mean_positions_n.argsort()]
    mean_positions_n_tidy = np.sort(mean_positions_n[:])     
    nb_drifters_mean = 0
 
    final_y_axis = np.arange(0+step_lat/2,10-step_lat/2,step_lat)
    nb_bin_of_means = len(final_y_axis)
    limit_bin = 0
 
 
    final_mean_velocity = np.zeros((nb_bin_of_means))
    final_diffusivity = np.zeros((nb_bin_of_means))
    t=0
    nb_value_per_bin = 0
    nb_bin_filled = 0

    for i in range(0,nb_bin_of_means):

       while mean_positions_n_tidy[t] < limit_bin + step_lat and t<t_min-2:
          final_mean_velocity[i] =  final_mean_velocity[i] + mean_velocity_n_tidy[t]
          final_diffusivity[i] = final_diffusivity[i] + diffusivity_tidy[t]
          nb_value_per_bin = nb_value_per_bin + 1
          t = t + 1
       if nb_value_per_bin != 0: 
        final_mean_velocity[i] = final_mean_velocity[i]/nb_value_per_bin
        final_diffusivity[i] = final_diffusivity[i]/nb_value_per_bin
        nb_drifters_mean = nb_drifters_mean + nb_value_per_bin
        nb_bin_filled = nb_bin_filled +1
       else :
        final_mean_velocity[i] = np.nan
        final_diffusivity[i] = np.nan

       limit_bin = limit_bin + step_lat
       nb_value_per_bin = 0

    nb_drifters_mean = nb_drifters_mean /nb_bin_filled

 return final_diffusivity, final_mean_velocity, nb_drifters_mean, final_y_axis
   




