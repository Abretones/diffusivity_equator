import numpy as np
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.dates as md
import pylab as pl
import datetime

import matplotlib.gridspec as gridspec
import opening_file
import playing_with_data

import selecting
import constants
import out

import pretreatment

def distributions(crossing_pool, nb_cross,  lat_cross, n_day, lat_limit_south, width, nb_bar, nb_figure):

  if width == 0.1:
      folder = 'plots_distrib'
  elif width == 0.5:
      folder = 'plots_distrib05'

  n=np.arange(4,(n_day+1)*4,4) 


  t_index = np.zeros((n_day))


  x=np.linspace( lat_limit_south, -lat_limit_south, nb_bar+1 )        
  height = np.zeros((nb_bar+1,n_day))


  for j in range(0,n_day):   
    t_index[j] = int(n[j])-1
    for i in range(0,nb_cross):
       index = np.where(np.logical_and(crossing_pool[i,t_index[j],0]>=x, crossing_pool[i,t_index[j],0]<x+width))
       height[index,j]=height[index,j]+1
    height[:,j] = height[:,j]/(nb_cross*width*constants.deg)
   
  fig=plt.figure(nb_figure)

  subfig = 221
  fig.suptitle('$y_{0}=$'+str(lat_cross)+'$^{o}$ \n distribution at different times ('+str(nb_cross)+' drifters)',    \
                   fontsize=16)
  for j in range(0,4):
      subfig = out.subplotting_distrib(height[:,int(j*(n_day+1)/4)], x, width, int(t_index[int(j*(n_day+1)/4)]), -lat_limit_south, subfig)

  plt.subplots_adjust(top=0.85, bottom=0.1, left=0.10, right=0.95, hspace=0.5, wspace=0.35)  

  plt.savefig(str(folder)+'/distrib_'+str(lat_cross)+'.pdf',format='pdf')

  nb_figure = nb_figure+1  
   
 

  for i in range(1,n_day+1):
          file = open(str(folder)+'/real_distribution_at_'+str(i)+'_days_starting_at_'+str(lat_cross)+'.txt','w')
          for j in range(0,nb_bar+1):
              file.write(str(height[j,i-1])+'\n')
          file.close()

  return height, nb_figure


#____________________________________________________________________

def KS_stat(height_float, p0_saved, n_index, dt , nb_points, M):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


 
   delta_N[0]=(height_float[0])/(playing_with_data.trap(height_float[:],1.))
   for i in range(1,nb_points+1):
      delta_N[i]=delta_N[i-1]+(height_float[i])/playing_with_data.trap(height_float,1.)



   F_p[0]=p0_saved[0]/playing_with_data.trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/playing_with_data.trap(p0_saved,1.)
      diff[i] = abs(delta_N[i]-F_p[i])
   

   
   return np.max(diff[:])


def Q_KS(Lambda):
  

  Q = 0

  
  for j in range(1,300):
 
       Q = Q + ((-1)**(j-1))*np.exp(-2*(j*Lambda)**2)


  return 2*Q


def all_included_diffusivity(nb_drifters, drifters_array, nb_data_per_drifters, t_max, month ,long_w,lat_cross,long_w_of_study,long_e_of_study,month_min,month_max,t_min):


  cossing_pool_wrong_size, nb_cross = selecting.latitudes(drifters_array[:,:,0], drifters_array[:,:,1], drifters_array[:,:,2],nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study,drifters_array[:,:,3], month, month_min,month_max  )
  crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]

  mean_velocity = np.zeros((t_min,1))
  mean_position = np.zeros((t_min,1))

  mean_velocity[:,0], mean_position[:,0] = playing_with_data.lagrangian_mean(crossing_pool[:,:,0], crossing_pool[:,:,2], nb_cross, t_min)

  nb_drifters_per_group = np.array([nb_cross])


  displ_squared = np.zeros((t_min))
  variance = np.zeros((t_min))
  diffusivity = np.zeros((t_min))

  diffusivity[0]= np.nan
  for t in range(1,t_min):
        for n in range(0, nb_cross):
          displ_squared[t]= displ_squared[t]+((crossing_pool[n,t,0]-mean_position[t,0])*constants.deg)**2
        variance[t] = displ_squared[t]/nb_cross
        diffusivity[t] = variance[t]/(2*(t)*6*3600) 
  
  return diffusivity, nb_drifters_per_group


def all_included_diffusivity_prov(nb_drifters, drifters_array, nb_data_per_drifters, t_max, month ,long_w,lat_cross,long_w_of_study,long_e_of_study,month_min,month_max,t_min):


  cossing_pool_wrong_size, nb_cross = selecting.latitudes(drifters_array[:,:,0], drifters_array[:,:,1], drifters_array[:,:,2],nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study,drifters_array[:,:,3], month, month_min,month_max  )
  crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]

  mean_velocity = np.zeros((t_min,1))
  mean_position = np.zeros((t_min,1))

  mean_velocity[:,0], mean_position[:,0] = playing_with_data.lagrangian_mean(crossing_pool[:,:,0], crossing_pool[:,:,2], nb_cross, t_min)

  nb_drifters_per_group = np.array([nb_cross])


  displ_squared = np.zeros((t_min))
  variance = np.zeros((t_min))
  diffusivity = np.zeros((t_min))

  diffusivity[0]= np.nan
  for t in range(1,t_min):
        for n in range(0, nb_cross):
          displ_squared[t]= displ_squared[t]+((crossing_pool[n,t,0]-mean_position[t,0])*constants.deg)**2
        variance[t] = displ_squared[t]/nb_cross
        diffusivity[t] = variance[t]/(2*(t)*6*3600) 
  
  return np.mean(diffusivity[20*4+1:30*4+1]), mean_position[20*4+1],mean_position[30*4+1],np.mean(mean_velocity[18]),np.mean(mean_velocity[22])


#///////////////////////////////////////////////////////////////////////////////#
#-------------------------------------------------------------------------------#
										#	
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# I] what area do you want to study ?   					#
										#
ocean = 'pacific'                        # atlantic or pacific			#
										#
lat_range = 30.				 					#
										#
long_e = -75.									#
										#
long_w = 120.									#
										#
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# II] download the corresponding file and give its path				#
										#
#path = '../Pacific-20+20_boundaries/interpolated_gld.20170424_065237'		#
path = '../interpolated_gld.20170705_040435' #
										#
										#
percent_lines = 100                   # put 100 if you want all the data	#

# PART I: reading data or runing adv_diff scheme

option_reading_data = 1

# 1
option_mean_eulerian_velocity = 0

# 2
option_crossing_pool = 0
option_trajectories = 0
option_writting_distrib =0

# 2 a)
option_drifter_crossing_eq_withdrawn = 0
option_lagrangian_velocity_no_cut = 0

option_diffusivity =0





# PART II (independant)
option_using_charged_data = 0


# PART III (independant)
option_multiple_diff = 1


# PART IV after part II or III

option_gaussian_fit = 0




										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# III] Specify these parameters, depending which options you chose		#
										#
long_e_of_study = -110								#
long_w_of_study = -170								#
										#
lat_cross = 1.  
t_day = 50      				             			#						
t_min = 4*t_day+1     								#
trajectories_length = 4*125
										#
										#
width = 0.5									#
step_lat = 0.25             							#
lat_limit_south = -20.                    					#
month_min =1
month_max =12										#
#-------------------------------------------------------------------------------#
#///////////////////////////////////////////////////////////////////////////////#


nb_figure=1

lat_s = -lat_range
lat_n = lat_range

#y_axis = np.arange(lat_cross+0.05,8.5,0.1)

if width == 0.1:
    folder = 'plots_distrib'
elif width == 0.5:
    folder = 'plots_distrib05'

#----------------
#--------------_______________________________________________________________________________________________________________
#    PART 0    ---------------------------------------------------------------------------------------------------------------
#--------------

if option_reading_data ==1:
  size_data = opening_file.open_all( path )

  nb_lines_to_extract = int(percent_lines*size_data/100)

  print 'We are going to work with '+str(percent_lines)+'% of the data...'

  if option_mean_eulerian_velocity ==1:

    nb_positions, nb_drogued_drifters, velocity_drogued, date = opening_file.extract_drogued_drifters_positions(  path, nb_lines_to_extract, ocean, long_w, lat_range, -lat_range,long_w_of_study,long_e_of_study )

    

  else:
    size_data_extracted, nb_drifters, drifters_array, nb_data_per_drifters, t_max, month =                                        \
                                            opening_file.extract_drogued_drifters_trajectories(  path, nb_lines_to_extract, ocean, long_w)

    drifters_array = out.remove_blank( drifters_array, nb_drifters, nb_data_per_drifters, t_max)

    if ocean == 'pacific' and long_w > 0:
      long_w = -180-(180-long_w)
      if long_w_of_study > 0:
         long_w_of_study = -180-(180-long_w_of_study)


  



#-------------- _______________________________________________________________________________________________________________
# end of PART 0 ---------------------------------------------------------------------------------------------------------------
#--------------

#--------------_______________________________________________________________________________________________________________
#    PART I.  ---------------------------------------------------------------------------------------------------------------
#--------------

if option_mean_eulerian_velocity == 1.:
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Mean eulerian velocities computed with all the drifters in the pacific --> Eulerian_mean_velocity_ALL (m.s-1)

  #////////////////////////
  step_lat = 0.5	        #//
  first_lat = lat_limit_south 	#//

  #////////////////////////

  if long_e_of_study == -110 and  long_w_of_study == -170:
     option_domain = 'whole_range'							
  elif long_e_of_study == -110 and  long_w_of_study > -170:
     option_domain = 'est'
  elif long_e_of_study < -110 and  long_w_of_study== -170:
     option_domain = 'west'
     
  
  nb_bin_of_means_whole_domain = int(abs(lat_limit_south)/step_lat)*2
  y_whole_domain = np.linspace(first_lat-step_lat/2, abs(lat_limit_south)-step_lat/2, nb_bin_of_means_whole_domain+1)
  Eulerian_mean_velocity_ALL = np.zeros((nb_bin_of_means_whole_domain+1,3))
  eddy_kinetic_energy = np.zeros((nb_bin_of_means_whole_domain+1,1))
  nb_drifter_per_band = np.zeros((nb_bin_of_means_whole_domain+1))


  
  a = 0
  lat = first_lat
  #file = open('plots_velocity/eulerian_velocity_'+str(option_domain)+'.txt','w') 
  while a < nb_bin_of_means_whole_domain+1 :
     Eulerian_mean_velocity_ALL[a,0],Eulerian_mean_velocity_ALL[a,1],Eulerian_mean_velocity_ALL[a,2], nb_drifter_per_band[a]= playing_with_data.eulerian_velocity_drogued_drifters(velocity_drogued[:,0], velocity_drogued[:,1], lat, step_lat, nb_positions,date)
     eddy_kinetic_energy[a,0]=playing_with_data.eddy_kine(Eulerian_mean_velocity_ALL[a,0],velocity_drogued[:,0], velocity_drogued[:,1], lat, step_lat, nb_positions,date)
     

     #file.write(str(Eulerian_mean_velocity_ALL[a,0])+'\n')
     lat = lat + step_lat
     a = a+1    

  #file.close()
  plt.figure(nb_figure)
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif',size=15)
  plt.plot(y_whole_domain,nb_drifter_per_band/(365.25*38+45), 'ro')
  plt.savefig('nb_buoys_day.png')
  nb_figure=nb_figure+1

  nbd=np.sum(nb_drifter_per_band)/(60*0.5*(365.25*38+45))
  print nbd
  
  plt.figure(nb_figure)
  ax=plt.subplot(111)
  plt.ylabel(r'Number of buoy-days',size=15)
  plt.xlabel('Latitudes',size=15)
  plt.xlim([-20,20])
  plt.yticks([2000,4000,6000])
  tick_locs = [-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
  tick_lbls = [r'20$^{o}$S','','','','','','','','','',r'$10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N','','','','','','','','','',r'20$^{o}$N']
  plt.xticks(tick_locs, tick_lbls)
  ax.bar(y_whole_domain,nb_drifter_per_band,step_lat)
  plt.savefig('plots_final/nb_buoy_days_'+str(option_domain)+'.pdf',format='pdf')
  nb_figure = nb_figure+1
  
  mean_nb_data_per_band = np.mean(nb_drifter_per_band)
  '''
  txt_file = 'plots_velocity/eulerian_velocity_whole_range_drogued_drifters05.txt'
  with open(txt_file, "r") as f:
            tab = []
            for line in range(0,nb_bin_of_means_whole_domain):
                tab.append(f.readline().split())
  
  vel = np.array([x[0] for x in tab])
  Eulerian_mean_velocity_ALL[:,0]=np.array(vel)
  '''
  label = ['velocity', 'standard error', 'fit']
  
  # - - - - - - - - - - - THE FIT - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
  M=int(abs(lat_limit_south)/step_lat)
  '''
  Eulerian_mean_velocity_ALL[0:M,2] = -(0.01*pl.tanh((-y_whole_domain[0:M]-0.3)/0.2)+0.02*np.exp(-((-y_whole_domain[0:M]-1)/4)**2)-0.02*np.tanh((-y_whole_domain[0:M]-13)/12) +0.023*pl.tanh((-y_whole_domain[0:M])/0.2) )
 

  Eulerian_mean_velocity_ALL[M:nb_bin_of_means_whole_domain,2] = (0.03*pl.tanh(y_whole_domain[M:nb_bin_of_means_whole_domain]/0.5)+0.035*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]-3)/2.)**2)-0.001*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.2)- 0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.21) +0.011*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-9.2)/1.3) - 0.029*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-14.5)/3.3)-0.008
  '''
  nb_figure = out.one_plot(y_whole_domain, Eulerian_mean_velocity_ALL, 4, label, mean_nb_data_per_band, 'velocity', 'eulerian',option_domain, abs(lat_limit_south), 'pdf', nb_figure )
  nb_figure = out.one_plot(y_whole_domain, eddy_kinetic_energy, 1, label, mean_nb_data_per_band, 'velocity', 'eulerian',option_domain, abs(lat_limit_south), 'pdf', nb_figure )
  




if option_crossing_pool == 1:
   
   cossing_pool_wrong_size, nb_cross = selecting.latitudes(drifters_array[:,:,0], drifters_array[:,:,1], drifters_array[:,:,2],nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study,drifters_array[:,:,3], month, month_min,month_max  )
   crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]



   nb_points = 2*int(-lat_limit_south/width)

   if option_writting_distrib == 1:
     height_float = np.zeros((nb_points+1,t_day))
     height_float, nb_figure = distributions(crossing_pool, nb_cross,  lat_cross, t_day, lat_limit_south,width, nb_points, nb_figure)


if option_trajectories ==1:

   title = 'Trajectories of the '+str(nb_cross)+' drifters starting at '+str(lat_cross)+'$^{o}N$'
   nb_figure = out.plotting_traj(lat_s, lat_n, long_w, long_e, crossing_pool[:,:,0], crossing_pool[:,:,1], nb_cross, lat_cross, nb_figure, title)

#-------------- _______________________________________________________________________________________________________________
# end of PART I ---------------------------------------------------------------------------------------------------------------
#--------------



#--------------_______________________________________________________________________________________________________________
#    PART II.  ---------------------------------------------------------------------------------------------------------------
#--------------
if option_lagrangian_velocity_no_cut ==1:
   mean_velocity = np.zeros((t_min,1))
   mean_position = np.zeros((t_min,1))

   mean_velocity[:,0], mean_position[:,0] = playing_with_data.lagrangian_mean(crossing_pool[:,:,0], crossing_pool[:,:,2], nb_cross, t_min)

   nb_drifters_per_group = np.array([nb_cross])

   label =[]
   label.append('all month, -110;-170')
   nb_figure = out.one_plot(mean_position, mean_velocity, 1, label, nb_drifters_per_group, 'velocity', lat_cross,'1', 'fc_mean', 'png', nb_figure )



   displ_squared = np.zeros((t_min))
   variance = np.zeros((t_min))
   diffusivity = np.zeros((t_min,1))

   diffusivity[0,0]= np.nan
   for t in range(1,t_min):
        for n in range(0, nb_cross):
          displ_squared[t]= displ_squared[t]+((crossing_pool[n,t,0]-mean_position[t,0])*constants.deg)**2
        variance[t] = displ_squared[t]/nb_cross
        diffusivity[t,0] = variance[t]/(2*(t)*6*3600)   

   nb_figure = out.one_plot(mean_position, diffusivity, 1, label, nb_drifters_per_group, 'diffusivity', lat_cross,'met1', 'fc_time', 'png', nb_figure )


if option_drifter_crossing_eq_withdrawn ==1:



   same_hemisphere_drifters, nb_drifters_at_t = playing_with_data.seperate_drifters_at_eq( crossing_pool, nb_cross, t_min,lat_cross )

   mean_velocity = np.zeros((t_min,1))
   mean_position = np.zeros((t_min,1))

   mean_velocity[:,0], mean_position[:,0] = playing_with_data.lagrangian_mean_one_side(same_hemisphere_drifters[:,:,0], same_hemisphere_drifters[:,:,2], nb_drifters_at_t, t_min)


   file = open(str(folder)+'/mean_velocity_starting_at_'+str(lat_cross)+'.txt','w')
   for j in range(0,t_min):
       file.write(str( mean_position[j,0])+'\n')
       file.write(str( nb_drifters_at_t[j,0])+'\n')
   file.close()



   label =[]
   label.append('all month,only NH points, -110;-170')
   nb_figure = out.one_plot(mean_position, mean_velocity, 1, label, nb_drifters_at_t, 'velocity', lat_cross,'NH', 'fc_mean', 'png', nb_figure )



   displ_squared = np.zeros((t_min))
   variance = np.zeros((t_min))
   diffusivity = np.zeros((t_min,1))

   diffusivity[0,0]= np.nan
   for t in range(1,t_min):
        for n in range(0, int(nb_drifters_at_t[t,0])):
          displ_squared[t]= displ_squared[t]+((same_hemisphere_drifters[n,t,0]-mean_position[t,0])*constants.deg)**2
        variance[t] = displ_squared[t]/nb_drifters_at_t[t,0]
        diffusivity[t,0] = variance[t]/(2*(t)*6*3600)   

   nb_figure = out.one_plot(mean_position, diffusivity, 1, label, nb_drifters_at_t, 'diffusivity', lat_cross,'NH', 'fc_time', 'png', nb_figure )   
   
#-------------- _______________________________________________________________________________________________________________
# end of PART I ---------------------------------------------------------------------------------------------------------------
#--------------

if option_using_charged_data == 1:

   mean_position = np.zeros((t_min,1))
   nb_drifters_at_t = np.zeros((t_min,1))


   nb_points = 2*int(-lat_limit_south/width)

   txt_file = str(folder)+'/mean_velocity_starting_at_'+str(lat_cross)+'.txt'
   with open(txt_file, "r") as f:
       tab1 = []
       tab2 = []

       for line in range(0,t_min):
         tab1.append(f.readline().split())
         tab2.append(f.readline().split())
 
       var1 = np.array([x[0] for x in tab1])
       var2 = np.array([x[0] for x in tab2])
       
       for i in range(0,t_min):            
         mean_position[i,0] = str(var1[i])
         nb_drifters_at_t[i,0] = str(var2[i])
   #--------------------------------------------------------------------------------------------------


   mean_p=np.zeros((t_day))
   mean_v=np.zeros((t_day))
   mean_p[0]=np.mean(mean_position[3-2:3+2,0])
   mean_v[0]=(mean_p[0]-lat_cross)*constants.deg/(24*3600)
   for i in range(1,t_day):
       mean_p[i]=np.mean(mean_position[3+i*4-2:3+i*4+2,0])
       mean_v[i]=(mean_p[i]-mean_p[i-1])*constants.deg/(24*3600)
   width=0.1   
   nb_bin_of_means_whole_domain = int(abs(lat_limit_south)/width)*2
   y_whole_domain = np.linspace(lat_limit_south, abs(lat_limit_south), nb_bin_of_means_whole_domain+1)
   Eulerian_mean_velocity_ALL = np.zeros((nb_bin_of_means_whole_domain+1))

   M=int(abs(lat_limit_south)/width)
 
   Eulerian_mean_velocity_ALL[0:M] = -(0.01*pl.tanh((-y_whole_domain[0:M]-0.3)/0.2)+0.02*np.exp(-((-y_whole_domain[0:M]-1)/4)**2)-0.02*np.tanh((-y_whole_domain[0:M]-13)/12) +0.023*pl.tanh((-y_whole_domain[0:M])/0.2) )
 

   Eulerian_mean_velocity_ALL[M:nb_bin_of_means_whole_domain] = (0.03*pl.tanh(y_whole_domain[M:nb_bin_of_means_whole_domain]/0.5)+0.035*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]-3)/2.)**2)-0.001*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.2)- 0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.21) +0.011*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-9.2)/1.3) - 0.029*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-14.5)/3.3)-0.008
   
   plt.figure(nb_figure)
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')
   tick_locs = [4*10,4*20,4*30]
   tick_lbls = [r'10',r'20',r'30']
   #plt.xticks(tick_locs, tick_lbls)
   plt.plot(mean_p[1:],mean_v[1:],y_whole_domain,Eulerian_mean_velocity_ALL)
   
   plt.savefig('plots_final/lagr_pos_'+str(lat_cross)+'.png')
   nb_figure=nb_figure+1
   width=0.5

   height_float = np.zeros((nb_points+1,t_day))



   
   for j in range(1,t_day+1):
     txt_file = str(folder)+'/real_distribution_at_'+str(j)+'_days_starting_at_'+str(lat_cross)+'.txt'
   
     with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])

       
       for i in range(0,nb_points+1):   
         
         height_float[i,j-1] = str(height[i])

   nb_points2 = 2*int(-lat_limit_south/0.1)
   height_float2 = np.zeros((nb_points2+1))
   xaxis2 = np.linspace( lat_limit_south, -lat_limit_south, nb_points2+1 )
   txt_file = 'plots_distrib/real_distribution_at_1_days_starting_at_'+str(lat_cross)+'.txt'
   
   with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points2+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])

       
       for i in range(0,nb_points2+1):   
         
         height_float2[i] = str(height[i])
 
   print np.max(height_float2)
   print mean_position[30*4-1], mean_position[40*4-1]
   
   xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )
   fig,ax1 =plt.subplots(figsize=(10,10))
   ax2 = ax1.twinx()
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')

   #plt.legend(prop={'size':6},bbox_to_anchor=(1.1, 1.05))
   print 'hey'
   tick_locs = [-5,-4,-3,-2,-1,0,1,2,3,4,5]
   tick_lbls = [r'$-5^{o}$S','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','',r'$5^{o}$N']
   plt.xticks(tick_locs, tick_lbls)

   pl.xlim([-10,10]) 
   ax1.bar(xaxis2, height_float2[:]/playing_with_data.trap(height_float2[:],0.1), 0.1, color='#F0E68C',edgecolor='#FFD700')
   
   ax1.arrow(1.2, 2, 2.7, 0, head_width=0.03, head_length=0.1,linewidth=5, fc='k', ec='k')
   ax1.text(1.5,2.05,'$\sim$14 cm.s$^{-1}$')


   ax2.bar(xaxis, height_float[:,29]/playing_with_data.trap(height_float[:,29],width), width, color='none',edgecolor='#FF8C00',hatch='//')
   ax2.plot([mean_position[30*4-1],mean_position[30*4-1]],[0,0.25],'--', color='#FF8C00')

   
   plt.subplots_adjust(top=0.95, bottom=0.05, left=0.15, right=0.9, hspace=0.2, wspace=0.7)
   plt.savefig('plots_final/distrib_1.pdf',format='pdf')
   nb_figure=nb_figure+1
   

if option_gaussian_fit == 1:


   xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )

   h = np.zeros((nb_points+1)) 
   p=np.zeros((nb_points+1,t_day,3))

   P=np.zeros((t_day))
   D=np.zeros((t_day))
   
   kappa = np.zeros((3))
   
   kappa[0] = 0.62*1E4
   kappa[1] = 0.8*1E4
   kappa[2] = 2.7*1E4

   #kappa[0] = 0.2*1E4
   #kappa[1] = 0.4*1E4
   #kappa[2] = 0.6*1E4

   print kappa

   for i in range(0,3):
    for j in range(1,t_day+1):
     p[:,j-1,i]=(1/np.sqrt(4*np.pi*kappa[i] *j*6*3600))*np.exp( -((xaxis-mean_position[j*4-1])*constants.deg)**2/(4*kappa[i] *j*24*3600))
     
     #h[nb_points/2:-1]=height_float[nb_points/2:-1,j-1]
     
     #D[j-1] = KS_stat(h, p[:,j-1], j, 1, nb_points, len(p[:,0]) )
     
     #Ne = nb_drifters_at_t[4*j-1]
     #P[j-1] = Q_KS((np.sqrt(Ne)+0.12+0.11/np.sqrt(Ne))*D[j-1])

   dt =10   

   plt.figure(nb_figure)
   plt.figure(figsize=(20,10))   
   
   plt.subplot(121)
   plt.ylim([0,0.34])
   plt.bar(xaxis,height_float[:,t_day-1-dt]/playing_with_data.trap(height_float[nb_points/2:-1,t_day-1-dt],width),width,color='#F0E68C',edgecolor='#FFD700')#,width),width)
   for i in range(1,2):
      plt.plot(xaxis,p[:,t_day-1-dt,i]/playing_with_data.trap(p[:,t_day-1-dt,i],width),linewidth=2.5)
   plt.plot(xaxis,p[:,t_day-1-dt,i]/playing_with_data.trap(p[:,t_day-1-dt,i],width),linewidth=2.5)
   #plt.legend(prop={'size':10},bbox_to_anchor=(0, 1.05))

   
   plt.subplot(122)
   plt.ylim([0,0.34])
   #plt.title('$\kappa$ ='+str(kappa))
   plt.bar(xaxis,height_float[:,t_day-1]/playing_with_data.trap(height_float[nb_points/2:-1,t_day-1],width),width,color='#F0E68C',edgecolor='#FFD700')#,width),width)
   for i in range(1,3):
      plt.plot(xaxis,p[:,t_day-1,i]/playing_with_data.trap(p[:,t_day-1,i],width),linewidth=2.5, label='$\kappa$='+str("%+02d" %kappa[i]))
   plt.legend(prop={'size':12},bbox_to_anchor=(1.1, 1.05))
   
   plt.savefig('plots_final/fit_gaussian_at_'+str(t_day)+'_days_starting_at_'+str(lat_cross)+'.png')
   

   
   plt.savefig('plots_gaussian/fit_gaussian_at_'+str(t_day)+'_days_starting_at_'+str(lat_cross)+'.png')
   
   nb_figure=nb_figure+1
   '''
   plt.figure(nb_figure)
   plt.plot(P)
   plt.savefig('plots_gaussian/KS_proba_'+str(lat_cross)+'_t'+str(t_day)+'days.png')
   nb_figure = nb_figure+1
   '''

if option_multiple_diff == 1:
  lat_cross0=np.array([1.,1.,6.,8.,10.])
  lat_cross1=np.array([0,-1.,-2.,-5.,-10.])
  
  lat_cross2=np.arange(-13.,13.,1)
  nb_diff = len(lat_cross2)

  mean_diff = np.zeros((2,nb_diff))
  mean = np.zeros((2,nb_diff))
 
  velocity_1d=np.zeros((nb_diff))
  velocity_2d=np.zeros((nb_diff))
  #mean[0,:]=lat_cross2
  
  diffusivity = np.zeros((t_min,5,2))
  nb_drifters_per_group = np.zeros((t_min,5,2))
  label=[]    
  for i in range(0,5):
    diffusivity[:,i,0], nb_drifters_per_group[:,i]=all_included_diffusivity(nb_drifters, drifters_array, nb_data_per_drifters, t_max, month,long_w,lat_cross0[i],long_w_of_study,long_e_of_study,month_min,month_max,t_min)
    #diffusivity[:,i,1], nb_drifters_per_group[:,i]=all_included_diffusivity(nb_drifters, drifters_array, nb_data_per_drifters, t_max, month,long_w,lat_cross1[i],long_w_of_study,long_e_of_study,month_min,month_max,t_min)
  '''
  for i in range(0,nb_diff):
    mean_diff[:,i],mean[0,i],mean[1,i],velocity_1d[i],velocity_2d[i]=all_included_diffusivity_prov(nb_drifters, drifters_array, nb_data_per_drifters, t_max, month,long_w,lat_cross2[i],long_w_of_study,long_e_of_study,month_min,month_max,t_min)
    print mean_diff[0,i]
  '''
  '''
  nb_bin_of_means_whole_domain = int(abs(lat_limit_south)/width)*2
  y_whole_domain = np.linspace(lat_limit_south, abs(lat_limit_south), nb_bin_of_means_whole_domain+1)
  Eulerian_mean_velocity_ALL = np.zeros((nb_bin_of_means_whole_domain+1))

  M=int(abs(lat_limit_south)/width)
 
  Eulerian_mean_velocity_ALL[0:M] = -(0.01*pl.tanh((-y_whole_domain[0:M]-0.3)/0.2)+0.02*np.exp(-((-y_whole_domain[0:M]-1)/4)**2)-0.02*np.tanh((-y_whole_domain[0:M]-13)/12) +0.023*pl.tanh((-y_whole_domain[0:M])/0.2) )
 

  Eulerian_mean_velocity_ALL[M:nb_bin_of_means_whole_domain] = (0.03*pl.tanh(y_whole_domain[M:nb_bin_of_means_whole_domain]/0.5)+0.035*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]-3)/2.)**2)-0.001*np.exp(-((y_whole_domain[M:nb_bin_of_means_whole_domain]+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.2)- 0.08*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-1.8)/0.21) +0.011*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-9.2)/1.3) - 0.029*pl.tanh((y_whole_domain[M:nb_bin_of_means_whole_domain]-14.5)/3.3)-0.008

  plt.figure(nb_figure)
  plt.plot(lat_cross2,velocity_1d,label='3days mean lagrangian velocity' )
  plt.plot(lat_cross2,velocity_2d,label='4days mean lagrangian velocity' )
  plt.plot(y_whole_domain,Eulerian_mean_velocity_ALL*1E2)
  plt.legend(prop={'size':8},bbox_to_anchor=(1.1, 1.05))
  plt.savefig('velocities.png')

  '''

  fig=plt.figure(figsize=(20,15))  
  ax = fig.add_axes([0.05,0.05,0.9,0.85])
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  gs = gridspec.GridSpec(0,0)
  pl.figure()
  
  ax = plt.subplot(111) # row 0, col 0

  tick_locs = [4*10,4*20,4*30,4*40]
  tick_lbls = [r'10',r'20',r'30',r'40']
  plt.xticks(tick_locs, tick_lbls)
  plt.xlabel(r'days')
  for i in range(3,4):
      if i==0:
        plt.plot(diffusivity[:,i,0],label=str(int(lat_cross0[i]))+'$^{o}$')
      else:
        plt.plot(diffusivity[:,i,0],label=str(abs(int(lat_cross0[3])))+'$^{o}$N')
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])
  plt.xlabel(r'Time (days)',size=13)
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=13)
  ax.legend(prop={'size':13},bbox_to_anchor=(1., 1.))
  plt.savefig('plots_final/variance_based_diffusivity_8.pdf',format='pdf')

  #/////////////////////////////////////////////////////////////////////////////

  fig=plt.figure(figsize=(20,15))  
  ax = fig.add_axes([0.05,0.05,0.9,0.85])
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  gs = gridspec.GridSpec(0,0)
  pl.figure()
  
  ax = plt.subplot(111) # row 0, col 0

  tick_locs = [4*10,4*20,4*30,4*40]
  tick_lbls = [r'10',r'20',r'30',r'40']
  plt.xticks(tick_locs, tick_lbls)
  plt.xlabel(r'days')
  plt.plot(diffusivity[:,3,0],label=str(int(lat_cross0[3]))+'$^{o}$')

  plt.plot(diffusivity[:,2,0],color='darkorange',label=str(abs(int(lat_cross0[2])))+'$^{o}$N')

  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])
  plt.xlabel(r'Time (days)',size=13)
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=13)
  ax.legend(prop={'size':13},bbox_to_anchor=(1., 1.))
  plt.savefig('plots_final/variance_based_diffusivity_6.pdf',format='pdf')


  #/////////////////////////////////////////////////////////////////////////////

  fig=plt.figure(figsize=(20,15))  
  ax = fig.add_axes([0.05,0.05,0.9,0.85])
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  gs = gridspec.GridSpec(0,0)
  pl.figure()
  
  ax = plt.subplot(111) # row 0, col 0

  tick_locs = [4*10,4*20,4*30,4*40]
  tick_lbls = [r'10',r'20',r'30',r'40']
  plt.xticks(tick_locs, tick_lbls)
  plt.xlabel(r'days')
  plt.plot(diffusivity[:,3,0],label=str(int(lat_cross0[3]))+'$^{o}$')

  plt.plot(diffusivity[:,2,0],color='darkorange',label=str(abs(int(lat_cross0[2])))+'$^{o}$N')
  plt.plot(diffusivity[:,1,0],color='red',label=str(abs(int(lat_cross0[1])))+'$^{o}$N')
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0),size=15)
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])
  plt.xlabel(r'Time (days)',size=13)
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=13)
  ax.legend(prop={'size':13},bbox_to_anchor=(1., 1.))
  plt.savefig('plots_final/variance_based_diffusivity_1.pdf',format='pdf')

  #/////////////////////////////////////////////////////////////////////////////


  fig=plt.figure(figsize=(20,15))  
  ax = fig.add_axes([0.05,0.05,0.9,0.85])
  plt.rc('text', usetex=True)
  plt.rc('font', family='serif')
  gs = gridspec.GridSpec(2, 2)
  pl.figure()
  
  ax = plt.subplot(gs[0 ,0]) # row 0, col 0

  tick_locs = [4*10,4*20,4*30,4*40]
  tick_lbls = [r'10',r'20',r'30',r'40']
  plt.xticks(tick_locs, tick_lbls)
  plt.xlabel(r'days')
  for i in range(0,5):
      if i==0:
        plt.plot(diffusivity[:,i,0],label=str(int(lat_cross0[i]))+'$^{o}$')
      else:
        plt.plot(diffusivity[:,i,0],label=str(abs(int(lat_cross0[i])))+'$^{o}$S')
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])
  plt.xlabel(r'Time (days)',size=10)
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=10)
  ax.legend(prop={'size':8},bbox_to_anchor=(1.1, 1.05))

  ax = plt.subplot(gs[0, 1]) # row 0, col 1
  tick_locs = [4*10,4*20,4*30]
  tick_lbls = [r'10',r'20',r'30']
  plt.xticks(tick_locs, tick_lbls)
  plt.xlabel(r'days')
  for i in range(0,5):
      if i==0:
        plt.plot(diffusivity[:,i,1],label=str(int(lat_cross0[i]))+'$^{o}$')
      else:
        plt.plot(diffusivity[:,i,1],label=str(int(lat_cross0[i]))+'$^{o}$N')
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=10)
  plt.xlabel(r'Time',size=10)
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])
  ax.legend(prop={'size':8},bbox_to_anchor=(1.1, 1.05))

  ax = plt.subplot(gs[1, :]) # row 1, span all columns
  tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
  tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
  plt.xlabel(r'Latitudes',size=10)
  plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=10)
  plt.xticks(tick_locs, tick_lbls)
  for i in range(0,nb_diff):
      plt.plot(mean[:,i],mean_diff[:,i],linewidth=3)
  plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
  plt.yticks([10000,20000,30000])
  plt.ylim([0,35000])


  plt.subplots_adjust(top=0.95, bottom=0.07, left=0.15, right=0.95, hspace=0.5, wspace=0.5)
  plt.savefig('plots_final/variance_based_diffusivity.pdf',format='pdf')
  #nb_figure = out.one_plot(diffusivity, diffusivity, 5, lat_cross0, nb_drifters_per_group, 'diffusivity', lat_cross,'multiplot', 'fc_time', 'png', nb_figure )
  



