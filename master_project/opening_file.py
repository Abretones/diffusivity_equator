import numpy as np
import scipy 
import datetime
import pandas as pd

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def open_all(name_file):
    nb_data=-1
    with open(name_file, "r") as f:
      for line in f:
         nb_data=nb_data+1
    print 'The file has '+str(nb_data)+' lines.'
    return nb_data



#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def extract_drogued_drifters_trajectories(name_file, nb_lines_to_extract, ocean, long_w):

    print '... that is, with '+str(nb_lines_to_extract)+' lines.'


    tab=[]
    tab_meta=[]

    with open(name_file, "r") as f:
      entete = f.readline().rstrip('\n').split()

      for line in range(0,nb_lines_to_extract):
        tab.append(f.readline().split())

    # - - - -  - - - - - - - - - -  - - - - -  - - - - - - - 

    with open('../metadata_gld.20170705_040435',"r") as f2:
       ente2 = f2.readline().rstrip('\n').split()

       for line in f2:
        tab_meta.append(line.split())

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - 


    date_list  = extract_column(tab,1)
    Lat = extract_column(tab,3)
    Long = extract_column(tab,4) 
    n_velocity = extract_column(tab,7)
    nb_data = int(len(Lat))

    lost_drogued_list = extract_column(tab_meta,12)

    #------------------------------------------------------
    ID = extract_column(tab,0)
    ID = np.append(ID, 0)


    inventory = np.array(sorted(list(set([x[0] for x in tab]))))
    
    nb_drifters = int(len(inventory))
    print 'Hence we are left with ',nb_drifters,'drifters.'
    
    nb_data_per_drifters = np.zeros((nb_drifters))
    a=0
    for i in range(0,nb_drifters):
        while ID[a]==ID[a+1] :
             nb_data_per_drifters[i] = nb_data_per_drifters[i]+1
   	     a=a+1

        nb_data_per_drifters[i]=nb_data_per_drifters[i]+1
        a=a+1

    nb_max_data_per_drifters = int(np.max(nb_data_per_drifters))   

    #------------------------------------------------------    


    #///////////////////////////////////////////////////////////////////////////#/
    # in the Pacific, the longitudinal range is 120E-75W (also +120,-75)	#/
    # it is easier to work with a (-180-60),-75 range !				#/
    if ocean == 'pacific' and long_w>0:						#/
										#/
      for i in range(0,nb_data):						#/
          if float(Long[i])>0:							#/
           Long[i] = -180-(180-float(Long[i]))					#/
										#/
    #///////////////////////////////////////////////////////////////////////////#/   

    

    drift_tidy = np.zeros((nb_drifters, nb_max_data_per_drifters, 4))
    date = np.empty_like(drift_tidy[:,:,0], dtype = 'datetime64[D]')
    month = np.zeros((nb_drifters, nb_max_data_per_drifters))

    a=0
    b=0
    c=0

    while c<= nb_data-1:

      while nb_data_per_drifters[b] > a:
        drift_tidy[b,a,0] = float(Lat[c])
        drift_tidy[b,a,1] = Long[c] 

        if lost_drogued_list[b] == 'null':
         lost_drogued = np.datetime64('2017-12-12')
        else:
         lost_drogued = np.datetime64(lost_drogued_list[b])
        
        date[b,a] = np.datetime64(date_list[c])
        if lost_drogued > np.datetime64(date_list[c]):
           drift_tidy[b,a,3] = 1.


        if float(n_velocity[c])==999.999:
           drift_tidy[b,a,2] = np.nan
        else :
           drift_tidy[b,a,2] = float(n_velocity[c])      
        a=a+1
        c=c+1
      month[b,:] = pd.DatetimeIndex(date[b,:]).month
      a=0   
      b=b+1

    
    print 'done'
    return nb_data, nb_drifters, drift_tidy, nb_data_per_drifters, nb_max_data_per_drifters, month




def extract_drogued_drifters_positions(name_file, nb_lines_to_extract, ocean, long_w, llat_n, llat_s, llong_w, llong_e):

    print '... that is, with '+str(nb_lines_to_extract)+' lines.'


    tab=[]
    tab_meta=[]

    with open(name_file, "r") as f:
      entete = f.readline().rstrip('\n').split()

      for line in range(0,nb_lines_to_extract):
        tab.append(f.readline().split())

    # - - - -  - - - - - - - - - -  - - - - -  - - - - - - - 

    with open('../metadata_gld.20170705_040435',"r") as f2:
       ente2 = f2.readline().rstrip('\n').split()

       for line in f2:
        tab_meta.append(line.split())

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - 

    lost_drogued_list = extract_column(tab_meta,12)

    date_list  = extract_column(tab,1)
    Lat = extract_column(tab,3)
    Long = extract_column(tab,4) 
    n_velocity = extract_column(tab,6)
    nb_data = int(len(Lat))

    #///////////////////////////////////////////////////////////
    ID = extract_column(tab,0)
    ID = np.append(ID, 0)

    inventory = np.array(sorted(list(set([x[0] for x in tab]))))
    
    nb_drifters = int(len(inventory))
    print 'Hence we are left with ',nb_drifters,'drifters.'
    
    nb_data_per_drifters = np.zeros((nb_drifters))
    a=0
    for i in range(0,nb_drifters):
        while ID[a]==ID[a+1] :
             nb_data_per_drifters[i] = nb_data_per_drifters[i]+1
   	     a=a+1

        nb_data_per_drifters[i]=nb_data_per_drifters[i]+1
        a=a+1
   #////////////////////////////////////////////////////////////


    #///////////////////////////////////////////////////////////////////////////#/
    # in the Pacific, the longitudinal range is 120E-75W (also +120,-75)	#/
    # it is easier to work with a (-180-60),-75 range !				#/
    if ocean == 'pacific' and long_w>0:						#/
										#/
      for i in range(0,nb_data):						#/
          if float(Long[i])>0:							#/
           Long[i] = -180-(180-float(Long[i]))					#/
										#/
    #///////////////////////////////////////////////////////////////////////////#/   

    velocity_drogued = np.zeros((nb_data,2))
    date = np.empty_like(velocity_drogued[:,0], dtype = 'datetime64[D]')
    date2=[]

    a=0
    b=0
    c=0
    d = 0
    nb_drogued_drifters = 0
    while c<= nb_data-1:
      if lost_drogued_list[b] == 'null':
         lost_drogued = np.datetime64('2017-12-12')
      else:
         lost_drogued = np.datetime64(lost_drogued_list[b])

      if lost_drogued > np.datetime64(date_list[c]):
         nb_drogued_drifters = nb_drogued_drifters +1
         
      while nb_data_per_drifters[b] > a:
        if lost_drogued > np.datetime64(date_list[c]) and  float(Lat[c])<=llat_n+0.25 and  float(Lat[c])>= llat_s-0.25 and  float(Long[c])<=llong_e and float(Long[c])>=llong_w:
          velocity_drogued[d,0]=float( Lat[c])
          date[d] = np.datetime64(date_list[c])
          date2.append(date_list[c])
          if float(n_velocity[c])==999.999:
            velocity_drogued[d,1] = np.nan
          else :
            velocity_drogued[d,1] = float(n_velocity[c]) 
          d=d+1
     
        a=a+1
        c=c+1

      a=0   
      b=b+1
    nb_positions = d-2



    print nb_positions, nb_drogued_drifters
    return nb_positions, nb_drogued_drifters, velocity_drogued[0:nb_positions,:], date2 #date[0:nb_positions]

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def extract_column(tab,i):
    column=np.array([x[i] for x in tab])
    return column


#----------------------------------------------------------------------------

