import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')

from mpl_toolkits.basemap import Basemap
from matplotlib.ticker import NullFormatter,MultipleLocator, FormatStrFormatter
import matplotlib.gridspec as gridspec
import scipy 
import pylab as pl
from netCDF4 import Dataset
from scipy.interpolate import Rbf
import sys
import os
from matplotlib.lines import Line2D
from matplotlib import patches

import constants


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def remove_blank(data, nb_drifters, counter, nb_max):
    for i in range(0,nb_drifters):
       for j in range(int(counter[i]),nb_max):
             data[i,j,0]=np.nan
             data[i,j,1]=np.nan

    return data  

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def plotting_traj( lat_s, lat_n, long_w, long_e, Lat, Long, nb_drifters, case, nb_figure, title):
   m = Basemap(llcrnrlon=long_w,llcrnrlat=-10,urcrnrlon=long_e,urcrnrlat=15,projection='mill')

   plt.figure(nb_figure)
   fig=plt.figure(figsize=(15,4.5*lat_n/20))
   ax = fig.add_axes([0.05,0.05,0.9,0.85])
   
   for i in range(1,nb_drifters):
     x, y = m(Long[i,:], Lat[i,:])
     m.plot(x,y,linestyle='-',lw=0.5)

   m.drawcoastlines(linewidth=0.5)
   m.drawparallels(np.arange(lat_s,lat_n,5),labels=[1,1,0,0])
   m.drawmeridians(np.arange(long_w,long_e,10),labels=[0,0,0,1])
   m.fillcontinents(color='0.8')

   #plt.title(title)  
   plt.savefig('plots_traj/Traj_'+str(case)+'_'+str(nb_drifters)+'.pdf',format='pdf')
   nb_figure=nb_figure+1

   return nb_figure


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def plotting_variable_fc_lat(variable1, variable2, variable3, x, nb_figure, title, lat_limit_south, option):

       
    plt.figure(nb_figure)
    plt.plot(x,variable1,x, variable2, x, variable1+variable3)
    pl.xlim([0, abs(lat_limit_south)])
    plt.xlabel('latitude')
    plt.title(title)

    if option == 'Diffusivity':
       units = 'm$^{2}$.s$^{-1}$'
       plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    else :
       units = 'm.s$^{-1}$'

    plt.ylabel('velocity ('+str(units)+')')
    plt.savefig('plots_north/'+str(option)+'.png')

    nb_figure = nb_figure+1

    return nb_figure
#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def subplotting_distrib(Lat_drifter,nb_cross,lat_cross,time_index, index_lat_max, width, nb_figure):


        nb_bar = int(index_lat_max/width)
        

        x=np.linspace( 0, index_lat_max, nb_bar+1 )        
        height=np.zeros((nb_bar+1))

        for i in range(0,nb_bar):
           for j in range(0,nb_cross):
               if Lat_drifter[j,time_index-1]-Lat_drifter[j,0]+lat_cross>=x[i] and Lat_drifter[j,time_index-1]-Lat_drifter[j,0]+lat_cross<x[i+1]:
                  height[i]=height[i]+1


        plt.subplot(nb_figure)
        plt.bar(x,height,width)
        plt.title('t='+str(time_index*6/24)+' days',fontsize=10)
        pl.xlim([0,index_lat_max])
        pl.ylim([0,np.max(height)])

        nb_figure=nb_figure+1
        return nb_figure


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def plotting_variable_fc_position( variable, position, lat_cross, nb_pool, nb_figure, variable_name,title ):
    plt.figure(nb_figure)
    if variable_name == 'Diffusivity_'+str(lat_cross):
        units = 'm$^{2}$.s$^{-1}$'
        pl.xlim([np.min(position),np.max(position)])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0)) 

    elif variable_name == 'Variance_'+str(lat_cross):
        units = 'm$^{2}$'
        pl.xlim([np.min(position),np.max(position)])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0)) 

    elif variable_name == 'Lagrangian-velocity_'+str(lat_cross):
        units = 'm.s$^{-1}$'
    elif variable_name == 'Theoretical_eulerian-velocity_'+str(lat_cross):
        units = 'm.s$^{-1}$'
        plt.ylim([-0.1,0.30])
       
    
    plt.ylabel(str(variable_name)+' ('+str(units)+')')
    plt.plot(position, variable)
    

    plt.title(title)

    plt.xlabel('latitude (in degre)')
    plt.savefig('plots_north/'+str(variable_name)+'.png')

    nb_figure = nb_figure+1

    return nb_figure

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def plotting_distrib_norm(Lat, MEAN, kappa, lat_cross, nb_drifter, time_index, index_lat_max, width, nb_figure):


        nb_bar = int(index_lat_max/width)
        x=np.linspace( 0, index_lat_max, nb_bar+1 )        
        height=np.zeros((nb_bar+1))
        
        
        for i in range(0,nb_bar):
           for j in range(0,nb_drifter):
               if Lat[j]>=x[i] and Lat[j]<x[i+1]:
                  height[i]=height[i]+1
	
        x = x * constants.deg
        MEAN = MEAN *constants.deg
        height=height/(nb_drifter*width*constants.deg)
        
        y=(1/np.sqrt(4*np.pi*kappa *time_index*6*3600))*np.exp( -(x-MEAN)**2/(4*kappa *time_index*6*3600))

        fig=plt.figure(figsize=(10,8.5))
        plt.figure(nb_figure)
        plt.bar(x,height,width*constants.deg)
        plt.plot(x,y, 'r--', lw=3)
        plt.title('Distribution at t='+str(time_index*6/24)+' days (trajectories of '+str(nb_drifter)+' drifters)') # \n Gaussian: $\kappa =$'+str("%.2e" % kappa)+'m$^{2}$.s$^{-1}$')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
	plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.xlabel('$y-y_{0}+y_{0 theoric}$ in meters \n ($y_{0theoric}$='+str(lat_cross)+'$^{o}$='+str("%.2e" % float(lat_cross*constants.deg))+'m )')
        plt.ylabel('Normalized number of drifters')
        plt.savefig('plots_north/distrib_'+str(int(lat_cross*10))+'_'+str(int(time_index*6/24))+'days.png')
         
        nb_figure=nb_figure+1
        return nb_figure

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def subplotting_distrib_norm(Lat, p0, MEAN, kappa, lat_cross, nb_drifter, time_index, index_lat_max, width, nb_figure):


        nb_bar = int(index_lat_max/width)
        x=np.linspace( 0, index_lat_max, nb_bar+1 )        
        height = np.zeros((nb_bar+1))
        
        for i in range(0,nb_drifter):
           index = np.where(np.logical_and(Lat[i]>=x, Lat[i]<x+width))
           height[index]=height[index]+1
	
        x = x * constants.deg
        #MEAN = MEAN *constants.deg
        height=height/(nb_drifter*width*constants.deg)
        
        #y=(1/np.sqrt(4*np.pi*kappa *time_index*6*3600))*np.exp( -(x-MEAN)**2/(4*kappa *time_index*6*3600))

        plt.subplot(nb_figure)
        plt.bar(x/constants.deg,height,width,color='c',edgecolor='c')
        #plt.plot(x/constants.deg,y, 'r', lw=1)
        plt.plot(x/constants.deg,p0, 'b', lw=1)
        plt.title('t='+str(time_index*6/24)+' days',fontsize=10)
        plt.yticks([0, 3*1E-6, 6*1E-6])
        pl.xlim([0,index_lat_max])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

        nb_figure=nb_figure+1

        return nb_figure

def subplotting_distrib(height,x, width, time_index, index_lat_max, nb_figure):


        plt.subplot(nb_figure)
        plt.bar(x,height,width,color='c',edgecolor='c')

        plt.title('t='+str(time_index*6/24)+' days',fontsize=10)
        plt.yticks([0, 3*1E-6, 6*1E-6])
        pl.xlim([-index_lat_max,index_lat_max])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

        nb_figure=nb_figure+1

        return nb_figure

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~ 
#----------------------------------------------------------------------------

def subplotting_pdf(distrib1,distrib2, x, index_lat_max,nb_figure):


        plt.subplot(nb_figure)
        plt.plot(x,distrib1, 'r', lw=1)
        plt.plot(x,distrib2, 'b', lw=1)
        plt.yticks([0, 3*1E-6, 6*1E-6])
        pl.xlim([0,index_lat_max])
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

        nb_figure=nb_figure+1

        return nb_figure

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~ 





def one_plot(x, f, nb_plots, label,nb_drifters, variable, option1,option2, representation, ft, nb_figure):

 gs = gridspec.GridSpec(0, 0)
 plt.figure(nb_figure)   
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 ax = plt.subplot(111)
 #ax = plt.subplot(gs[0, 0])
 if option1 == 'eulerian' or option1 == 'eulerian_whole_range' or option1 == 'eulerian_whole_range_drogued' :
   if nb_plots ==4:
     
     ax.fill_between(x, f[:,1], f[:,2], color = 'lightgrey',label = 'standard error')
     lineA,=ax.plot(x, f[:,0],linewidth=2.5, color='darkslategray',label=str(label[0]))
     blue_patch = patches.Patch(color='lightgrey', label='standard error')
     ax.legend(handles = [lineA, blue_patch], \
          prop={'size':11},bbox_to_anchor=(1.1, 1.05))
     #plt.ylim([-0.12,0.13])
     plt.xlim([-20,20])
     tick_locs = [-20,-19,-18,-17,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
     tick_lbls = [r'20$^{o}$S','','','','','','','','','',r'$10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N','','','','','','','','','',r'20$^{o}$N']
     plt.xticks(tick_locs, tick_lbls)
     #plt.yticks([-0.1,-0.05,0.,0.05,0.1])  
     plt.xlabel(r'Latitudes',size=14)
     units = r'm.s$^{-1}$'
     plt.ylabel('Velocity ('+str(units)+')',size=15)
     fig = 'plots_final/'+str(nb_plots)+'plots_'+str(option1)+'_'+str(option2)+'zonal.'+str(ft)    
     ann = ax.annotate("NEC",\
                  xy=(18, 0), xycoords='data',\
                  xytext=(12, .2), textcoords='data',\
                  size=10, va="center", ha="center",\
                  arrowprops=dict(arrowstyle="-|>",\
                                  connectionstyle="arc3,rad=-0.2",\
                                  fc="w"), )

     ann = ax.annotate("NECC",\
                  xy=(6.5, 0.3), xycoords='data',\
                  xytext=(-5, .3), textcoords='data',\
                  size=10, va="center", ha="center",\
                  arrowprops=dict(arrowstyle="-|>",\
                                  connectionstyle="arc3,rad=-0.2",\
                                  fc="w"), )

     nn = ax.annotate("SEC",\
                  xy=(0.2, -0.42), xycoords='data',\
                  xytext=(-15,-.5), textcoords='data',\
                  size=10, va="center", ha="center",\
                  arrowprops=dict(arrowstyle="-|>",\
                                  connectionstyle="arc3,rad=0.2",\
                                  fc="w"),)

     nn = ax.annotate("SEC",\
                  xy=(-5, -0.3), xycoords='data',\
                  xytext=(-15,-.5), textcoords='data',\
                  size=10, va="center", ha="center",\
                  arrowprops=dict(arrowstyle="-|>",\
                                  connectionstyle="arc3,rad=0.2",\
                                  fc="w"),  )

     nn = ax.annotate("EUC",\
                  xy=(-2, -0.07), xycoords='data',\
                  xytext=(-10, 0.01), textcoords='data',\
                  size=10, va="center", ha="center",\
                  arrowprops=dict(arrowstyle="-|>",\
                                  connectionstyle="arc3,rad=-0.2",\
                                  fc="w"), )

   else:
    for i in range(0,nb_plots):
        ax.plot(x, f[:,i],linewidth=2)
    plt.xlim([-15,15])
    tick_locs = [-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
    tick_lbls = ['','','','','',r'$10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N','','','','','']
    plt.xticks(tick_locs, tick_lbls)
    plt.yticks([0.01,0.02,0.03,0.04])
    plt.xlabel(r'Latitudes',size=14)
    units = r'm$^{2}$.s$^{-2}$' 
    plt.ylabel('EKE ('+str(units)+')',size=15)
    fig = 'plots_final/EKE_merid_est.'+str(ft)

 if option1 == 'diffusivity':
   
    tick_locs = [4*10,4*20,4*30,4*40]
    tick_lbls = [r'10',r'20',r'30',r'40']
    plt.xticks(tick_locs, tick_lbls)
    plt.xlabel(r'days')
    ax.plot(x, f[:,0],linewidth=2)
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.yticks([10000,20000,30000])
    plt.ylim([0,35000])
    plt.xlabel(r'Time',size=10)
    plt.ylabel(r'Meridional diffusivity ( m$^{2}$.s$^{-1}$)',size=10)
    fig = 'plots_final/diffusivity1.'+str(ft)

 if ft == 'png':
    plt.savefig(fig)
 elif ft == 'pdf':
    plt.savefig(fig,format=ft)

 nb_figure = nb_figure + 1

 return nb_figure


