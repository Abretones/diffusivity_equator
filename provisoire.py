import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')

import constants

import pylab as pl
#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx
  yo = np.sum(v)

  return yo


#____________________________________________________________________
def adrhs(p,v,k,y,dy):
#
# RHS of the 1D Adv-Diff equation for absolute dispersion
#
  gp=np.gradient(p,dy)
  y=-np.gradient(v*p,dy) + np.gradient(k*gp,dy)

  return y


#____________________________________________________________________
def distribution_6_different_times(y, dy, ys, py, nb_points, nb_time, dt, factor, n_index, v, kappa,nb_distrib):
   p0 = np.zeros((nb_points))

   p0 = (1/(np.sqrt(2*np.pi)*py))*np.exp(-(((y-ys)/py)**2)/2) #initial PDF


   p0_saved = np.zeros((nb_distrib,nb_points))
   cnt =0
 
   for j in range(0,int(nb_time)):
     k1 = adrhs(p0,v,kappa,y,dy)
     k2 = adrhs(p0+k1*dt/2,v,kappa,y,dy)
     k3 = adrhs(p0+k2*dt/2,v,kappa,y,dy)
     k4 = adrhs(p0+k3*dt,v,kappa,y,dy)
     p0=p0+dt/6*(k1+2*k2+2*k3+k4)   

     if cnt <nb_distrib and j == int(n_index[cnt]-1): 
       p0_saved[cnt,:] = np.copy(p0) #/trap(p2,dy)
       cnt=cnt+1
     

   return p0_saved



#____________________________________________________________________
def KS_stat(height_float, p0_saved, nb_points, M):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


   delta_N[0:19]=(height_float[0])/(trap(height_float[:],1))
   for i in range(1,nb_points+1):
      delta_N[20*i:20*i+20]=delta_N[20*(i-1)]+(height_float[i])/trap(height_float,1)


   F_p[0]=p0_saved[0]/trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/trap(p0_saved,1.)
      diff[i] = abs(delta_N[i]-F_p[i])
   return np.max(diff)

#____________________________________________________________________
def Q_KS(Lambda):
  

  Q = 0

  
  for j in range(1,300):
 
       Q = Q + ((-1)**(j-1))*np.exp(-2*(j*Lambda)**2)


  return 2*Q

#____________________________________________________________________
def connecting_dots(a,b,y,dy,distance_a_b):

   # if distance_a_b=1degree, then there are 200 points to compute between a and b
   nb_points = int(200*distance_a_b)
   f = np.zeros((nb_points))

   coeff = (b-a)/(y[-1]-y[0])
   f[0]=a

   for i in range(1,nb_points):
    f[i] = f[i-1]+coeff*dy*distance_a_b

   return f


#____________________________________________________________________
def kappa_profile(kappa_values,kappa_resolution,option_figure,nb_figure):

 deg=110.567
 dy = 1./200        # grid spacing, in degres
 y_max = 20.
 y  = np.linspace(-y_max, y_max, 2*y_max/dy+1)
 dy = dy*constants.deg*1E-3
 y_max = y_max*constants.deg*1E-3
 y = y*constants.deg*1E-3
 M = len(y)

 kappa = np.zeros((M))

 nb_points_between_kappa_values = int(200*kappa_resolution)
 equator_index = int(M/2)
 
 kappa[0:equator_index-7*nb_points_between_kappa_values]=kappa_values[0]

 for i in range(0,int(14*kappa_resolution)):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)] = connecting_dots(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy,kappa_resolution)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M]=kappa_values[-2]
 
 if option_figure==1:
   plt.figure(nb_figure)
   plt.plot(y/deg,kappa)
   plt.ylabel('kappa')
   plt.xlabel('latitude')
   plt.savefig('plots_DEAP/new/kappa_best_fits_'+str(nb_figure)+'.png')
   nb_figure =nb_figure+1

 kappa = kappa*0.0864

 return dy,y_max,y,dy,M,kappa,nb_figure


#//////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////////////
#____________________________________________________________________
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

def distrib_at_n( kappa_values,kappa_resolution,starting_lat , lat_limit_south, width, day_max):

 deg=110.567
 t0 = 0 					                # starting time
 dt = 0.0001				                        # time step in day

 factor = 2500 #150/(600*dt)
 N = 4*day_max*factor #4*3*factor#501*factor			# total integration (N*dt)
 
 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------

 dy,y_max,y,dy,M,kappa,nb_figure = kappa_profile(kappa_values,kappa_resolution,0,0)

 #////////////////////////////////////////////////////////////////////////////////////////////
 # #-------------------------------------------------------------------------------------------
 #Eulerian velocity
 # fit velocity drogued and undrogued drifters

 v = np.zeros((M))
 v[0:int(M/2)-1] = -(0.01*pl.tanh((-y[0:int(M/2)-1]/deg-0.3)/0.2)+0.02*np.exp(-((-y[0:int(M/2)-1]/deg-1)/4)**2)-0.02*np.tanh((-y[0:int(M/2)-1]/deg-13)/12) +0.023*pl.tanh((-y[0:int(M/2)-1]/deg)/0.2) )
 

 v[int(M/2):M] = (0.03*pl.tanh(y[int(M/2):M]/(deg*0.5))+0.035*np.exp(-((y[int(M/2):M]/deg-3)/2.)**2)-0.001*np.exp(-((y[int(M/2):M]/deg+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.2)- 0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.21) +0.011*pl.tanh((y[int(M/2):M]/deg-9.5)/1.5) - 0.029*pl.tanh((y[int(M/2):M]/deg-14.5)/3.3)  -0.008 #- 0.04*pl.tanh((y[int(M/2)+1:M-1]/deg-1)/1.6)+0.04

 v=v*86.4 #km/day



 
 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 
 ys = starting_lat*deg                    # positon of initial PDF (degres)
 py = 0.2*deg				  # width of initial PDF



 n_index=np.arange(4*factor,4*(day_max+1)*factor+1,4*factor) 

 nb_points = 2*int(-lat_limit_south/width)


 p0_saved = np.zeros((day_max,M))     
 p0_saved = distribution_6_different_times(y,dy,ys,py,M,N,dt,factor, n_index,v,kappa,day_max)

 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 first =10
 height_float = np.zeros((nb_points+1,day_max-first))   
 xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )
 P = np.zeros((day_max-first))

 if starting_lat>0:     
     nb_floats = np.array([0,391,537,671,0,0,692,0,529,0,0,0,0,0,0,0,0,0])
 else:
     nb_floats = np.array([0,463,409,487,0,0,489,0,0,0,0,0,0,0])
 Ne = nb_floats[abs(int(starting_lat))]


 for t in range(first,day_max):

   txt_file = '../master_project/plots_distrib/real_distribution_at_'+str(int(n_index[t-1]*6/(24*factor)))+'_days_starting_at_'+str(starting_lat)+'.txt'
 
   with open(txt_file, "r") as f:
     tab = []

     for line in range(0,nb_points+1):
         tab.append(f.readline().split())

     height = np.array([x[0] for x in tab])
     
     for i in range(0,nb_points+1):
       height_float[i,t-first] = str(height[i])

   #///////////////////////////////////////////////////////////////////////////////////////////
   #-------------------------------------------------------------------------------------------    


   Q = KS_stat(height_float[:,t-first], p0_saved[t-1,:], nb_points, M)
   P[t-first] = Q_KS((np.sqrt(Ne)+0.12+0.11/np.sqrt(Ne))*Q)
   print np.max(P)
   print np.mean(P)



 return np.mean(P)
 
