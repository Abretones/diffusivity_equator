import numpy as np
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.dates as md
import pylab as pl
import datetime

import opening_file
import playing_with_data_north
import selecting
import constants
import out_north


def distributions(drifters_array, nb_drifters, nb_data_per_drifters, t_max, lat_cross, trajectories_length, t_last_plot, long_e_of_study,   \
   				                                    long_w_of_study,lat_limit_south, nb_figure):

  cossing_pool_wrong_size, nb_cross = selecting.latitudes(drifters_array[:,:,0], drifters_array[:,:,1], nb_drifters, nb_data_per_drifters,   \
                                                                           t_max, lat_cross, trajectories_length, long_e_of_study,   \
   				                                           long_w_of_study,1,drifters_array[:,:,3]  )
  crossing_pool = cossing_pool_wrong_size[0:nb_cross, :, :]




  n=np.arange(4,4*125+1,4) 

  nb_distrib = (t_last_plot-1)/4
  t_index = np.zeros((nb_distrib))

  width = 0.5

  nb_bar = 2*int(-lat_limit_south/width)
  x=np.linspace( lat_limit_south, -lat_limit_south, nb_bar+1 )        
  height = np.zeros((nb_bar+1,nb_distrib))


   

  for j in range(0,nb_distrib):   
    t_index[j] = int(n[j])  
    for i in range(0,nb_cross):
       index = np.where(np.logical_and(crossing_pool[i,int(n[j]),0]>=x, crossing_pool[i,int(n[j]),0]<x+width))
       height[index,j]=height[index,j]+1
    height[:,j] = height[:,j]/(nb_cross*width*constants.deg)
   
  fig=plt.figure(nb_figure)

  subfig = 221
  fig.suptitle('$y_{0}=$'+str(lat_cross)+'$^{o}$ \n distribution at different times ('+str(nb_cross)+' drifters)',    \
                   fontsize=16)
  for j in range(0,4):
      subfig = out_north.subplotting_distrib(height[:,int(j*nb_distrib/4)], x, width, int(t_index[int(j*nb_distrib/4)]), -lat_limit_south, subfig)

  plt.subplots_adjust(top=0.85, bottom=0.1, left=0.10, right=0.95, hspace=0.5, wspace=0.35)  

  plt.savefig('plots_distrib/distrib_'+str(lat_cross)+'.png')

  nb_figure = nb_figure+1  
   
   
  for i in range(0,nb_distrib):
          t = t_index[i]*6/24
          file = open('plots_distrib/real_distribution_at_'+str(int(t))+'_days_starting_at_'+str(lat_cross)+'.txt','w')
          for j in range(0,nb_bar+1):
              file.write(str(height[j,i])+'\n')
          file.close()

  return nb_figure

#///////////////////////////////////////////////////////////////////////////////#
#-------------------------------------------------------------------------------#
										#	
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# I] what area do you want to study ?   					#
										#
ocean = 'pacific'                        # atlantic or pacific			#
										#
lat_range = 30.				 					#
										#
long_e = -75.									#
										#
long_w = 120.									#
										#
										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# II] download the corresponding file and give its path				#
										#
#path = '../Pacific-20+20_boundaries/interpolated_gld.20170424_065237'		#
path = '../interpolated_gld.20170705_040435' #
										#
										#
percent_lines = 100                      # put 100 if you want all the data	#

# III] Specify these parameters, depending which options you chose		#
										#
long_e_of_study = -110								#
long_w_of_study = -170								#
										#				
t_last_plot = 4*(50+1)	      							#
trajectories_length = 4*(50+1)
										#
										#
width = 0.5									#
step_lat = 0.25             							#
lat_limit_south = -20.                    					#
										#
#-------------------------------------------------------------------------------#
#///////////////////////////////////////////////////////////////////////////////#


nb_figure=1

lat_s = -lat_range
lat_n = lat_range


#lat_cross=np.array([0.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,14.5])
lat_cross=np.array([1.,2.,3.,4.,5.,6.,7.,8.,9.,10.,11.,12.,13.,14.,15.])
#lat_cross=np.array([-0.5,-1.5,-2.5,-3.5,-4.5,-5.5,-6.5,-7.5,-8.5,-9.5,-10.5,-11.5,-12.5,-13.5,-14.5])
#lat_cross=np.array([-1.,-2.,-3.,-4.,-5.,-6.,-7.,-8.,-9.,-10.,-11.,-12.,-13.,-14.,-15.])

size_data = opening_file.open_all( path )

nb_lines_to_extract = int(percent_lines*size_data/100)

print 'We are going to work with '+str(percent_lines)+'% of the data...'

size_data_extracted, nb_drifters, drifters_array, nb_data_per_drifters, t_max =                                        \
                                            opening_file.extract_subset(  path, nb_lines_to_extract, ocean, long_w)

drifters_array = out_north.remove_blank( drifters_array, nb_drifters, nb_data_per_drifters, t_max)

if ocean == 'pacific' and long_w > 0:
      long_w = -180-(180-long_w)
      if long_w_of_study > 0:
         long_w_of_study = -180-(180-long_w_of_study)


#for k in range(0,15):

nb_figure = distributions(drifters_array, nb_drifters, nb_data_per_drifters, t_max, 7.,trajectories_length, t_last_plot, long_e_of_study,   \
   				                                    long_w_of_study,lat_limit_south,nb_figure)





