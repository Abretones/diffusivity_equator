import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')


from mpl_toolkits.basemap import Basemap
from matplotlib.ticker import NullFormatter,MultipleLocator, FormatStrFormatter
import matplotlib.gridspec as gridspec
import scipy 
import pylab as pl
from netCDF4 import Dataset
from scipy.interpolate import Rbf
import sys
import os
from matplotlib.lines import Line2D
import constants

def fc(a,b,y,dy):
 f = np.zeros((200))
 coeff = (b-a)/(y[-1]-y[0])
 f[0]=a
 for i in range(1,200):
  f[i] = f[i-1]+coeff*dy
 return f

#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx
  yo = np.sum(v)

  return yo


#____________________________________________________________________
def adrhs(p,v,k,y,dy):
#
# RHS of the 1D Adv-Diff equation for absolute dispersion
#
  gp=np.gradient(p,dy)
  y=-np.gradient(v*p,dy) + np.gradient(k*gp,dy)

  return y



#____________________________________________________________________
def distribution_6_different_times(y, dy, ys, py, nb_points, nb_time, dt, factor, n_index, v, kappa):
 p0 = np.zeros((nb_points))


 p0 = (1/(np.sqrt(2*np.pi)*py))*np.exp(-(((y-ys)/py)**2)/2) #initial PDF


 i=1
 #///////////////////
 #//////////////////
 #//////////////////

 p0_saved = np.zeros((4,nb_points))
 cnt=0
 
 for j in range(0,nb_time):
   k1 = adrhs(p0,v,kappa,y,dy)
   k2 = adrhs(p0+k1*dt/2,v,kappa,y,dy)
   k3 = adrhs(p0+k2*dt/2,v,kappa,y,dy)
   k4 = adrhs(p0+k3*dt,v,kappa,y,dy)
   p0=p0+dt/6*(k1+2*k2+2*k3+k4)   

   if cnt <4 and j == int(n_index[cnt]-1):
     p0_saved[cnt,:] = np.copy(p0) #/trap(p2,dy)
     cnt=cnt+1
     print 'hey'


 return p0_saved


#____________________________________________________________________
def subplotting_distrib(y_p0, p0, y,height, nb_confi,width, time,nb_figure):
        plt.subplot(nb_figure)
        tick_locs = [-10,0,10]
        tick_lbls = [r'$-10^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'$10^{o}$N']
        plt.xticks(tick_locs, tick_lbls)
        plt.yticks([0,0.1,0.2,0.3])
        for i in range(0,nb_confi-2):
           plt.plot(y_p0,p0[:,i]/trap(p0[:,i],1./200))
        plt.plot(y_p0,p0[:,2]/trap(p0[:,2],1./200),'m')
        plt.plot([0.,0.],[0,0.35],'k--')
        plt.bar(y, height/trap(height,width), width, color='#F0E68C',edgecolor='#FFD700')
        pl.xlim([-15,15])
        pl.ylim([0,0.35])

        if time==1.0:
           plt.title(r''+str(int(time))+'$^{st}$ day',fontsize=15,color='gray')
        else:
           plt.title(r''+str(int(time))+'$^{th}$ day',fontsize=15,color='gray')
   

        #plt.title('t='+str(time_index*6/24)+' days',fontsize=10)
        #pl.xlim([-20,20])
        #pl.ylim([0,np.max(height)])

        nb_figure=nb_figure+1
        return nb_figure



#____________________________________________________________________
def figure_subplots( p0, x_p0, height,x,nb_confi, width, index_t, dt, starting_lat, nb_figure):

   fig=plt.figure(figsize=(9,9))
   subfig=221
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')

   for i in range(0,4):    
    subfig=subplotting_distrib(x_p0,p0[i,:,:],x,height[:,i],nb_confi, width, index_t[i]*dt, subfig)

   plt.subplots_adjust(top=0.9, bottom=0.1, left=0.10, right=0.95, hspace=0.35, wspace=0.35)
   plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   plt.savefig('plots_adv_diff/distrib_scheme_'+str(starting_lat)+'.pdf',format='pdf')

   nb_figure=nb_figure+1 
   return nb_figure


#____________________________________________________________________
def plotting_distrib(x,p0,kappa, time, nb_tests, label, nb_figure):

   
   plt.plot(nb_figure)
   if time==1.0:
     plt.title('t='+str(int(time))+'$^{st}$ day',fontsize=11)
   else:
     plt.title('t='+str(int(time))+'$^{th}$ day',fontsize=11)
   
   ax1 = plt.subplot(211)
   #plt.setp(ax1.set_yticks([0, 0.003, 0.006]))
   for i in range(0, nb_tests):
       ax1.plot(y,p0[:,i], label=str(label[i]))
       #ax1.set_yticks([0, 0.000003, 0.000006])
       ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   ax1.legend(prop={'size':6},bbox_to_anchor=(1.1, 1.05))

   ax2=plt.subplot(212)
   for i in range(0, nb_tests):
       ax2.plot(y, kappa[:,i])  
   #ax2.set_yticks([0, 10000,20000])
   ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))   
   plt.savefig('plots_adv_diff/distrib_'+str(nb_tests)+'_different_diffusivity_'+str(time)+'.png')

   nb_figure = nb_figure+1
   return nb_figure

#____________________________________________________________________
def plotting_distrib_plus(y, p0, height, index_lat_max, width, kappa, time, nb_tests, label, nb_figure):

   nb_bar = int(index_lat_max/width)
   x = np.linspace( 0, index_lat_max, nb_bar+1 )

   plt.plot(nb_figure)
   plt.title('t='+str(time)+' days',fontsize=10)
   ax1 = plt.subplot(211)
   ax1.bar(x, height, width, color='c',edgecolor='c')
   for i in range(0, nb_tests):
       ax1.plot(x, p0[:,:,i], label=str(label[i]))
   ax1.set_yticks([0, 0.000003, 0.000006])
   ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   ax1.legend(prop={'size':6},bbox_to_anchor=(1.1, 1.05))

   ax2=plt.subplot(212)
   for i in range(0, nb_tests):
       ax2.plot(y, kappa[:,i])  
   ax2.set_yticks([0, 10000,20000])
   ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   
   plt.savefig('plots_adv_diff/distrib_'+str(nb_tests)+'_different_diffusivity_'+str(time)+'_plus.png')

   nb_figure = nb_figure+1
   return nb_figure

# solve meridional adv-diff equation numerically
# dp/dt = - d/dy(k dp/dy)
# RHS is in adrhs.m






#//////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////////////
#____________________________________________________________________
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

def distrib_at_n(starting_lat, nb_diff_confi, index_snapshot, option_testing_different_diff, option_superimpose_real_distrib, lat_limit_south, width, nb_figure):

 deg=110.567

 dy = 1./200        # grid spacing, in degres
 y_max = 20.
 y  = np.linspace(-y_max, y_max, 2*y_max/dy+1)
 print len(y)
 dy = dy*deg
 y_max = y_max*deg
 y = y*deg
 M = len(y)

 t0 = 0 					# starting time
 dt = 0.0001				# time step in day
 factor = 2500 #150/(600*dt)
 N = 4*(index_snapshot+1)*factor#501*factor			# total integration (N*dt)
 

 
 #////////////////////////////////////////////////////////////////////////////////////////////
 # #-------------------------------------------------------------------------------------------
 v = np.zeros((M))


 #Eulerian velocity

 
 # fit velocity drogued drifters

 
 v[0:int(M/2)-1] = -(0.01*pl.tanh((-y[0:int(M/2)-1]/deg-0.3)/0.2)+0.02*np.exp(-((-y[0:int(M/2)-1]/deg-1)/4)**2)-0.02*np.tanh((-y[0:int(M/2)-1]/deg-13)/12) +0.023*pl.tanh((-y[0:int(M/2)-1]/deg)/0.2) )
 

 v[int(M/2):M] = (0.03*pl.tanh(y[int(M/2):M]/(deg*0.5))+0.035*np.exp(-((y[int(M/2):M]/deg-3)/2.)**2)-0.001*np.exp(-((y[int(M/2):M]/deg+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.2)- 0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.21) +0.011*pl.tanh((y[int(M/2):M]/deg-9.5)/1.5) - 0.029*pl.tanh((y[int(M/2):M]/deg-14.5)/3.3)  -0.008 #- 0.04*pl.tanh((y[int(M/2)+1:M-1]/deg-1)/1.6)+0.04


 plt.figure(nb_figure)
 pl.xlim([-y_max/deg, y_max/deg])

 plt.plot(y/deg,v)
 units = 'm.s$^{-1}$'
 plt.ylim([-0.15,0.15])
 plt.ylabel('velocity ('+str(units)+')')
 plt.xlabel('latitude')
 plt.savefig('plots_adv_diff/Eulerian_velocity_try.png')
 nb_figure=nb_figure+1
 
 v=v*86.4 #km/day


 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 kappa = np.zeros((M,3))
 label = []
   
 nb_confi = 3
 
 kappa[:,0] = 1.*1E4
 label.append('constant ')
  
 kappa[:,1] = 2.*1E4 * np.exp(-((y-200)/400)**2)+ 5000
 #label.append(r'$\kappa_{y} = 2. 10^{4}\times e^{-\big(\frac{y-2}{8}\big)^{2}}+3.10^{3}$')
 label.append(r'maximum at the equator ')
 '''
 #kappa[:,2] = 4.1E4 * np.exp(-((y+400)/200)**2)  + 4.*1E4*np.exp(-((y-400)/200)**2) + 5000
 kappa[:,2] = 2.*1E4 * np.exp(-((y+500)/150)**2)  + 4.*1E4*np.exp(-((y-400)/200)**2) + 5000
 #label.append(r'$\kappa_{y} = 4.10^{4}\times \Big(e^{-\big(\frac{y+4}{2}\big)^{2}}+e^{-\big(\frac{y-4}{2}\big)^{2}}\Big)+5. 10^{3}$')
 label.append(r'"minimum" at the equator ')
 '''

 nb_points_between_kappa_values = int(200)
 equator_index = int(M/2)

 kappa_values2=np.array([28802, 13915, 16145, 4007, 36590, 39953, 41472, 8854, 13916, 35681, 46754, 41669, 35904, 15853, 7301, 7301]) 
 kappa[0:equator_index-7*nb_points_between_kappa_values,2]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),2] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,2]=kappa_values2[-1]
 label.append('optimized $\kappa$ for group starting at 1$^{o}$')
 

 print label

 #kappa[:,nb_confi] = (1./2)*pl.tanh((y/deg+4)/0.8)*24*1E3-(1./2)*pl.tanh((y/deg-6)/3)*28*1E3+7*1E3+np.tanh((y/deg-15)/10)*5*1E3
 
 #kappa98



 
 #nb_confi = nb_confi +1
 
 plt.figure(nb_figure)
 tick_locs = [-10,0,10]
 tick_lbls = [r'$10^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls)
 ax = plt.subplot(111)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 pl.xlim([-y_max/deg, y_max/deg])
 
 for i in range(0,nb_confi-1):
   ax.plot(y/deg,kappa[:,i],label=str(label[i]))
 ax.plot(y/deg,kappa[:,2],'m',label=str(label[2]))
 ax.legend(prop={'size':10},bbox_to_anchor=(1.12, 1.05))

 units = 'm$^{2}$.s$^{-1}$'
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 plt.ylabel(r'diffusivity ('+str(units)+')')
 #plt.xlabel(r'latitude')
 plt.savefig('plots_adv_diff/diffusivity.pdf',format='pdf')
 nb_figure=nb_figure+1

 kappa = kappa*0.0864
 

 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 
 ys = starting_lat*deg                    # positon of initial PDF (degres)
 py = 0.2*deg				  # width of initial PDF


 #n_index = [4*5*factor, 4*15*factor, 4*30*factor, 4*50*factor, 4*80*factor, 4*125*factor]
 #n_index = [4*5*factor, 4*15*factor, 4*30*factor, 4*45*factor, 4*60*factor, 4*79*factor]
 n_index = [4*factor, 4*factor, 4*factor, 4*factor]
 nb_points = int(-lat_limit_south/width)+1


   

 if option_testing_different_diff == 0:
    
    nb_bar = 2*int(-lat_limit_south/width)
    xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_bar+1 )

    height_float = np.zeros((nb_bar+1,4))
    
    for i in range(0,4):
        txt_file = 'plots_distrib/real_distribution_at_'+str(int(n_index[i]*6/(24*factor)))+'_days_starting_at_'+str(starting_lat)+'.txt'
        with open(txt_file, "r") as f:
            tab = []
            for line in range(0,nb_bar+1):
                tab.append(f.readline().split())
        
        height = np.array([x[0] for x in tab])
        height_float[:,i] = np.zeros((nb_bar+1))
        for j in range(0,nb_bar+1):
              height_float[j,i] = str(height[j])
    

    p0_saved = np.zeros((4,M,nb_confi))
    for i in range(0,nb_confi):
      p0_saved[:,:,i] = distribution_6_different_times(y, dy, ys, py, M, N, dt, factor, n_index, v, kappa[:,i])


    '''
    file = open('plots_adv_diff/real_distribution_at_125_days_'+str(starting_lat)+'.txt','w') 
   
    for i in range(0,M):
         file.write(str(p0_saved[5,i,0])+'\n') 
    file.close()
    
    txt_file = 'plots_adv_diff/real_distribution_at_125_days_'+str(starting_lat)+'.txt'
    with open(txt_file, "r") as f:
       tab = []
       for line in range(0,M):
                tab.append(f.readline().split())

       prov = np.array([x[0] for x in tab])
       for j in range(0,M):
              p0_saved[5,j,0] = str(prov[j])
    '''

    nb_figure = figure_subplots(p0_saved, y/deg, height_float, xaxis[:],nb_confi, width, n_index, dt, starting_lat, nb_figure)
    
    nb_figure=nb_figure+1

    
   
 return p0_saved[:,:]*1E-3, nb_figure
