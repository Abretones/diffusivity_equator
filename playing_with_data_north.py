import numpy as np

import constants


#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~ 

def seperate_drifters_at_eq( cross_drift, nb_cross, t_min ):

   drift_northward_pool=np.zeros((nb_cross,t_min,2))
   drift_southward_pool=np.zeros((nb_cross,t_min,2))
   nb_north = np.zeros(( t_min ))
   nb_south = np.zeros(( t_min ))
 
   a=0
   b=0

   for i in range(0,t_min):
       for j in range(0, nb_cross):
           if cross_drift[j,i,0] > 0:
              drift_northward_pool[a,i,:] = cross_drift[j,i,:]
              nb_north[i]=nb_north[i]+1
              a=a+1
           elif cross_drift[j,i,0] < 0:
	      drift_southward_pool[b,i,:] = cross_drift[j,i,:]
              nb_south[i]=nb_south[i]+1
              b=b+1
       a=0
       b=0


   return drift_northward_pool, nb_north, drift_southward_pool, nb_south


def seperate_drifters( cross_drift, nb_cross, t_min ):

   drift_northward_pool_wrong_size=np.zeros((nb_cross,t_min,2))

   a=0
   b=0

   for i in range(0,nb_cross):
       if cross_drift[i,t_min-1,0]>0:
          drift_northward_pool_wrong_size[a,:,:]=cross_drift[i,:,:]
          a=a+1

   drift_northward_pool = drift_northward_pool_wrong_size[0:a-1,:,:]

   return drift_northward_pool, a-1

#----------------------------------------------------------------------------


def mean_one_size(drift_latward_pool, lat_cross, nb_drifter_one_side, t_min):

   mean_velocity = np.zeros((t_min-1))
   mean_position = np.zeros((t_min))
   mean_position[0]=lat_cross

   for i in range(0,t_min-1):
       if nb_drifter_one_side[i] == 0:
          mean_position[i+1] = 0
          mean_velocity[i] = np.nan
       else :
          mean_position[i+1]=np.mean(drift_latward_pool[0:nb_drifter_one_side[i],i+1,0])
          mean_velocity[i]=(mean_position[i+1]-mean_position[i])*constants.deg/(6*3600)


   return mean_velocity, mean_position



#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------


def eulerian_velocity(Lat, Long, velocity, lat_studied, nb_data, nb_drifters, t_max, step, long_e_of_study, long_w_of_study, drog):
  
  if lat_studied < 0:
     factor = -1
  else :
     factor = 1

  MEAN_VELOCITY = 0
  MEAN_OF_SQRD = 0
  nb_drifters_band = 0
  val = 0
  for n in range(0,nb_drifters):
    for t in range(0,int(t_max[n])-1):
        if drog[n,t]==1 and Lat[n,t]>lat_studied and Lat[n,t]<lat_studied+step and Long[n,t]>long_w_of_study and Long[n,t]<long_e_of_study:
           if velocity[n,t+1] != np.nan and velocity[n,t+1]<900:
             MEAN_OF_SQRD = MEAN_OF_SQRD + (1E-2*velocity[n,t+1])**2
             MEAN_VELOCITY = MEAN_VELOCITY + velocity[n,t+1]*1E-2
             nb_drifters_band=nb_drifters_band+1
           else :
             val=val+1
  print val
  
  if nb_drifters_band == 0 :
     MEAN_VELOCITY = np.nan
     MEAN_OF_SQRD = np.nan
  else :
     MEAN_VELOCITY = MEAN_VELOCITY/nb_drifters_band
     MEAN_OF_SQRD = MEAN_OF_SQRD/nb_drifters_band

  ste = factor * 1.96*np.sqrt(MEAN_OF_SQRD-MEAN_VELOCITY**2) /np.sqrt(nb_drifters_band/20)
      
  return MEAN_VELOCITY, ste+MEAN_VELOCITY, nb_drifters_band

#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def eulerian_velocity_drogued_drifters(Lat, velocity, lat_studied,step, nb_data):

  if lat_studied < 0:
     factor = -1
  else :
     factor = 1

  MEAN_VELOCITY = 0
  MEAN_OF_SQRD = 0
  nb_drifters_band = 0
  val = 0
  for n in range(0,nb_data):
        if Lat[n]>lat_studied and Lat[n]<lat_studied+step :
           if velocity[n] != np.nan and velocity[n]<900:
             MEAN_OF_SQRD = MEAN_OF_SQRD + (1E-2*velocity[n])**2
             MEAN_VELOCITY = MEAN_VELOCITY + velocity[n]*1E-2
             nb_drifters_band=nb_drifters_band+1
           else :
             val=val+1
  
  if nb_drifters_band == 0 :
     MEAN_VELOCITY = np.nan
     MEAN_OF_SQRD = np.nan
  else :
     MEAN_VELOCITY = MEAN_VELOCITY/nb_drifters_band
     MEAN_OF_SQRD = MEAN_OF_SQRD/nb_drifters_band

  ste = factor * 1.96*np.sqrt(MEAN_OF_SQRD-MEAN_VELOCITY**2) /np.sqrt(nb_drifters_band/20)
 
  return MEAN_VELOCITY, ste+MEAN_VELOCITY, nb_drifters_band


#----------------------------------------------------------------------------
#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  
#----------------------------------------------------------------------------

def mean_drifters_in_bins(Lat,nb_drifter,index_lat_max, width):

        nb_bar = int(index_lat_max/width)
        x=np.linspace( 0, index_lat_max, nb_bar+1 )        
        bins=np.zeros((nb_bar+1))
        
        
        for i in range(0,nb_drifter):
           index = np.where(np.logical_and(Lat[i]>=x, Lat[i]<x+width))
           bins[index]=bins[index]+1

        mean = np.sum(bins*x)/(nb_drifter-1)

        return mean


#----------------------------------------------------------------------------

def approx_gaussian(Lat1, Lat2, y, MEAN1, MEAN2, kappa1,kappa2, time_index):

	
        y = y * constants.deg
        MEAN1 = MEAN1 *constants.deg
        MEAN2 = MEAN2 *constants.deg

        gaussian1 = (1/np.sqrt(4*np.pi*kappa1 *time_index*6*3600))*np.exp( -(y-MEAN1)**2/(4*kappa1 *time_index*6*3600))
        y_deriv_gaussian1 = (1/np.sqrt(4*np.pi*kappa1 *time_index*6*3600))*np.exp( -(y-MEAN1)**2/(4*kappa1 *time_index*6*3600)) *(-(y-MEAN1)*2/(4*kappa1 *time_index*6*3600))*constants.deg
        gaussian2 = (1/np.sqrt(4*np.pi*kappa2 *time_index*6*3600))*np.exp( -(y-MEAN2)**2/(4*kappa2 *(time_index+1)*6*3600))
        time_derivative = np.exp( -(y-MEAN1)**2/(4*kappa1 *time_index*6*3600))*((1/np.sqrt(4*np.pi*kappa1 *time_index*6*3600))*(y-MEAN1)**2/(4*kappa1 *(time_index*6*3600)**2)- (1/(2*np.sqrt(4*np.pi*kappa1 *time_index*6*3600)))*4*np.pi*kappa1*4*(1/(4*np.pi*kappa1 *time_index*6*3600)) )
        #time_derivative = (gaussian2-gaussian1)/(6*3600)

        return gaussian1, y_deriv_gaussian1, time_derivative

def integrate(time_derivative,dy,i,nb_bar):
    integral = 0
    for i in range(i, nb_bar+1):
        integral=integral+time_derivative[i]*dy

    return integral

#----------------------------------------------------------------------------


def velocity_pic(Lat, nb_cross, x, lat_cross, nb_bar, t_min, fact, width):

   height = np.zeros((nb_bar+1,t_min))
   position_pic_n = np.zeros((t_min))
   position_pic_s = np.zeros((t_min))
   velocity_n = np.zeros((t_min-1))
   velocity_s = np.zeros((t_min-1))

   if lat_cross > 0:
      position_pic_n[0]=lat_cross
      position_pic_s[0]=np.nan
      
   else :
      position_pic_s[0]=lat_cross
      position_pic_n[0]=np.nan
   
   for j in range(1,t_min):   
    for i in range(0,nb_cross):
       index = np.where(np.logical_and(Lat[i,j*fact]>=x, Lat[i,j*fact]<x+width))
       height[index,j]=height[index,j]+1
    height[:,j] = height[:,j]/(nb_cross*width*constants.deg)

    index = np.where(height[int(nb_bar/2):nb_bar,j] == np.max(height[int(nb_bar/2):nb_bar,j]))[0]
    
    pic_s = np.where(np.max(height[0:int(nb_bar/2)-1,j]))
    if x[index[0]] != 0:
       position_pic_n[j] = x[index[0]]+20
       velocity_n[j-1] = (position_pic_n[j]-position_pic_n[j-1] )*constants.deg/(fact*6*3600)
       
    else :
       velocity_n[j-1] = np.nan
    #velocity_s = (np.where(np.max(height[int(nb_bar/2):nb_bar,j]))-position_pic_n )*constants.deg/(6*3600)

   return position_pic_n[1:], velocity_n

