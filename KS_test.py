import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')


from mpl_toolkits.basemap import Basemap
from matplotlib.ticker import NullFormatter,MultipleLocator, FormatStrFormatter
import matplotlib.gridspec as gridspec
import scipy 
import pylab as pl
from netCDF4 import Dataset
from scipy.interpolate import Rbf
import sys
import os
from matplotlib.lines import Line2D
import constants

#____________________________________________________________________
def fc(a,b,y,dy):
 f = np.zeros((200))
 coeff = (b-a)/(y[-1]-y[0])
 f[0]=a
 for i in range(1,200):
  f[i] = f[i-1]+coeff*dy
 return f

#____________________________________________________________________
def reading_distrib(lat_cross,n_index,factor,width,nb_points,nb_distrib):

   height_float = np.zeros((nb_points+1,nb_distrib))

   if width == 0.1:
      folder = '../master_project/plots_distrib/'
   elif width == 0.5:
      folder = 'plots_distrib/'

   for j in range(0,nb_distrib):
   
     txt_file = str(folder)+'real_distribution_at_'+str(int(n_index[j]*6/(24*factor)))+'_days_starting_at_'+str(lat_cross)+'.txt'

     with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])
     
       for i in range(0,nb_points+1):
         height_float[i,j] = str(height[i])

   return height_float






#____________________________________________________________________
def plotting_distrib_plus(p0_0,p0_4, y_p0, height_0,height_4,y, index_lat_max, width, kappa, velocity, index_t, dt, starting_lat, nb_figure,nb_test,KS_prob,day):

 
   fig=plt.subplots(1,2,figsize=(14,22))
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')


   plt.title('t='+str(index_t*dt)+' days',fontsize=10)

   '''
   plt.subplot(311)
   plt.xlim([-15,15])
   plt.yticks([0,0.1,0.2,0.3])
   pl.ylim([0,0.35])
   tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
   tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
   plt.xticks(tick_locs, tick_lbls)
   plt.ylabel(r'Normalized distributions',size=14)
   plt.bar(y, height_4/trap(height_4,width), width, color='#F0E68C',edgecolor='#FFD700')
   #plt.bar(y, height_0/trap(height_0,width), width, color='none',edgecolor='#FF8C00',hatch='//')
   plt.plot(y_p0,p0_4/trap(p0_4,1./200),'m' ,label='numerical distribution')
   #plt.plot(y_p0,p0_0/trap(p0_0,1./200),'--m' )
   '''
   #plt.legend(prop={'size':6},bbox_to_anchor=(1.1, 1.05))

   plt.subplot(211)
   tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
   tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
   plt.xticks(tick_locs, tick_lbls)
   plt.yticks([0,10000,20000,30000,40000])
   plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   pl.xlim([-15,15]) 
   plt.xlabel(r'Latitudes',size=14)
   plt.ylabel(r'Diffusivity (m$^{2}$.s$^{-1}$)',size=14)
   plt.plot(y_p0, kappa,'m')  

   plt.subplot(212)
   tick_locs = [10-1,20-1,30-1,40-1]
   tick_lbls = [r'10',r'20',r'30',r'40']
   plt.yticks([0,0.2,0.4,0.6,0.8,1])
   plt.ylim([0,1])
   plt.xticks(tick_locs, tick_lbls)
   plt.plot(KS_prob)
   plt.ylabel(r'KS-probability',size=14)
   #plt.plot([day-1,day-1],[0,np.max(KS_prob)],'r',linewidth=2)  
   plt.xlabel(r'days',size=14)


   plt.subplots_adjust(top=0.95, bottom=0.05, left=0.15, right=0.9, hspace=0.2, wspace=0.7)
   plt.savefig('tests'+str(starting_lat)+'/distrib_'+str(int(index_t*dt))+'_starting_at_'+str(int(starting_lat))+'_try-'+str(nb_test)+'.png',format='png')




   nb_figure = nb_figure+2
   return nb_figure

#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx
  yo = np.sum(v)

  return yo


#____________________________________________________________________
def adrhs(p,v,k,y,dy):
#
# RHS of the 1D Adv-Diff equation for absolute dispersion
#
  gp=np.gradient(p,dy)
  y=-np.gradient(v*p,dy) + np.gradient(k*gp,dy)

  return y

#____________________________________________________________________
def distribution_6_different_times(y, dy, ys, py, nb_points, nb_time, dt, factor, n_index, v, kappa,nb_distrib):
 p0 = np.zeros((nb_points))


 p0 = (1/(np.sqrt(2*np.pi)*py))*np.exp(-(((y-ys)/py)**2)/2) #initial PDF
 mean=np.zeros((nb_distrib))
 velocity=np.zeros((nb_distrib))

 p0_saved = np.zeros((nb_distrib,nb_points))
 p2 = np.zeros((nb_points))
 cnt =0
 
 for j in range(0,int(nb_time)):
   k1 = adrhs(p0,v,kappa,y,dy)
   k2 = adrhs(p0+k1*dt/2,v,kappa,y,dy)
   k3 = adrhs(p0+k2*dt/2,v,kappa,y,dy)
   k4 = adrhs(p0+k3*dt,v,kappa,y,dy)
   p0=p0+dt/6*(k1+2*k2+2*k3+k4)   

   if cnt <nb_distrib and j == int(n_index[cnt]-1):
     p0_saved[cnt,:] = np.copy(p0) #/trap(p2,dy)
     if cnt ==0:
        p2= np.copy(p0_saved[cnt,:])
        p2[np.where(y<0)]=0
        mean[cnt]=trap(y*p2,dy)/trap(p2,dy) 
        velocity[cnt]=(mean[cnt]-ys)*1E3/(24*3600)
     else:
        p2= np.copy(p0_saved[cnt,:])
        p2[np.where(y<0)]=0
        mean[cnt]=trap(y*p2,dy)/trap(p2,dy) 
        velocity[cnt]=(mean[cnt]-mean[cnt-1])*1E3/(24*3600)
     cnt=cnt+1
     print 'hey'
   



 return p0_saved, mean, velocity

#____________________________________________________________________

def KS_stat(height_float, p0_saved, n_index, dt , nb_points, M, y,y2, width):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


   if width == 0.1:
    delta_N[0:19]=(height_float[0])/(trap(height_float[:],1.))
    for i in range(1,nb_points+1):
      delta_N[20*i:20*i+20]=delta_N[20*(i-1)]+(height_float[i])/trap(height_float,1.)

   elif width ==0.5:
    delta_N[0:99]=(height_float[0])/(trap(height_float[:],1.))
    for i in range(1,nb_points+1):
      delta_N[100*i:100*i+100]=delta_N[100*(i-1)]+(height_float[i])/trap(height_float,1.)



   F_p[0]=p0_saved[0]/trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/trap(p0_saved,1.)
      diff[i] = abs(delta_N[i]-F_p[i])
   '''
   plt.figure(nb_figure)
   plt.plot(y,F_p,y,delta_N)
   plt.savefig('plots_KS_test/diff_accumulative_distribution_fc_'+str(n_index*dt)+'days.png')
   nb_figure=nb_figure+1
   '''
   return np.max(diff)

#____________________________________________________________________
def Q_KS(Lambda):
  

  Q = 0

  
  for j in range(1,300):
 
       Q = Q + ((-1)**(j-1))*np.exp(-2*(j*Lambda)**2)


  return 2*Q


#____________________________________________________________________

def KS_variant(height_float, p0_saved, n_index, dt , nb_points, M, y):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


 
   delta_N[0:99]=(height_float[0])/(trap(height_float[:],1))
   for i in range(1,nb_points+1):
      delta_N[100*i:100*i+100]=delta_N[100*(i-1)]+(height_float[i])/trap(height_float,1)



   F_p[0]=p0_saved[0]/trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/trap(p0_saved,1.)
      if F_p[i]==1:
         diff[i]=0
      else :
         diff[i] = abs(delta_N[i]-F_p[i])/np.sqrt(F_p[i]*(1-F_p[i]))


   return np.max(diff)

#____________________________________________________________________
def Chi_stat(height_float, p0_saved, nb_points, M, nb_floats):

   diff_sqrd = np.zeros((nb_points+1))
   p0_in_bin = np.zeros((nb_points+1))

   for i in range(0,nb_points+1):
      p0_in_bin[i]=np.mean(p0_saved[i*100:(i+1)*100])
      if height_float[i]==0:
         diff_sqrd[i]=0
      else:
         diff_sqrd[i]=((p0_in_bin[i]-height_float[i])**2)/height_float[i]


   return np.sqrt(np.sum(diff_sqrd)),p0_in_bin





#//////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////////////
#____________________________________________________________________
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

def distrib_at_n( nb_test,starting_lat, index_snapshot, option_testing_different_diff, option_superimpose_real_distrib, lat_limit_south, width, nb_distrib,option05,nb_figure):

 deg=110.567

 dy = 1./200        # grid spacing, in degres
 y_max = 20.
 y  = np.linspace(-y_max, y_max, 2*y_max/dy+1)
 print len(y)
 dy = dy*deg
 y_max = y_max*deg
 y = y*deg
 M = len(y)

 t0 = 0 					# starting time
 dt = 0.0001				# time step in day
 factor = 2500#150/(600*dt)
 N = 4*(nb_distrib+1)*factor#4*3*factor#501*factor			# total integration (N*dt)
 

 
 #////////////////////////////////////////////////////////////////////////////////////////////
 # #-------------------------------------------------------------------------------------------
 v = np.zeros((M))


 #Eulerian velocity

 
 #v=0.02*np.tanh(y/50)+0.08*np.exp(-((y-300)/400)**2)-0.08*np.exp(-((y+300)/400)**2)
 
 # fit velocity drogued and undrogued drifters
 
 v[0:int(M/2)-1] = -(0.01*pl.tanh((-y[0:int(M/2)-1]/deg-0.3)/0.2)+0.02*np.exp(-((-y[0:int(M/2)-1]/deg-1)/4)**2)-0.02*np.tanh((-y[0:int(M/2)-1]/deg-13)/12) +0.023*pl.tanh((-y[0:int(M/2)-1]/deg)/0.2) )
 

 v[int(M/2):M] = (0.03*pl.tanh(y[int(M/2):M]/(deg*0.5))+0.035*np.exp(-((y[int(M/2):M]/deg-3)/2.)**2)-0.001*np.exp(-((y[int(M/2):M]/deg+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.2)- 0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.21) +0.011*pl.tanh((y[int(M/2):M]/deg-9.5)/1.5) - 0.029*pl.tanh((y[int(M/2):M]/deg-14.5)/3.3)  -0.008 #- 0.04*pl.tanh((y[int(M/2)+1:M-1]/deg-1)/1.6)+0.04



 plt.figure(nb_figure)
 pl.xlim([-y_max/deg, y_max/deg])

 plt.plot(y/deg,v)
 units = 'm.s$^{-1}$'
 plt.ylim([-0.15,0.15])
 plt.ylabel('velocity ('+str(units)+')')
 plt.xlabel('latitude')
 plt.savefig('plots_adv_diff/Eulerian_velocity_try.png')
 nb_figure=nb_figure+1
 
 v=v*86.4 #km/day


 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 kappa = np.zeros((M,300))

 kappa_mean_all = np.zeros((M))
 error_all = np.zeros((M))
 nb_points_between_kappa_values = int(200)
 equator_index = int(M/2)

 '''
 kappa_values2=np.array([48385, 4245, 35421, 36919, 12245, 3395, 7470, 8545, 20967, 10879, 3793, 39482, 6132, 20361, 27215, 27215])
 kappa[0:equator_index-7*nb_points_between_kappa_values,0]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),0] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,0]=kappa_values2[-1]



 kappa_values2=np.array([48385, 4245, 35421, 36919, 12245, 3395, 7470, 8545, 20967, 10879, 3793, 39482, 6132, 20361, 27215, 27215])
 kappa[0:equator_index-7*nb_points_between_kappa_values,1]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),1] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,1]=kappa_values2[-1]

 # 3N - 1st

 # 3N - 2nd
 kappa_values2=np.array([40327, 29454, 3673, 24604, 38941, 49316, 7470, 8545, 20967, 10879, 3793, 39482, 33242, 8618, 30348,30348])
 kappa[0:equator_index-7*nb_points_between_kappa_values,2]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),2] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,2]=kappa_values2[-1]


 # 6N - 1st
 kappa_values2=np.array([40327, 29454, 3673, 24604, 38941, 49316, 7470, 8545, 20967, 10879, 3793, 39482, 15542, 8618, 30348,30348])
 kappa[0:equator_index-7*nb_points_between_kappa_values,3]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),3] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,3]=kappa_values2[-1]



 # 1N - 2nd 
 kappa_values2=np.array([40327, 21097, 3673, 24604, 38941, 49316, 7470, 8545, 20967, 10879, 3793, 39482, 15542, 8618, 30348, 30348])
 kappa[0:equator_index-7*nb_points_between_kappa_values,4]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),4] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,4]=kappa_values2[-1]

 # 1N - 3rd 
 kappa_values2=np.array([27988, 4245, 35421, 36919, 12245, 49316, 7470, 8545, 20967, 10879, 3793, 39482, 15542, 8618, 30348, 30348])
 kappa[0:equator_index-7*nb_points_between_kappa_values,5]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),5] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,5]=kappa_values2[-1]


 # 2N - 2nd 
 kappa_values2=np.array([24014, 41803, 11326, 6811, 12245, 3594, 7470, 8545, 14359, 10879, 3793, 30386, 22980, 8618, 5602, 5602])
 kappa[0:equator_index-7*nb_points_between_kappa_values,6]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),6] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,6]=kappa_values2[-1]

 # 2N - 3rd 
 kappa_values2=np.array([40327, 21097, 24480, 6811, 29629, 9435, 33733, 39215, 27583, 5240, 3793, 39482, 20441, 8618, 30348, 30348])
 kappa[0:equator_index-7*nb_points_between_kappa_values,7]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),7] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,7]=kappa_values2[-1]

 # 3N - 3rd
 kappa_values2=np.array([40925, 34123, 11326, 6811, 29629, 9435, 33733, 39215, 27583, 5240, 3793, 30386, 22980, 8618, 18198, 18198])
 kappa[0:equator_index-7*nb_points_between_kappa_values,8]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),8] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,8]=kappa_values2[-1]


 # 3N - 4
 kappa_values2=np.array([40925, 34123, 11326, 6811, 29629, 9435, 33733, 22908, 27583, 5240, 3793, 30386, 22980, 8618, 18198, 18198])
 kappa[0:equator_index-7*nb_points_between_kappa_values,9]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),9] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,9]=kappa_values2[-1]



 # 6N - 2 exp 
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 19213, 5983, 39215, 27583, 5240, 3793, 43065, 15542, 8618, 14396, 14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,10]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),10] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,10]=kappa_values2[-1]

 # 6N - "3rd"
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 19213, 5983, 39215, 27583, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,11]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),11] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,11]=kappa_values2[-1]

 nb=11
 nb=nb+1
 kappa_values2=np.array([40327, 21097, 24480, 6811, 29629, 9435, 33733, 39215, 27583, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 9435, 33733, 11807, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 9435, 33733, 11807, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 38525, 9435, 33733, 11807, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 9435, 33733, 24035, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 29629, 27401, 5983, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 6811, 38525, 9435, 42814, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 29445, 41263, 9435, 42814, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]


 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 29445, 41263, 9435, 42814, 39215, 42479, 5240, 3793, 43065, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]


 nb=nb+1
 kappa_values2=np.array([40925, 21252, 38944, 21331, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 38944, 21331, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 19965, 38944, 21331, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([13767, 21252, 11633, 6811, 38525, 9435, 33733, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]


 nb=nb+1
 kappa_values2=np.array([13767, 21252, 11633, 6811, 38525, 9435, 33733, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([13767, 21252, 15597, 6811, 38525, 9435, 33733, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 14396,  14396])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 19965, 38944, 21331, 38525, 9435, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 19965, 38944, 21331, 38525, 9782, 33733, 39215, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 19965, 38944, 21331, 38525, 9435, 33733, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]


 nb=nb+1
 kappa_values2=np.array([40925, 19965, 38944, 21331, 38525, 9435, 33733, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 29629, 13861, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]


 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 29629, 40306, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 38667, 11326, 21331, 29629, 40306, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 38667, 11326, 21331, 29629, 40306, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 29629, 32208, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 29629, 36257, 43121, 43305, 42479, 5240, 3793, 43985, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 40099, 13861, 43121, 43305, 42479, 5240, 3793, 44979, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 40099, 13861, 43121, 43305, 42479, 5240, 3793, 44979, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]

 nb=nb+1
 kappa_values2=np.array([40925, 21252, 11326, 21331, 40099, 44217, 43121, 43305, 42479, 5240, 3793, 44979, 15542, 8618, 12865,  12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 '''
 nb=0
 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 10072, 21292, 41075, 46479, 37204, 38893, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 10072, 21292, 41075, 46479, 37204, 38893, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 10072, 21292, 41075, 46479, 37204, 38893, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 10072, 21292, 41075, 46479, 37204, 42936, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([21994, 43752, 13482, 12998, 6058, 37149, 25781, 10072, 21292, 41075, 46479, 37204, 38893, 13232, 39852, 39852])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 13482, 12998, 6058, 37149, 25781, 9258, 11633, 30332, 46479, 37204, 42936, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 13482, 12998, 6058, 37149, 25781, 9258, 11633, 30332, 46479, 37204, 42936, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 9258, 11633, 30332, 46479, 37204, 42936, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 26142, 10977, 17321, 19073, 47563, 9258, 11633, 30332, 46479, 37204, 42936, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1

 kappa_values2=np.array([27335, 21009, 13482, 12998, 6058, 37149, 47563, 9258, 11633, 30332, 46479, 37204, 38893, 13232, 14651, 14651])
 kappa[0:equator_index-7*nb_points_between_kappa_values,nb]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),nb] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,nb]=kappa_values2[-1]
 nb=nb+1


 #/////////////////////////////////////////////////////////////////
 kappa[0:int(M/2)-2*200,40]=20000
 kappa[int(M/2)-2*200:int(M/2),40] = 6000+(y[int(M/2)-2*200:int(M/2)]-y[int(M/2)-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+300,40] = 6000
 kappa[int(M/2)+300:int(M/2)+200*2+100+1,40]=6000+(y[int(M/2)+300:int(M/2)+200*2+100+1]-y[int(M/2)-1+300])*(20000-6000)/(y[int(M/2)+200*2+100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*2+100:int(M/2)+200*3+1,40]=20000+(y[int(M/2)+200*2+100:int(M/2)+200*3+1]-y[int(M/2)-1+200*2+100])*(40000-20000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*2+100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,40] = 40000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-40000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,40] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,40] = 5000

 kappa[0:int(M/2)-2*200,41]=20000
 kappa[int(M/2)-2*200:int(M/2),41] = 6000+(y[int(M/2)-2*200:int(M/2)]-y[int(M/2)-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+300,41] = 6000
 kappa[int(M/2)+300:int(M/2)+200*2+100+1,41]=6000+(y[int(M/2)+300:int(M/2)+200*2+100+1]-y[int(M/2)-1+300])*(20000-6000)/(y[int(M/2)+200*2+100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*2+100:int(M/2)+200*3+1,41]=20000+(y[int(M/2)+200*2+100:int(M/2)+200*3+1]-y[int(M/2)-1+200*2+100])*(45000-20000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*2+100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,41] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,41] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,41] = 5000

 kappa[0:int(M/2)-2*200,42]=20000
 kappa[int(M/2)-2*200:int(M/2),42] = 6000+(y[int(M/2)-2*200:int(M/2)]-y[int(M/2)-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+300,42] = 6000
 kappa[int(M/2)+300:int(M/2)+200*3+1,42]=6000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-4000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,42] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,42] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-36000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,42] = 5000

 kappa[0:int(M/2)-2*200,43]=20000
 kappa[int(M/2)-2*200:int(M/2),43] = 4000+(y[int(M/2)-2*200:int(M/2)]-y[int(M/2)-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+350,43] = 4000
 kappa[int(M/2)+350:int(M/2)+200*3+1,43]=4000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-4000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,43] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,43] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,43] = 5000

 kappa[0:int(M/2)-2*200,44]=20000
 kappa[int(M/2)-2*200:int(M/2),44] = 4000+(y[int(M/2)-2*200:int(M/2)]-y[int(M/2)-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+350,44] = 4000
 kappa[int(M/2)+350:int(M/2)+200*3+1,44]=4000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(47000-4000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,44] = 47000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-47000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,44] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,44] = 5000

 kappa[0:int(M/2)-2*200,45]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,45] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+350+1,45] = 6000+(y[int(M/2)+200:int(M/2)+350+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+350]-y[int(M/2)+200-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,45]=8000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,45] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,45] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,45] = 5000

 kappa[0:int(M/2)-2*200,46]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,46] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+450+1,46] = 6000+(y[int(M/2)+200:int(M/2)+450+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+450]-y[int(M/2)+200-1])
 kappa[int(M/2)+450:int(M/2)+200*3+1,46]=8000+(y[int(M/2)+450:int(M/2)+200*3+1]-y[int(M/2)-1+450])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+450])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,46] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,46] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,46] = 5000
 '''
 kappa[0:int(M/2)-14*100,47] = 6000
 valeurs=np.array([8450,8250,8450,8700,8650,9050,9150,9550,10450,10900,11350,15000,10000,10000, 10000,8950,7200,7400,7850,7850,8950,8850,8650,7200,4150,4000,2650,1500,1150]) 
 for i in range(0,28):
   kappa[int(M/2)-14*100+i*100:int(M/2)-14*100+i*100+100,47] = fc(valeurs[i],valeurs[i+1],y[int(M/2)-14*100+i*100:int(M/2)-14*100+i*100+100],dy/2)
 kappa[int(M/2)+14*100:M,47]=50
 '''
 kappa[0:int(M/2)-2*200,48]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,48] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+350+1,48] = 6000+(y[int(M/2)+200:int(M/2)+350+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+350]-y[int(M/2)+200-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,48]=8000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,48] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,48] = 45000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-45000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,48] = 5000


 kappa[0:int(M/2)-2*200,49]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,49] = 5000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-5000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+350+1,49] = 5000+(y[int(M/2)+200:int(M/2)+350+1]-y[int(M/2)+200-1])*(8000-5000)/(y[int(M/2)+350]-y[int(M/2)+200-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,49]=8000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,49] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,49] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,49] = 5000


 kappa[0:int(M/2)-2*200,50]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,50] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+350+1,50] = 6000+(y[int(M/2)+200:int(M/2)+350+1]-y[int(M/2)+200-1])*(9000-6000)/(y[int(M/2)+350]-y[int(M/2)+200-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,50]=9000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-9000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,50] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,50] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,50] = 5000


 kappa[0:int(M/2)-2*200,51]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,51] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,51] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,51]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,51] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,51] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,51] = 5000

 kappa[0:int(M/2)-2*200,52]=20000
 kappa[int(M/2)-2*200:int(M/2)+150,52] = 6000+(y[int(M/2)-2*200:int(M/2)+150]-y[int(M/2)+150-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+150-1])
 kappa[int(M/2)+150:int(M/2)+350+1,52] = 6000+(y[int(M/2)+150:int(M/2)+350+1]-y[int(M/2)+150-1])*(8000-6000)/(y[int(M/2)+350]-y[int(M/2)+150-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,52]=8000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,52] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,52] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,52] = 5000

 kappa[0:int(M/2)-2*200,53]=20000
 kappa[int(M/2)-2*200:int(M/2)+150,53] = 6000+(y[int(M/2)-2*200:int(M/2)+150]-y[int(M/2)+150-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+150-1])
 kappa[int(M/2)+150:int(M/2)+300+1,53] = 6000+(y[int(M/2)+150:int(M/2)+300+1]-y[int(M/2)+150-1])*(9000-6000)/(y[int(M/2)+300]-y[int(M/2)+150-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,53]=9000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-9000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,53] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,53] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,53] = 5000

 kappa[0:int(M/2)-2*200,54]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,54] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,54] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,54]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,54] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,54] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,54] = 5000

 kappa[0:int(M/2)-2*200,55]=20000
 kappa[int(M/2)-2*200:int(M/2)+250,55] = 6000+(y[int(M/2)-2*200:int(M/2)+250]-y[int(M/2)+250-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+250-1])
 kappa[int(M/2)+250:int(M/2)+300+1,55] = 6000+(y[int(M/2)+250:int(M/2)+300+1]-y[int(M/2)+250-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+250-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,55]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,55] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,55] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,55] = 5000

 kappa[0:int(M/2)-2*200,56]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,56] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+350+1,56] = 6000+(y[int(M/2)+200:int(M/2)+350+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+350]-y[int(M/2)+200-1])
 kappa[int(M/2)+350:int(M/2)+200*3+1,56]=8000+(y[int(M/2)+350:int(M/2)+200*3+1]-y[int(M/2)-1+350])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+350])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,56] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,56] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,56] = 5000

 kappa[0:int(M/2)-2*200,57]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,57] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,57] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,57]=9000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-9000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,57] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,57] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,57] = 5000

 kappa[0:int(M/2)-2*200,58]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,58] = 6500+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6500)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,58] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,58]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,58] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,58] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,58] = 5000

 kappa[0:int(M/2)-2*200,59]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,59] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,59] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(7500-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,59]=7500+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-7500)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,59] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,59] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,59] = 5000

 kappa[0:int(M/2)-2*200,60]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,60] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,60] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,60]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(46000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,60] = 46000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-46000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,60] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,60] = 5000

 kappa[0:int(M/2)-2*200,61]=20000
 kappa[int(M/2)-2*200:int(M/2)+200,61] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,61] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,61]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(44000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,61] = 44000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-44000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,61] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,61] = 5000

 kappa[0:int(M/2)-2*200,62]=19000
 kappa[int(M/2)-2*200:int(M/2)+200,62] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(19000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,62] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,62]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,62] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,62] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,62] = 5000

 kappa[0:int(M/2)-2*200,63]=21000
 kappa[int(M/2)-2*200:int(M/2)+200,63] = 6000+(y[int(M/2)-2*200:int(M/2)+200]-y[int(M/2)+200-1])*(21000-6000)/(y[int(M/2)-2*200]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,63] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,63]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,63] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,63] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,63] = 5000

 kappa[0:int(M/2)-2*200+100,64]=20000
 kappa[int(M/2)-2*200+100:int(M/2)+200,64] = 6000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(20000-6000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,64] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,64]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,64] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,64] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,64] = 5000

 kappa[0:int(M/2)-2*200+100,65]=21000
 kappa[int(M/2)-2*200+100:int(M/2)+200,65] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(21000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,65] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,65]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,65] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,65] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,65] = 5000

 kappa[0:int(M/2)-2*200+100,66]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,66] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,66] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,66]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,66] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,66] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,66] = 5000

 kappa[0:int(M/2)-2*200+100,67]=21000
 kappa[int(M/2)-2*200+100:int(M/2)+100,67] =6600+(y[int(M/2)-2*200+100:int(M/2)+100]-y[int(M/2)+100-1])*(21000-6600)/(y[int(M/2)-2*200+100]-y[int(M/2)+100-1])
 kappa[int(M/2)+100:int(M/2)+200,67] = 6500+(y[int(M/2)+100:int(M/2)+200]-y[int(M/2)+200-1])*(6600-6500)/(y[int(M/2)+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,67] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,67]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,67] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,67] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,67] = 5000

 kappa[0:int(M/2)-2*200+100,68]=21000
 kappa[int(M/2)-2*200+100:int(M/2)+200,68] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(21000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,68] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,68]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,68] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,68] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,68] = 5000

 kappa[0:int(M/2)-2*200+100,69]=21000
 kappa[int(M/2)-2*200+100:int(M/2)+250,69] = 6500+(y[int(M/2)-2*200+100:int(M/2)+250]-y[int(M/2)+250-1])*(21000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+250-1])
 kappa[int(M/2)+250:int(M/2)+300+1,69] = 6500+(y[int(M/2)+250:int(M/2)+300+1]-y[int(M/2)+250-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+250-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,69]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,69] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,69] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,69] = 5000

 kappa[0:int(M/2)-2*200+100,70]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,70] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,70] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,70]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,70] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,70] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,70] = 5000


 kappa[0:int(M/2)-2*200+100,71]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+1,71] = 12000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-1])*(24000-12000)/(y[int(M/2)-2*200+100]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+200,71] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(12000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,71] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,71]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,71] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,71] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,71] = 5000


 kappa[0:int(M/2)-2*200+100,72]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,72] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,72] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,72]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(40000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,72] = 40000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-40000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,72] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,72] = 5000


 kappa[0:int(M/2)-2*200+100,73]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,73] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,73] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,73]=9000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-9000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,73] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,73] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,73] = 5000


 kappa[0:int(M/2)-2*200+100,74]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,74] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,74] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(7500-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,74]=7500+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-7500)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,74] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,74] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,74] = 5000


 kappa[0:int(M/2)-2*200+100,75]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+1,75] = 6000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-1])*(24000-6000)/(y[int(M/2)-2*200+100]-y[int(M/2)-1])
 kappa[int(M/2):int(M/2)+200,75] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(6000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,75] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,75]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,75] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,75] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,75] = 5000

 kappa[0:int(M/2)-2*200+100,76]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,76] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,76] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,76]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(25000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,76]=25000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-25000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,76] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,76] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,76] = 5000

 kappa[0:int(M/2)-2*200+100,77]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,77] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,77] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,77]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(32000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,77]=32000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-32000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,77] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,77] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,77] = 5000

 kappa[0:int(M/2)-2*200+100,78]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,78] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,78] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,78]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,78]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,78] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,78] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,78] = 5000

 kappa[0:int(M/2)-2*200+100,79]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,79] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,79] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,79]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,79]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(43000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,79] = 43000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-43000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,79] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,79] = 5000

 kappa[0:int(M/2)-2*200+100,80]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,80] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,80] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,80]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,80]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(40000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,80] = 40000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-40000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,80] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,80] = 5000

 kappa[0:int(M/2)-2*200+100,81]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,81] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,81] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,81]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,81]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,81] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,81] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,81] = 5000


 kappa[0:int(M/2)-2*200+100,82]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,82] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,82] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,82]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,82]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,82] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(36000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,82] = 36000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-36000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,82] = 5000


 kappa[0:int(M/2)-2*200+100,83]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,83] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,83] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,83]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,83]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,83] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,83] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(4500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,83] = 4500


 kappa[0:int(M/2)-5*200+100,84]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,84]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,84] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,84] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,84]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(35000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,84]=35000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-35000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,84] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,84] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,84] = 5000

 kappa[0:int(M/2)-5*200+100,85]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,85]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,85] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,85] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,85]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,85]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,85] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,85] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,85] = 5000

 kappa[0:int(M/2)-5*200+100,86]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,86]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,86] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,86] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,86]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,86]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,86] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,86] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,86] = 5000

 kappa[0:int(M/2)-5*200+100,87]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,87]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,87] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,87] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,87]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(40000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,87]=40000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-40000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,87] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,87] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,87] = 5000

 kappa[0:int(M/2)-4*200+100,88]=20000
 kappa[int(M/2)-4*200+100:int(M/2)-2*200+100+1,88]=20000+(y[int(M/2)-4*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-4*200+100-1])*(24000-20000)/(-y[int(M/2)-4*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,88] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,88] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,88]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,88]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,88] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,88] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,88] = 5000


 kappa[0:int(M/2)-5*200+100,89]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,89]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,89] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,89] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,89]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,89]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,89] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,89] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,89] = 5000

 kappa[0:int(M/2)-5*200+100,90]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,90]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,90] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,90] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,90]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,90]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,90] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,90] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,90] = 5000

 kappa[0:int(M/2)-5*200+100,85]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,85]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,85] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,85] = 6500+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,85]=8000+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8000)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,85]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,85] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,85] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,85] = 5000

 kappa[0:int(M/2)-5*200+100,91]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,91]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,91] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,91] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,91]=8500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,91]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,91] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,91] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,91] = 5000

 kappa[0:int(M/2)-5*200+100,92]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,92]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(25500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,92] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(25500-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,92] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,92]=9500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-9500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,92]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,92] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,92] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,92] = 5000

 kappa[0:int(M/2)-5*200+100,93]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,93]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(25500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,93] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(25500-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,93] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,93]=9500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-9500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,93]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(43500-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,93] = 43500 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-43500)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3]) 
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,93] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,93] = 5000

 kappa[0:int(M/2)-5*200+100,94]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,94]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(25500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,94] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(25500-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,94] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,94]=9500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-9500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,94]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(43500-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,94] = 43500 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(36500-43500)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3]) 
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,94] = 36500+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-36500)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,94] = 5000

 kappa[0:int(M/2)-5*200+100,95]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,95]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(25500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,95] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(25500-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,95] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(9500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,95]=9500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-9500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,95]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(43500-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,95] = 43500 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(36500-43500)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,95] = 36500+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-36500)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,95] = 3500


 kappa[0:int(M/2)-5*200+100,98]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,98]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,98] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,98] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,98]=8500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(38500-8500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,98]=38500+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45500-38500)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,98] = 45500 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45500)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,98] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,98] = 5000


 kappa[0:int(M/2)-5*200+100,96]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,96]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,96] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24500-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,96] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,96]=8500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(37000-8500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,96]=37000+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45000-37000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,96] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,96] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,96] = 5000

 kappa[0:int(M/2)-5*200+100,97]=20000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,97]=20000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24500-20000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,97] = 7000+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-7000)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,97] = 7000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8500-7000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3-100+1,97]=8500+(y[int(M/2)+300:int(M/2)+200*3-100+1]-y[int(M/2)-1+300])*(38500-8500)/(y[int(M/2)+200*3-100]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3-100:int(M/2)+200*3+1,97]=38500+(y[int(M/2)+200*3-100:int(M/2)+200*3+1]-y[int(M/2)-1+200*3-100])*(45500-38500)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*3-100])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,97] = 45500 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38500-45500)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,97] = 38500+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38500)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,97] = 5000

# tests starting at 2degree. use the 66th test
 kappa[0:int(M/2)-2*200+100,98]=24000
 kappa[int(M/2)-2*200+100:int(M/2)+200,98] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,98] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,98]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,98] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,98] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,98] = 5000

 kappa[0:int(M/2)-200,99]=19342
 valeurs=np.array([19342,14000,7460,8000,45000,38000,5312,5000])
 for i in range(0,7):
   kappa[int(M/2)-200+i*200:int(M/2)-200+i*200+200,99] = fc(valeurs[i],valeurs[i+1],y[int(M/2)-200+i*200:int(M/2)+i*200],dy)
 kappa[int(M/2)+200*6:M,99] = 5000

 kappa[0:int(M/2)-200,100]=24000
 valeurs=np.array([24000,14000,7460,8000,45000,38000,5312,5000])
 for i in range(0,7):
   kappa[int(M/2)-200+i*200:int(M/2)-200+i*200+200,100] = fc(valeurs[i],valeurs[i+1],y[int(M/2)-200+i*200:int(M/2)+i*200],dy)
 kappa[int(M/2)+200*6:M,100] = 5000

# test at -1: use 98th

 kappa[0:int(M/2)-5*200+100,101]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,101]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,101] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,101] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,101]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,101] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,101] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,101] = 5000

 kappa[0:int(M/2)-5*200+100,102]=5000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,102]=5000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-5000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+200,102] = 6500+(y[int(M/2)-2*200+100:int(M/2)+200]-y[int(M/2)+200-1])*(24000-6500)/(y[int(M/2)-2*200+100]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,102] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,102]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,102] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,102] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,102] = 5000

 kappa[0:int(M/2)-5*200+100,103]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,103]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,103]=24000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-24000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,103] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,103] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,103]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,103] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,103] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,103] = 5000

 kappa[0:int(M/2)-5*200+100,104]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,104]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)+1,104]=24000+(y[int(M/2)-3*200+100:int(M/2)+1]-y[int(M/2)-3*200+100-1])*(8000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,104] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,104] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,104]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,104] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,104] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,104] = 5000

 kappa[0:int(M/2)-5*200+100,105]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,105]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(20000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,105]=20000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-20000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,105] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,105] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,105]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,105] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,105] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,105] = 5000

 kappa[0:int(M/2)-5*200+100,106]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,106]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,106]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(9000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,106]=9000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-9000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,106] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,106] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,106]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,106] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,106] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,106] = 5000

 kappa[0:int(M/2)-5*200+100,108]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,108]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,108]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(7000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,108]=7000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-7000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,108] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,108] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,108]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,108] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,108] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,108] = 5000

 kappa[0:int(M/2)-5*200+100,110]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,110]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,110]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,110]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(7000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,110]=7000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-7000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,110] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,110] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,110]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,110] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,110] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,110] = 5000

 kappa[0:int(M/2)-5*200+100,112]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,112]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,112]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,112]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,112]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,112] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,112] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,112]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,112] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,112] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:M,112] = 5000

 kappa[0:int(M/2)-5*200+100,114]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,114]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,114]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(22000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,114]=22000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-22000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,114]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,114] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,114] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,114]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,114] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,114] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,114] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,114] = 5000

 kappa[0:int(M/2)-5*200+100,116]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,116]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,116]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,116]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,116]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,116] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,116] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,116]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,116] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,116] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,116] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,116] = 5000

 kappa[0:int(M/2)-5*200+100,118]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,118]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(14000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,118]=14000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-14000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,118]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,118]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,118] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,118] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,118]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,118] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,118] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,118] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,118] = 5000

 kappa[0:int(M/2)-5*200+100,120]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,120]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+100+1,120]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+100+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,120]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,120]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,120] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,120] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,120]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,120] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,120] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,120] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,120] = 5000

 kappa[0:int(M/2)-5*200+100,122]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,122]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,122]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-200+1,122]=24000+(y[int(M/2)-3*200:int(M/2)-200+1]-y[int(M/2)-3*200-1])*(6000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,122]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,122] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,122] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,122]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,122] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,122] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,122] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,122] = 5000



 kappa[0:int(M/2)-5*200+100,124]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,124]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,124]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-200+1,124]=24000+(y[int(M/2)-3*200:int(M/2)-200+1]-y[int(M/2)-3*200-1])*(6000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,124]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(6000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,124] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(6000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,124] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,124]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,124] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,124] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-30000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,124] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,124] = 5000


 kappa[0:int(M/2)-5*200+100,126]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,126]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,126]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-200+1,126]=24000+(y[int(M/2)-3*200:int(M/2)-200+1]-y[int(M/2)-3*200-1])*(6000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,126]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,126] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,126] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,126]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,126] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,126] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-30000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,126] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,126] = 5000

 kappa[0:int(M/2)-5*200+100,127]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,127]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,127]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-200+1,127]=24000+(y[int(M/2)-3*200:int(M/2)-200+1]-y[int(M/2)-3*200-1])*(6000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,127]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,127] = 5000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-5000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,127] = 5000+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-5000)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,127]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,127] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,127] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,127] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,127] = 5000


 kappa[0:int(M/2)-7*200+100,128]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,128]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,128]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-200+1,128]=24000+(y[int(M/2)-3*200:int(M/2)-200+1]-y[int(M/2)-3*200-1])*(6000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,128] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,128] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,128] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,128]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,128] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,128] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,128] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,128] = 5000

# tests paralleles a 2degree (on part de 105th)

 kappa[0:int(M/2)-7*200+100,129]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,129]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,129]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(21000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,129]=21000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(20000-21000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,129]=20000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-20000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,129] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,129] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,129] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,129]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,129] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,129] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,129] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,129] = 5000

 kappa[0:int(M/2)-7*200+100,130]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,130]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-3*200+1,130]=15000+(y[int(M/2)-5*200:int(M/2)-3*200+1]-y[int(M/2)-5*200-1])*(24000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,130]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,130]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,130] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,130] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,130] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,130]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,130] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,130] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,130] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,130] = 5000

 kappa[0:int(M/2)-7*200+100,131]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,131]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,131]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(18000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,131]=18000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-18000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,131]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(20000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,131]=20000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-20000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,131] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,131] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,131] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,131]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,131] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,131] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,131] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,131] = 5000

 kappa[0:int(M/2)-7*200+100,132]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,132]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,132]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(18000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,132]=18000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-18000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,132]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,132]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,132] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,132] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,132] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,132]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,132] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,132] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,132] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,132] = 5000

 kappa[0:int(M/2)-7*200+100,133]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,133]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,133]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(21000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,133]=21000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-21000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,133]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,133]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,133] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,133] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,133] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,133]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,133] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,133] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,133] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,133] = 5000


 kappa[0:int(M/2)-7*200+100,134]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,134]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(18000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,134]=18000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(21000-18000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,134]=21000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-21000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,134]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,134]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,134] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,134] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,134] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,134]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,134] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,134] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,134] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,134] = 5000

 kappa[0:int(M/2)-7*200+100,135]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,135]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(18000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,135]=18000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(23000-18000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,135]=23000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-23000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,135]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,135]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,135] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,135] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,135] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,135]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,135] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,135] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,135] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,135] = 5000

 kappa[0:int(M/2)-7*200+100,136]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,136]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,136]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(23000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,136]=23000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-23000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,136]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,136]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,136] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,136] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,136] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,136]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,136] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,136] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,136] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,136] = 5000

 kappa[0:int(M/2)-7*200+100,137]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,137]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,137]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(23000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,137]=23000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-23000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,137]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,137]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,137] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(5500-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,137] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(5500-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,137] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,137]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,137] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,137] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,137] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,137] = 5000

 kappa[0:int(M/2)-7*200+100,138]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,138]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,138]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,138]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,138]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,138]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,138] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,138] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,138] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,138]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,138] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,138] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,138] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,138] = 5000

 kappa[0:int(M/2)-7*200+100,139]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,139]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,139]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,139]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,139]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,139]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(7500-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,139] = 7500+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-7500)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,139] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,139] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,139]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,139] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,139] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,139] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,139] = 5000

 kappa[0:int(M/2)-7*200+100,140]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,140]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,140]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,140]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(25500-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,140]=25500+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(13500-25500)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,140]=13500+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(7500-13500)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,140] = 7500+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-7500)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,140] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,140] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,140]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,140] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,140] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,140] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,140] = 5000

 kappa[0:int(M/2)-7*200+100,141]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,141]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,141]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,141]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,141]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,141]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,141] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,141] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,141] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,141]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,141] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,141] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,141] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,141] = 5000

 kappa[0:int(M/2)-7*200+100,142]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,142]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,142]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,142]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,142]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,142]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(5000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,142] = 5000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-5000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,142] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,142] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,142]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,142] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,142] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,142] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,142] = 5000


 kappa[0:int(M/2)-7*200+100,144]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,144]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,144]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,144]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,144]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,144]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(4000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,144] = 4000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-4000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,144] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,144] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,144]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,144] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,144] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,144] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,144] = 5000

 kappa[0:int(M/2)-7*200+100,146]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,146]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,146]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,146]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,146]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(15000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,146]=15000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-15000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,146] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,146] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,146] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,146]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,146] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,146] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,146] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,146] = 5000

 kappa[0:int(M/2)-7*200+100,147]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,147]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,147]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,147]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,147]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,147]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(5000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,147] = 5000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-5000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,147] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,147] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,147]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,147] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,147] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,147] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,147] = 5000

 kappa[0:int(M/2)-7*200+100,149]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,149]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,149]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,149]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,149]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(17000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,149]=17000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-17000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,149] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,149] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,149] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+200*2:int(M/2)+200*3+1,149]=8000+(y[int(M/2)+200*2:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+200*2])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,149] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,149] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,149] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,149] = 5000

 kappa[0:int(M/2)-7*200+100,151]=4000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,151]=4000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-4000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,151]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(23000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,151]=23000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-23000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,151]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(15000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,151]=15000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-15000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,151] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,151] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,151] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,151]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,151] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,151] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,151] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,151] = 5000

 # tests paralleles a -6degree (on part de 138th)
 kappa[0:int(M/2)-7*200+100,143]=6000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,143]=6000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-6000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,143]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,143]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,143]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,143]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,143] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,143] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,143] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,143]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,143] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,143] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,143] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,143] = 5000

 kappa[0:int(M/2)-7*200+100,145]=6000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,145]=6000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(20000-6000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,145]=20000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-20000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,145]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(23000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,145]=23000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-23000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,145]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,145] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,145] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,145] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,145]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,145] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,145] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,145] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,145] = 5000

 kappa[0:int(M/2)-7*200+100,148]=6000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+1,148]=6000+(y[int(M/2)-7*200+100:int(M/2)-5*200+1]-y[int(M/2)-7*200+100-1])*(15000-6000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,148]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,148]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,148]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,148]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,148] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,148] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,148] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,148]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,148] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,148] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,148] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,148] = 5000

 kappa[0:int(M/2)-7*200,150]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,150]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(8000-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,150]=8000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(15000-8000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,150]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(25000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,150]=25000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-25000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,150]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,150]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,150] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,150] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,150] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,150]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,150] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,150] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,150] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,150] = 5000

 kappa[0:int(M/2)-7*200,152]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,152]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(8000-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,152]=8000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(15000-8000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,152]=15000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(19000-15000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,152]=19000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-19000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,152]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,152]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,152] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,152] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,152] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,152]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,152] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,152] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,152] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,152] = 5000

 kappa[0:int(M/2)-7*200,153]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,153]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(8000-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,153]=8000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(10000-8000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,153]=10000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(19000-10000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,153]=19000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-19000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,153]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,153]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,153] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,153] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,153] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,153]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,153] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,153] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,153] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,153] = 5000

 kappa[0:int(M/2)-7*200,154]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,154]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(8000-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,154]=8000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(10000-8000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,154]=10000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(19000-10000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,154]=19000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-19000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,154]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(8000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,154]=8000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-8000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,154] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,154] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,154] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,154]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,154] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,154] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,154] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,154] = 5000

 kappa[0:int(M/2)-7*200,155]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,155]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(7000-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,155]=7000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(9000-7000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,155]=9000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(17000-9000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,155]=17000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-17000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,155]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,155]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,155] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,155] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,155] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,155]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,155] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,155] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,155] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,155] = 5000

 kappa[0:int(M/2)-7*200,156]=6000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,156]=6000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6500-6000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,156]=6500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(9000-6500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,156]=9000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(16000-9000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,156]=16000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-16000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,156]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,156]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,156] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,156] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,156] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,156]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,156] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,156] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,156] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,156] = 5000

 kappa[0:int(M/2)-7*200,158]=5500
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,158]=5500+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5500)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,158]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(8000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,158]=8000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(16000-8000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,158]=16000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-16000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,158]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,158]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,158] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,158] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,158] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,158]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,158] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,158] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,158] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,158] = 5000

 kappa[0:int(M/2)-7*200,159]=5500
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,159]=5500+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5500)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,159]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(8000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,159]=8000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(10000-8000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,159]=10000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-10000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,159]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,159]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,159] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,159] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,159] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,159]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,159] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,159] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,159] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,159] = 5000

 kappa[0:int(M/2)-7*200,161]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,161]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,161]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(7000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,161]=7000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(9000-7000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,161]=9000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-9000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,161]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,161]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,161] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,161] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,161] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,161]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,161] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,161] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,161] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,161] = 5000

 kappa[0:int(M/2)-7*200,163]=4000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,163]=4000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-4000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,163]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(7000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,163]=7000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(9000-7000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,163]=9000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-9000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,163]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,163]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,163] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(7000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,163] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,163] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,163]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,163] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,163] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,163] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,163] = 5000

 kappa[0:int(M/2)-7*200,164]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,164]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,164]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6500-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,164]=6500+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6500)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,164]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,164]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,164]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,164] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,164] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,164] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,164]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,164] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,164] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,164] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,164] = 5000

 kappa[0:int(M/2)-7*200,166]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,166]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(5500-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,166]=5500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6500-5500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,166]=6500+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6500)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,166]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,166]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,166]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,166] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,166] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,166] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,166]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,166] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,166] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,166] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,166] = 5000

 kappa[0:int(M/2)-7*200,168]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,168]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(5500-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,168]=5500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6500-5500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,168]=6500+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6500)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+100+1,168]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+100+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-2*200+1,168]=24000+(y[int(M/2)-3*200+100:int(M/2)-2*200+1]-y[int(M/2)-3*200+100-1])*(10000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,168]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,168] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,168] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,168] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,168]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,168] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,168] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,168] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,168] = 5000

 kappa[0:int(M/2)-7*200,169]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,169]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(5500-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,169]=5500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6500-5500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,169]=6500+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6500)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,169]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,169]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,169]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,169] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,169] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,169] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,169]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,169] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,169] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,169] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,169] = 5000

 kappa[0:int(M/2)-6*200,170]=5000
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,170]=5000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6000-5000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,170]=6000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,170]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,170]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,170]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,170] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,170] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,170] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,170]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,170] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,170] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,170] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,170] = 5000

 kappa[0:int(M/2)-7*200,171]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,171]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(5500-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,171]=5500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6500-5500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,171]=6500+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6500)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,171]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-300+1,171]=24000+(y[int(M/2)-3*200:int(M/2)-300+1]-y[int(M/2)-3*200-1])*(8000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-300])
 kappa[int(M/2)-300:int(M/2)-200+1,171]=8000+(y[int(M/2)-300:int(M/2)-200+1]-y[int(M/2)-300-1])*(6000-8000)/(-y[int(M/2)-300-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,171] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,171] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,171] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,171]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,171] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,171] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,171] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,171] = 5000


 kappa[0:int(M/2)-6*200,172]=5000
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,172]=5000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6000-5000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,172]=6000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,172]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,172]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(9000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,172]=9000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-9000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,172] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,172] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,172] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,172]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,172] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,172] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,172] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,172] = 5000

 kappa[0:int(M/2)-6*200,173]=5000
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,173]=5000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6000-5000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,173]=6000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,173]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,173]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(11000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,173]=11000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-11000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,173] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,173] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,173] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,173]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,173] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,173] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,173] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,173] = 5000

 kappa[0:int(M/2)-6*200,174]=5000
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,174]=5000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(6000-5000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,174]=6000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(8000-6000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,174]=8000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-8000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+100+1,174]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+100+1]-y[int(M/2)-3*200-1])*(9000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)-200+1,174]=9000+(y[int(M/2)-2*200+100:int(M/2)-200+1]-y[int(M/2)-2*200+100-1])*(6000-9000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,174] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,174] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,174] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,174]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,174] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,174] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,174] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,174] = 5000

 kappa[0:int(M/2)-6*200,175]=4500
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,175]=4500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5800-4500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,175]=5800+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(7000-5800)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,175]=7000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-7000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,175]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,175]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,175] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,175] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,175] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,175]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,175] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,175] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,175] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,175] = 5000

 kappa[0:int(M/2)-6*200,176]=3000
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,176]=3000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5800-3000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,176]=5800+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(7000-5800)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,176]=7000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-7000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,176]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,176]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,176] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,176] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,176] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,176]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,176] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,176] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,176] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,176] = 5000

 kappa[0:int(M/2)-6*200,177]=4500
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,177]=4500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5000-4500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,177]=5000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(6000-5000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,177]=6000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-6000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,177]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,177]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,177] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,177] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,177] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,177]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,177] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,177] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,177] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,177] = 5000

 kappa[0:int(M/2)-6*200,178]=4500
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,178]=4500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5000-4500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,178]=5000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(6000-5000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,178]=6000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(25000-6000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,178]=25000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-25000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,178]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,178] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,178] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,178] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,178]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,178] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,178] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,178] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,178] = 5000

 kappa[0:int(M/2)-6*200,179]=4500
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,179]=4500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5000-4500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,179]=5000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(6000-5000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,179]=6000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(28000-6000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,179]=28000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(10000-28000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,179]=10000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-10000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,179] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,179] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,179] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,179]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,179] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,179] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,179] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,179] = 5000

 kappa[0:int(M/2)-6*200,180]=4500
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,180]=4500+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(5000-4500)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,180]=5000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(6000-5000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,180]=6000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(28000-6000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,180]=28000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(15000-28000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,180]=15000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-15000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,180] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,180] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,180] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,180]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,180] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,180] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,180] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,180] = 5000



 # tests paralleles a 6degree (on part de 105th)
 kappa[0:int(M/2)-5*200+100,107]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,107]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(20000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,107]=20000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-20000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,107] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,107] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,107]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,107] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,107] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(5000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,107] = 5000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(4000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,107] = 4000

 kappa[0:int(M/2)-5*200+100,109]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,109]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(20000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,109]=20000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-20000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,109] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,109] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,109]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,109] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,109] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(4000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,109] = 4000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-4000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,109] = 5000

 kappa[0:int(M/2)-5*200+100,111]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,111]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(20000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,111]=20000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-20000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,111] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,111] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,111]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,111] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,111] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3500-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,111] = 3500+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3500)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,111] = 5000

 kappa[0:int(M/2)-5*200+100,113]=15000
 kappa[int(M/2)-5*200+100:int(M/2)-2*200+100+1,113]=15000+(y[int(M/2)-5*200+100:int(M/2)-2*200+100+1]-y[int(M/2)-5*200+100-1])*(20000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-2*200+100])
 kappa[int(M/2)-2*200+100:int(M/2)+1,113]=20000+(y[int(M/2)-2*200+100:int(M/2)+1]-y[int(M/2)-2*200+100-1])*(8000-20000)/(-y[int(M/2)-2*200+100-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,113] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,113] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,113]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,113] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(38000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,113] = 38000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-38000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,113] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,113] = 5000

 kappa[0:int(M/2)-5*200+100,112]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,112]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,112]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,112]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,112]=6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,112] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])


 kappa[0:int(M/2)-7*200+100,115]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,115]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,115]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,115]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,115] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,115] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,115] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,115]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(45000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,115] = 45000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(35000-45000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,115] = 35000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-35000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,115] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,115] = 5000


 kappa[0:int(M/2)-7*200+100,117]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,117]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,117]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,117]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,117] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,117] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,117] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,117]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(44000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,117] = 44000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(35000-44000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,117] = 35000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-35000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,117] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,117] = 5000

 kappa[0:int(M/2)-7*200+100,119]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,119]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,119]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,119]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,119] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,119] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,119] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,119]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,119] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(35000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,119] = 35000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-35000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,119] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,119] = 5000


 kappa[0:int(M/2)-7*200+100,121]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,121]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,121]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,121]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,121] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,121] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,121] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,121]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,121] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6+1,121] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6+1]-y[int(M/2)-1+200*4])*(3000-30000)/(y[int(M/2)+200*6]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6:int(M/2)+200*8+1,121] = 3000+(y[int(M/2)+200*6:int(M/2)+200*8+1]-y[int(M/2)-1+200*6])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6])
 kappa[int(M/2)+200*8:M,121] = 5000

 kappa[0:int(M/2)-7*200+100,123]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,123]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,123]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,123]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,123] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,123] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,123] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,123]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,123] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-100+1,123] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-100+1]-y[int(M/2)-1+200*4])*(3000-30000)/(y[int(M/2)+200*6-100]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-100:int(M/2)+200*8+1,123] = 3000+(y[int(M/2)+200*6-100:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-100])*(5000-3000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-100])
 kappa[int(M/2)+200*8:M,123] = 5000


 kappa[0:int(M/2)-7*200+100,125]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,125]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,125]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,125]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,125] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,125] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,125] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,125]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(42000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,125] = 42000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-42000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,125] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,125] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,125] = 5000

 kappa[0:int(M/2)-7*200+100,157]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,157]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,157]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,157]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,157] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,157] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,157] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,157]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(40000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,157] = 40000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-40000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,157] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,157] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,157] = 5000

 kappa[0:int(M/2)-7*200+100,160]=5000
 kappa[int(M/2)-7*200+100:int(M/2)-5*200+100+1,160]=5000+(y[int(M/2)-7*200+100:int(M/2)-5*200+100+1]-y[int(M/2)-7*200+100-1])*(15000-5000)/(-y[int(M/2)-7*200+100-1]+y[int(M/2)-5*200+100])
 kappa[int(M/2)-5*200+100:int(M/2)-3*200+100+1,160]=15000+(y[int(M/2)-5*200+100:int(M/2)-3*200+100+1]-y[int(M/2)-5*200+100-1])*(24000-15000)/(-y[int(M/2)-5*200+100-1]+y[int(M/2)-3*200+100])
 kappa[int(M/2)-3*200+100:int(M/2)-200+1,160]=24000+(y[int(M/2)-3*200+100:int(M/2)-200+1]-y[int(M/2)-3*200+100-1])*(6000-24000)/(-y[int(M/2)-3*200+100-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,160] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,160] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,160] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,160]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,160] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,160] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,160] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,160] = 5000

 kappa[0:int(M/2)-7*200,162]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,162]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,162]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(7000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,162]=7000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(9000-7000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,162]=9000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-9000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,162]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,162]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,162] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,162] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,162] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,162]=8000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-8000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,162] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,162] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,162] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,162] = 5000

 kappa[0:int(M/2)-7*200,165]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,165]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,165]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(7000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,165]=7000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(9000-7000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,165]=9000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-9000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,165]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,165]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,165] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(8000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,165] = 6500+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(8000-6500)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+400+1,165] = 6500+(y[int(M/2)+200:int(M/2)+400+1]-y[int(M/2)+200-1])*(7000-6500)/(y[int(M/2)+400]-y[int(M/2)+200-1])
 kappa[int(M/2)+400:int(M/2)+200*3+1,165]=7000+(y[int(M/2)+400:int(M/2)+200*3+1]-y[int(M/2)-1+400])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+400])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,165] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,165] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,165] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,165] = 5000

 kappa[0:int(M/2)-7*200,167]=5000
 kappa[int(M/2)-7*200:int(M/2)-6*200+1,167]=5000+(y[int(M/2)-7*200:int(M/2)-6*200+1]-y[int(M/2)-7*200-1])*(6000-5000)/(-y[int(M/2)-7*200-1]+y[int(M/2)-6*200])
 kappa[int(M/2)-6*200:int(M/2)-5*200+1,167]=6000+(y[int(M/2)-6*200:int(M/2)-5*200+1]-y[int(M/2)-6*200-1])*(7000-6000)/(-y[int(M/2)-6*200-1]+y[int(M/2)-5*200])
 kappa[int(M/2)-5*200:int(M/2)-4*200+1,167]=7000+(y[int(M/2)-5*200:int(M/2)-4*200+1]-y[int(M/2)-5*200-1])*(9000-7000)/(-y[int(M/2)-5*200-1]+y[int(M/2)-4*200])
 kappa[int(M/2)-4*200:int(M/2)-3*200+1,167]=9000+(y[int(M/2)-4*200:int(M/2)-3*200+1]-y[int(M/2)-4*200-1])*(24000-9000)/(-y[int(M/2)-4*200-1]+y[int(M/2)-3*200])
 kappa[int(M/2)-3*200:int(M/2)-2*200+1,167]=24000+(y[int(M/2)-3*200:int(M/2)-2*200+1]-y[int(M/2)-3*200-1])*(12000-24000)/(-y[int(M/2)-3*200-1]+y[int(M/2)-2*200])
 kappa[int(M/2)-2*200:int(M/2)-200+1,167]=12000+(y[int(M/2)-2*200:int(M/2)-200+1]-y[int(M/2)-2*200-1])*(6000-12000)/(-y[int(M/2)-2*200-1]+y[int(M/2)-200])
 kappa[int(M/2)-200:int(M/2)+1,167] = 6000+(y[int(M/2)-200:int(M/2)+1]-y[int(M/2)-200-1])*(10000-6000)/(-y[int(M/2)-200-1]+y[int(M/2)])
 kappa[int(M/2):int(M/2)+200,167] = 6000+(y[int(M/2):int(M/2)+200]-y[int(M/2)+200-1])*(10000-6000)/(y[int(M/2)]-y[int(M/2)+200-1])
 kappa[int(M/2)+200:int(M/2)+300+1,167] = 6000+(y[int(M/2)+200:int(M/2)+300+1]-y[int(M/2)+200-1])*(8000-6000)/(y[int(M/2)+300]-y[int(M/2)+200-1])
 kappa[int(M/2)+300:int(M/2)+200*3+1,167]=8000+(y[int(M/2)+300:int(M/2)+200*3+1]-y[int(M/2)-1+300])*(39000-7000)/(y[int(M/2)+200*3]-y[int(M/2)-1+300])
 kappa[int(M/2)+200*3:int(M/2)+200*4+1,167] = 39000 +(y[int(M/2)+200*3:int(M/2)+200*4+1]-y[int(M/2)-1+200*3])*(30000-39000)/(y[int(M/2)+200*4]-y[int(M/2)-1+200*3])
 kappa[int(M/2)+200*4:int(M/2)+200*6-50+1,167] = 30000+(y[int(M/2)+200*4:int(M/2)+200*6-50+1]-y[int(M/2)-1+200*4])*(5000-30000)/(y[int(M/2)+200*6-50]-y[int(M/2)-1+200*4])
 kappa[int(M/2)+200*6-50:int(M/2)+200*8+1,167] = 5000+(y[int(M/2)+200*6-50:int(M/2)+200*8+1]-y[int(M/2)-1+200*6-50])*(5000-5000)/(y[int(M/2)+200*8]-y[int(M/2)-1+200*6-50])
 kappa[int(M/2)+200*8:M,167] = 5000


 
 kappa_values=np.array([25357, 12429, 41467, 43437, 6019, 11156, 4094, 34467, 23533, 11430, 6933, 40485, 16263, 30541, 20488, 34201, 15237, 5232, 15624, 43872, 3392])
 nb_points_between_kappa_values = int(200)
 equator_index = int(M/2)

 kappa[0:equator_index-10*nb_points_between_kappa_values,181]=kappa_values[0]

 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),181] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,181]=kappa_values[-1]



 kappa_values=np.array([21229, 11495, 22564, 43960, 15323, 4130, 20364, 13118, 26246, 3610, 13856, 35309, 39474, 40837, 27798, 11203, 15237, 5232, 9344, 30430, 27993, 43141])
 kappa[0:equator_index-10*nb_points_between_kappa_values,182]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),182] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,182]=kappa_values[-1]

 kappa_values=np.array([5570, 5822, 10624, 43960, 3324, 4130, 11823, 24909, 34222, 3610, 8926, 42154, 27768, 24554, 13579, 36001, 4640, 5232, 8686, 22644, 4312, 43141])
 kappa[0:equator_index-10*nb_points_between_kappa_values,183]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),183] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,183]=kappa_values[-1]


 #from plot 19: 15days_-6,-3,3,6_70pop
 kappa_values=np.array([5965, 22160, 13472, 39524, 4378, 3468, 32983, 24753, 8800, 16271, 3393, 13150, 17288, 10308, 43612, 19216, 7271, 5372, 8874, 27397, 29227, 12012])
 kappa[0:equator_index-10*nb_points_between_kappa_values,184]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),184] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,184]=kappa_values[-1]

 # from plot 21 with modified first and last values
 kappa_values=np.array([13472, 13472, 13472, 20458, 4378, 4538, 32983, 24753, 8800, 16271, 3393, 13150, 10009, 23382, 43612, 19216, 7271, 5372, 8874, 8874, 8874, 12012])
 kappa[0:equator_index-10*nb_points_between_kappa_values,185]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),185] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,185]=kappa_values[-1]




 # from plot 35 with modified first and last values
 kappa_values=np.array([4378, 4378, 4302, 4378, 4378, 4538, 32983, 19289, 8800, 8547, 3393, 29974, 10009, 23382, 43612, 19216, 7271, 5170, 8771, 5170, 5170, 5170])
 kappa[0:equator_index-10*nb_points_between_kappa_values,186]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),186] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,186]=kappa_values[-1]

 # from plot 10 -3,-2,-1,1,2,3 - 20days (second run)
 kappa_values2=np.array([18135, 7199, 29680, 26531, 34642, 9861, 8112, 4088, 11521, 27457, 25704, 40076, 21973, 4002, 16635, 21893])
 kappa[0:equator_index-7*nb_points_between_kappa_values,187]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),187] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,187]=kappa_values2[-1]

 # from plot 10 -3,-2,-1,1,2,3 - 20days (second run) with modified first and last values
 kappa_values2=np.array([5000, 7199, 29680, 26531, 34642, 9861, 8112, 4088, 11521, 27457, 25704, 40076, 21973, 4002, 5000, 5000])
 kappa[0:equator_index-7*nb_points_between_kappa_values,188]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),188] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,188]=kappa_values2[-1]

 # from plot 22 -3,-2,-1,1,2,3 - 20days (second run) 
 kappa_values2=np.array([6788, 4605, 13921, 38300, 14372, 9861, 8112, 4088, 11521, 14575, 25704, 40076, 19573, 4002, 8138, 8138])
 kappa[0:equator_index-7*nb_points_between_kappa_values,189]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),189] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,189]=kappa_values2[-1]

 # from plot 22 -3,-2,-1,1,2,3 - 22days (second run) with modified first and last values
 kappa_values2=np.array([4605, 4605, 13921, 38300, 14372, 9861, 8112, 4088, 11521, 14575, 25704, 40076, 19573, 4002, 4002, 4002])
 kappa[0:equator_index-7*nb_points_between_kappa_values,190]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),190] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,190]=kappa_values2[-1]

 # from plot 24 -3,-2,-1,1,2,3 - 20days (second run) 
 kappa_values2=np.array([6788, 4605, 13921, 38300, 14372, 9861, 8112, 4088, 9537, 8076, 38242, 31431, 39012, 7088, 7031, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,191]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),191] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,191]=kappa_values2[-1]


 # like the 183 but with modified values at 10N
 kappa_values=np.array([5570, 5822, 10624, 43960, 3324, 4130, 11823, 24909, 34222, 3610, 8926, 42154, 27768, 24554, 13579, 36001, 4640, 5232, 8686, 22644, 4312, 4312])
 kappa[0:equator_index-10*nb_points_between_kappa_values,192]=kappa_values[0]
 for i in range(0,20):
   kappa[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10),192] = fc(kappa_values[i],kappa_values[i+1],y[equator_index+ nb_points_between_kappa_values*(i-10):equator_index+ nb_points_between_kappa_values*(i+1-10)],dy)
 
 kappa[equator_index+10*nb_points_between_kappa_values:M,192]=kappa_values[-1]

 # from plot 24 -3,-2,-1,1,2,3 - 20days (second run) 
 kappa_values2=np.array([4605, 4605, 13921, 38300, 14372, 9861, 8112, 4088, 9537, 8076, 38676, 40076, 41079, 4002, 4002, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,193]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),193] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,193]=kappa_values2[-1]



 # from plot 30 -3,-2,-1,1,2,3 - 20days (second run) 
 kappa_values2=np.array([4390, 4605, 13921, 38300, 14372, 7194, 8112, 4088, 9537, 8076, 38676, 40076, 41079, 4002, 4265, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,194]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),194] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,194]=kappa_values2[-1]

 # dromadary A REFAIRE
 kappa[:,195] = 2.*1E4 * np.exp(-((y-200)/400)**2)+ 3000


 # from plot 32 -3,-2,-1,1,2,3 - 20days (second run) A REFAIRE ?
 kappa_values2=np.array([4390, 4605, 13921, 38300, 14372, 7194, 5384, 4088, 9537, 8076, 38676, 40076, 41079, 4002, 8138, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,196]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),196] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,196]=kappa_values2[-1]

 # CHAMEL symmetric
 kappa[:,197] = 4.*1E4 * np.exp(-((y+400)/200)**2)  + 4.*1E4*np.exp(-((y-400)/200)**2) + 5000

 # CHAMEL NON SYMMETRIC
 kappa[:,198] = 2.*1E4 * np.exp(-((y+500)/150)**2)  + 4.*1E4*np.exp(-((y-400)/200)**2) + 5000


 # from plot 39 -3,-2,-1,1,2,3 - 20days (second run)
 kappa_values2=np.array([4390, 4605, 13921, 38300, 14372, 7194, 3441, 4088, 9537, 8076, 38676, 40076, 38691, 4002, 4174, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,199]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),199] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,199]=kappa_values2[-1]


 # from plot 41 -3,-2,-1,1,2,3 - 20days (second run)
 kappa_values2=np.array([4390, 4605, 13921, 38300, 14372, 7194, 5384, 6165, 9537, 8076, 38676, 40076, 41079, 4002, 8138, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,200]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),200] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,200]=kappa_values2[-1]


 # from plot 44 -3,-2,-1,1,2,3 - 20days (second run) modifed last value
 kappa_values2=np.array([4390, 4605, 13921, 38300, 14372, 7194, 5384, 6165, 9537, 8076, 38676, 40076, 23102, 4002, 4174, 4265])
 kappa[0:equator_index-7*nb_points_between_kappa_values,201]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),201] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,201]=kappa_values2[-1]


 kappa_values2=np.array([26816, 6906, 10668, 38331, 36593, 5546, 38548, 5644, 3436, 13734, 11740, 42057, 28769, 7909, 11191, 19353])
 kappa[0:equator_index-7*nb_points_between_kappa_values,202]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),202] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,202]=kappa_values2[-1]

 kappa_values2=np.array([31905, 20500, 16145, 35917, 16171, 28620, 35983, 8854, 14203, 35681, 46754, 26395, 26179, 46556, 49786, 20455])
 kappa[0:equator_index-7*nb_points_between_kappa_values,203]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),203] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,203]=kappa_values2[-1]

 kappa_values2=np.array([31905, 20500, 16145, 8655, 28228, 39365, 19044, 8712, 24986, 44740, 46754, 26395, 26179, 35255, 48780, 16430])
 kappa[0:equator_index-7*nb_points_between_kappa_values,204]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),204] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,204]=kappa_values2[-1]

 kappa_values2=np.array([28802, 13915, 16145, 4007, 36590, 39953, 41472, 8854, 13916, 35681, 46754, 41669, 35904, 15853, 7301, 7301])
 kappa[0:equator_index-7*nb_points_between_kappa_values,206]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),206] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,206]=kappa_values2[-1]

 kappa_values2=np.array([28143, 18210, 48688, 33915, 43548, 22161, 17137, 16685, 7493, 4637, 43155, 48635, 46008, 23735, 4312, 4312])
 kappa[0:equator_index-7*nb_points_between_kappa_values,207]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),207] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,207]=kappa_values2[-1]


 kappa_values2=np.array([28143, 18210, 24191, 33915, 43548, 22161, 44819, 16685, 7493, 4637, 43389, 48635, 46008, 5666, 4312, 4312])
 kappa[0:equator_index-7*nb_points_between_kappa_values,208]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),208] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,208]=kappa_values2[-1]

 kappa_values2=np.array([44658, 9365, 48688, 29926, 9974, 36016, 24537, 30694, 5674, 4637, 43389, 48635, 46008, 3769, 4312, 4312])
 kappa[0:equator_index-7*nb_points_between_kappa_values,209]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),209] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,209]=kappa_values2[-1]


 kappa_values2=np.array([41046, 18210, 24097, 10664, 41423, 36475, 43742, 44652, 4741, 4637, 44440, 48635, 46008, 3379, 4312, 4312])
 kappa[0:equator_index-7*nb_points_between_kappa_values,210]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),210] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,210]=kappa_values2[-1]


 kappa_values2=np.array([4069, 20130, 43032, 42915, 31256, 40104, 41974, 44726, 49430, 4215, 6768, 43488, 21587, 4111, 3777, 3777])
 kappa[0:equator_index-7*nb_points_between_kappa_values,211]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),211] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,211]=kappa_values2[-1]


 kappa_values2=np.array([40772, 16163, 35221, 22040, 5190, 6742, 29011, 8984, 13797, 5237, 32571, 12381, 16525, 4874, 3230, 3230])
 kappa[0:equator_index-7*nb_points_between_kappa_values,212]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),212] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,212]=kappa_values2[-1]

 kappa_values2=np.array([12244, 17696, 41209, 36916, 34247, 40169, 9909, 3878, 3145, 4400, 20615, 12368, 17115, 4073, 4292, 4292])
 kappa[0:equator_index-7*nb_points_between_kappa_values,213]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),213] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,213]=kappa_values2[-1]

 # 6N - 2nd
 kappa_values2=np.array([45747, 14261, 21379, 35893, 20670, 15485, 4350, 4105, 5086, 3946, 21545, 34222, 8036, 10841, 3846, 3846])
 kappa[0:equator_index-7*nb_points_between_kappa_values,214]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),214] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,214]=kappa_values2[-1]

 # 6N - 2nd
 kappa_values2=np.array([45747, 32673, 43957, 27464, 23065, 28164, 4350, 4105, 4105, 3164, 21545, 34222, 8036, 8394, 3846, 3846])
 kappa[0:equator_index-7*nb_points_between_kappa_values,215]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),215] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,215]=kappa_values2[-1]

 # 6N - 3rd exp
 kappa_values2=np.array([25296, 6432, 39449, 4965, 11533, 31816, 42227, 7846, 19422, 3649, 3524, 8206, 28285, 4255, 16370, 16370])
 kappa[0:equator_index-7*nb_points_between_kappa_values,216]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),216] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,216]=kappa_values2[-1]

 # 6N - 4 exp 
 kappa_values2=np.array([37770, 16686, 27782, 21184, 20256, 22198, 20729, 40292, 11454, 6618, 3024, 3395, 21271, 22944, 40282, 40282])
 kappa[0:equator_index-7*nb_points_between_kappa_values,217]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),217] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,217]=kappa_values2[-1]


 # 1N - 2nd 
 kappa_values2=np.array([43375, 32952, 3748, 26776, 39816, 37964, 23828, 17588, 4131, 41939, 35845, 43728, 39170, 12306, 25364, 25364])
 kappa[0:equator_index-7*nb_points_between_kappa_values,218]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),218] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,218]=kappa_values2[-1]

 # 1N - 3rd 
 kappa_values2=np.array([23415, 14140, 7543, 20707, 16965, 42147, 21414, 18058, 4611, 44209, 49788, 31792, 40244, 12291, 9333, 9333])
 kappa[0:equator_index-7*nb_points_between_kappa_values,219]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),219] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,219]=kappa_values2[-1]

 # 2N - 2nd 
 kappa_values2=np.array([13570, 30550, 46505, 41929, 40723, 39569, 43808, 44702, 4468, 3641, 48999, 44032, 25480, 3181, 3060, 3060])
 kappa[0:equator_index-7*nb_points_between_kappa_values,220]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),220] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,220]=kappa_values2[-1]


 # 2N - 3rd
 kappa_values2=np.array([26804, 41743, 44516, 33531, 42508, 33936, 5515, 33727, 3907, 4039, 40935, 47344, 28088, 4266, 5304, 5304])
 kappa[0:equator_index-7*nb_points_between_kappa_values,221]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),221] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,221]=kappa_values2[-1]

 # 3N - rd
 kappa_values2=np.array([25599, 9041, 30689, 21057, 44341, 30010, 31443, 40702, 42717, 6508, 5124, 37364, 22263, 4340, 5159, 5159])
 kappa[0:equator_index-7*nb_points_between_kappa_values,222]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),222] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,222]=kappa_values2[-1]

 # 3N - 4rd
 kappa_values2=np.array([40925, 27589, 40666, 30056, 33231, 43899, 43121, 43639, 44383, 5240, 3793, 44979, 15542, 8618, 10616, 10616])
 kappa[0:equator_index-7*nb_points_between_kappa_values,223]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),223] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,223]=kappa_values2[-1]

 # 6N - 3rd
 kappa_values2=np.array([17897, 14171, 11925, 9148, 25361, 15401, 3347, 4326, 36054, 6491, 40168, 15730, 14184, 4958, 3664, 3664])
 kappa[0:equator_index-7*nb_points_between_kappa_values,224]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),224] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,224]=kappa_values2[-1]

 # -1N - 1st
 kappa_values2=np.array([6760, 34173, 44292, 3323, 8409, 36301, 4826, 8693, 44305, 43675, 30285, 34215, 25521, 36476, 19037, 19037])
 kappa[0:equator_index-7*nb_points_between_kappa_values,225]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),225] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,225]=kappa_values2[-1]

# -1N - 2nd
 kappa_values2=np.array([24307, 16382, 14109, 8156, 7345, 33633, 3915, 9287, 44236, 38655, 15216, 6403, 29863, 37864, 35110, 35110])
 kappa[0:equator_index-7*nb_points_between_kappa_values,226]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),226] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,226]=kappa_values2[-1]

# 1N - 2spe
 kappa_values2=np.array([40466, 31956, 18243, 14023, 27703, 41991, 41529, 13778, 4158, 37309, 43804, 43931, 35896, 6263, 3973, 3973])
 kappa[0:equator_index-7*nb_points_between_kappa_values,227]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),227] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,227]=kappa_values2[-1]

 # from plot 64 -3,-2,-1,1,2,3 - 20days (second run) modifed last value
 # 4390, 4605, 13921, 44021, 14372, 7194, 4065, 10815, 9537, 8076, 38676, 40076, 24950, 4002, 6096, 13071

 '''
 plt.figure(nb_figure)
 plt.plot(y/deg, kappa[:,nb_test])
 plt.savefig('tests'+str(starting_lat)+'/test_kappa_'+str(nb_test)+'.png')
 nb_figure = nb_figure+1
 '''

 kappa = kappa*0.0864

 
 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 
 ys = starting_lat*deg                    # positon of initial PDF (degres)
 py = 0.2*deg				  # width of initial PDF


 #n_index = [4*5*factor, 4*15*factor, 4*30*factor, 4*50*factor, 4*80*factor, 4*125*factor]
 n_index=np.arange(4*factor,4*nb_distrib*factor+1,4*factor)
 print starting_lat
 nb_points = 2*int(-lat_limit_south/width)


 p0_saved = np.zeros((nb_distrib,M,4))
      
 lat_cross0=np.array([1.,2.,3.,6.])
 height_float = np.zeros((nb_points+1,nb_distrib,4))   
 height_float05 = np.zeros((nb_points/5+1,nb_distrib,4))
 xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )
 xaxis2 = np.linspace( lat_limit_south, -lat_limit_south, nb_points/5+1 )
 D = np.zeros((nb_distrib,4))
 D_star = np.zeros((nb_distrib,4))
 P = np.zeros((nb_distrib,4))
 p0_in_bin = np.zeros((nb_points+1,nb_distrib,4))


 mean_n = np.zeros((nb_distrib,4))
 velocity_n = np.zeros((nb_distrib,4))

 for k in range(0,1):
   print lat_cross0[k]
   p0_saved[:,:,k],mean_n[:,k],velocity_n[:,k] = distribution_6_different_times(y,dy,lat_cross0[k]*deg,py,M,N,dt,factor, n_index,v,kappa[:,nb_test],nb_distrib)
   #p0_saved[:,:,1] = distribution_6_different_times(y,dy,-6.0*deg,py,M,N,dt,factor, n_index,v,kappa[:,nb_test],nb_distrib)  
          

   if lat_cross0[k]>0:     
       nb_floats = np.array(np.array([0,391,537,671,0,0,692,0,529,0,0,0,0,0,0,0,0,0]))
       
   else:
       nb_floats = np.array([0,463,409,487,0,0,489,0,0,0,0,0,0,0])

   Ne =nb_floats[int(abs(lat_cross0[k]))]

   height_float[:,:,k] = reading_distrib(lat_cross0[k],n_index,factor,width,nb_points,nb_distrib)
   height_float05[:,:,k] = reading_distrib(lat_cross0[k],n_index,factor,0.5,nb_points/5,nb_distrib)

 
   for j in range(0,nb_distrib):
     D[j,k] = KS_stat(height_float[:,j,k], p0_saved[j,:,k], n_index[j], dt, nb_points, M, y/deg,xaxis,width )
    
     P[j,k] = Q_KS((np.sqrt(Ne)+0.12+0.11/np.sqrt(Ne))*D[j,k])

    
     

 
   plt.figure(nb_figure)
   plt.plot(P[:,k])
   plt.savefig('tests'+str(lat_cross0[k])+'/KS_proba_'+str(lat_cross0[k])+'_try'+str(nb_test)+'.png')
   nb_figure = nb_figure+1

   nb_distrib2=2
   nb_figure = plotting_distrib_plus(p0_saved[nb_distrib2-1,:,0],p0_saved[nb_distrib2-1,:,k], y/deg,  height_float05[:,nb_distrib2-1,0],  height_float05[:,nb_distrib2-1,k], xaxis2, -lat_limit_south, 0.5, kappa[:,nb_test]/0.0864, v/86.4, n_index[nb_distrib-1], dt, lat_cross0[k], nb_figure, nb_test,P[:,k], nb_distrib2)
   

 '''
 print mean_n[0:5,:]
 print velocity_n[0:5,:]*1E2
 plt.figure(nb_figure)
 for i in range(0,4):
   plt.plot(mean_n[:,i]/deg,velocity_n[:,i]*1E2)
 plt.plot(y/deg,v)
 plt.savefig('lagrangian_velocity_model_'+str(lat_cross0[0])+'_try'+str(nb_test)+'.png')
 nb_figure=nb_figure+1
 '''
 


 plt.figure(nb_figure)
 ax = plt.subplot(111) 
 ax.plot(D[:,0],'--m',label=str(lat_cross0[0]))

 
 for i in range(1,4):
     ax.plot(D[:,i], label=str(lat_cross0[i]) ) 
 
 #ax.plot(D[:,4],'m',label=str(lat_cross0[4]))
 #ax.plot(D[:,5],'k',label=str(lat_cross0[5]))
 #ax.plot(D[:,6],'--c',label=str(lat_cross0[6]))
 #ax.plot(D[:,7],'--r',label=str(lat_cross0[7]))
 
 ax.legend(prop={'size':8},bbox_to_anchor=(1.12, 1.05))
 plt.savefig('tests'+str(starting_lat)+'/KS_stat_'+str(starting_lat)+'_try'+str(nb_test)+'.png')
 nb_figure = nb_figure+1
 
 

   
   
 return p0_saved*1E-3, nb_figure





