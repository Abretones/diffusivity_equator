import provisoire
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')

import random
import sys
sys.path.append('/uio/kant/geo-metos-u7/anaisb/Desktop/diffusivity_equator/DEAP_folder/lib/python2.6/site-packages/deap')
import creator, base, tools, algorithms
import multiprocessing

nb_figure=1

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()

toolbox.register("attr_bool", random.randint, 3000, 50000)
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, n=16)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)


def computing_chi_stat(kappa_values):
  lat_limit_south=-20
  width = 0.1

  lat_cross =np.array([ 1.,3.,-1.])
  day_max = 30
  kappa_resolution = 1
  
  #P=np.zeros((1))
  #for i in range(0,3):
  P = provisoire.distrib_at_n(kappa_values,kappa_resolution,lat_cross[0], lat_limit_south, width, day_max )
  #fit = np.min(P)
  #print fit
  return float(P),


toolbox.register("evaluate", computing_chi_stat)
toolbox.register("mate", tools.cxTwoPoint )
toolbox.register("mutate", tools.mutUniformInt,low=3000,up=45000, indpb=0.05)
toolbox.register("select", tools.selTournament, tournsize=3)

population = toolbox.population(n=45)
print population
NGEN=10
pool = multiprocessing.Pool() 
toolbox.register("map", pool.map)
kappa_val5=np.zeros((16,5))

file = open('plots_DEAP/info_run.txt','w')
for gen in range(NGEN): 
    print gen
    file.write(str(gen)+'\n')   
    offspring = algorithms.varAnd(population, toolbox, cxpb=0.3, mutpb=0.7)
    fits = toolbox.map(toolbox.evaluate, offspring)
    file.write(str(fits)+'\n')
    for fit, ind in zip(fits, offspring):
        ind.fitness.values = fit
    population = toolbox.select(offspring, k=len(population))
    top5 = tools.selBest(population, k=5)
    print top5
    
    for j in range(0,5):
        file.write(str(top5[j])+'\n') 
        kappa_val5[:,j]=np.array(top5[j])
        dy,y_max,y,dy,M,kappa,nb_figure = provisoire.kappa_profile(kappa_val5[:,j],1,1,nb_figure)
        nb_figure=nb_figure-1
    nb_figure=nb_figure+1

top1 = tools.selBest(population, k=1)


print top1[0]
file.write(str(top1[0])+'\n') 

kappa_values = np.array(top1[0])

dy,y_max,y,dy,M,kappa,nb_figure = provisoire.kappa_profile(kappa_values,1,1,nb_figure)


file.close()


