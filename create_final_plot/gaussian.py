import numpy as np
import matplotlib
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import matplotlib.dates as md
import pylab as pl
import datetime

import matplotlib.gridspec as gridspec

import playing_with_data


import out
import constants



#____________________________________________________________________

def KS_stat(height_float, p0_saved, n_index, dt , nb_points, M):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


 
   delta_N[0]=(height_float[0])/(playing_with_data.trap(height_float[:],1.))
   for i in range(1,nb_points+1):
      delta_N[i]=delta_N[i-1]+(height_float[i])/playing_with_data.trap(height_float,1.)



   F_p[0]=p0_saved[0]/playing_with_data.trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/playing_with_data.trap(p0_saved,1.)
      diff[i] = abs(delta_N[i]-F_p[i])
   

   
   return np.max(diff[:])


def Q_KS(Lambda):
  

  Q = 0

  
  for j in range(1,300):
 
       Q = Q + ((-1)**(j-1))*np.exp(-2*(j*Lambda)**2)


  return 2*Q






#///////////////////////////////////////////////////////////////////////////////#
#-------------------------------------------------------------------------------#
										#	
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# I] what area do you want to study ?   					#
										#
ocean = 'pacific'                        # atlantic or pacific			#
										#
lat_range = 30.				 					#
										#
long_e = -75.									#
										#
long_w = 120.									#
										#


# PART II (independant)
option_using_charged_data = 1




# PART IV after part II or III

option_gaussian_fit = 1




										#
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - -			#
# III] Specify these parameters, depending which options you chose		#
										#
long_e_of_study = -110								#
long_w_of_study = -170								#
										#
lat_cross = 1.  
t_day = 40      				             			#						
t_min = 4*t_day+1     								#
trajectories_length = 4*125
										#
										#
width = 0.5									#
step_lat = 0.25             							#
lat_limit_south = -20.                    					#
month_min =1
month_max =12										#
#-------------------------------------------------------------------------------#
#///////////////////////////////////////////////////////////////////////////////#


nb_figure=1

lat_s = -lat_range
lat_n = lat_range

#y_axis = np.arange(lat_cross+0.05,8.5,0.1)

if width == 0.1:
    folder = '../master_project/plots_distrib'
elif width == 0.5:
    folder = '../master_project/plots_distrib05'



lat_cross2=np.array([8.,6.,1.])


#-------------- _______________________________________________________________________________________________________________
# end of PART I ---------------------------------------------------------------------------------------------------------------
#--------------

if option_using_charged_data == 1:

   mean_position = np.zeros((t_min,3))
   nb_drifters_at_t = np.zeros((t_min,3))


   nb_points = 2*int(-lat_limit_south/width)

   for j in range(0,3):
     txt_file = '../master_project/plots_distrib/mean_velocity_starting_at_'+str(lat_cross2[j])+'.txt'
     with open(txt_file, "r") as f:
       tab1 = []
       tab2 = []

       for line in range(0,t_min):
         tab1.append(f.readline().split())
         tab2.append(f.readline().split())
 
       var1 = np.array([x[0] for x in tab1])
       var2 = np.array([x[0] for x in tab2])
       
       for i in range(0,t_min):            
         mean_position[i,j] = str(var1[i])
         nb_drifters_at_t[i,j] = str(var2[i])
   #--------------------------------------------------------------------------------------------------



   width=0.5

   height_float = np.zeros((nb_points+1,2,3))

   

   
   for j in range(0,3):

     txt_file = str(folder)+'/real_distribution_at_20_days_starting_at_'+str(lat_cross2[j])+'.txt'
   
     with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])

       
       for i in range(0,nb_points+1):   
         
         height_float[i,0,j] = str(height[i])

     txt_file = str(folder)+'/real_distribution_at_30_days_starting_at_'+str(lat_cross2[j])+'.txt'
   
     with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])

       
       for i in range(0,nb_points+1):   
         
         height_float[i,1,j] = str(height[i])




 

   

if option_gaussian_fit == 1:


   xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )

   h = np.zeros((nb_points+1)) 
   p=np.zeros((nb_points+1,4,3))


   
   kappa = np.zeros((4))
   
   kappa[0] = 0.62*1E4
   kappa[1] = 0.8*1E4
   kappa[2] = 2.7*1E4
   kappa[3] = 0.4*1E4

   #kappa[0] = 0.2*1E4
   #kappa[1] = 0.4*1E4
   #kappa[2] = 0.6*1E4




   for j in range(0,3):
     p[:,0,j]=(1/np.sqrt(4*np.pi*kappa[j] *20*24*3600))*np.exp( -((xaxis-mean_position[20*4-1,j])*constants.deg)**2/(4*kappa[j] *20*24*3600))
     p[:,1,j]=(1/np.sqrt(4*np.pi*kappa[j] *30*24*3600))*np.exp( -((xaxis-mean_position[30*4-1,j])*constants.deg)**2/(4*kappa[j] *30*24*3600))
     p[:,2,j]=(1/np.sqrt(4*np.pi*kappa[3] *20*24*3600))*np.exp( -((xaxis-mean_position[20*4-1,j])*constants.deg)**2/(4*kappa[3] *20*24*3600))
     p[:,3,j]=(1/np.sqrt(4*np.pi*kappa[3] *30*24*3600))*np.exp( -((xaxis-mean_position[30*4-1,j])*constants.deg)**2/(4*kappa[3] *30*24*3600))

   dt =10   
   gs = gridspec.GridSpec(2, 1)
   plt.figure(nb_figure)
   
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')
   plt.figure(figsize=(6,10))   

   '''
   ax = plt.subplot(gs[0, 0]) 
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)   
   plt.ylim([0,0.4])
   plt.yticks([0,0.2])
   plt.xlim([-10,15])
   plt.bar(xaxis,height_float[:,0,0]/playing_with_data.trap(height_float[nb_points/2:-1,0,0],width),width,color='lightsage',edgecolor='seagreen')#,width),width)
   
   plt.plot(xaxis,p[:,0,0]/playing_with_data.trap(p[:,0,0],width),linewidth=2.5, label='$\kappa$='+str("%+02d" %kappa[0]))
   plt.plot(xaxis,p[:,2,0]/playing_with_data.trap(p[:,2,0],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   plt.title(r'$\kappa=6.2\times 10^{3}$ m$^{2}$.s$^{-1}$',size=18)
   #plt.legend(prop={'size':14},bbox_to_anchor=(0.3, 1.05))

   
   
   ax = plt.subplot(gs[0, 0]) 
   plt.title(r'$\kappa=8\times 10^{3}$ m$^{2}$.s$^{-1}$',size=18)
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)   
   plt.ylim([0,0.4])
   plt.yticks([0,0.2])
   plt.xlim([-10,15])
   #plt.title('$\kappa$ ='+str(kappa))
   plt.bar(xaxis,height_float[:,0,1]/playing_with_data.trap(height_float[nb_points/2:-1,0,1],width),width,color='peachpuff',edgecolor='burlywood')#,width),width)

   plt.plot(xaxis,p[:,0,1]/playing_with_data.trap(p[:,0,1],width),linewidth=2.5, color='darkorange',label='$\kappa$='+str("%+02d" %kappa[1]))
   plt.plot(xaxis,p[:,2,1]/playing_with_data.trap(p[:,2,1],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   #plt.legend(prop={'size':14},bbox_to_anchor=(0.3, 1.05))
   '''
   
   ax = plt.subplot(gs[0, 0]) 
   plt.title(r'$\kappa=27\times 10^{3}$ m$^{2}$.s$^{-1}$',size=18)
   tick_locs = [-5,1,10]
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)  
   plt.ylim([0,0.4])
   plt.yticks([0,0.2])
   plt.xlim([-10,15])
   plt.bar(xaxis,height_float[:,0,2]/playing_with_data.trap(height_float[nb_points/2:-1,0,2],width),width,color='#F0E68C',edgecolor='#FFD700')#,width),width)
   plt.plot(xaxis,p[:,0,2]/playing_with_data.trap(p[:,0,2],width),linewidth=2.5,color='red', label='$\kappa$='+str("%+02d" %kappa[2]))
   plt.plot(xaxis,p[:,2,2]/playing_with_data.trap(p[:,2,2],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   #plt.legend(prop={'size':14},bbox_to_anchor=(0.3, 1.05))
   '''
   ax = plt.subplot(gs[1, 0]) 
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)
   plt.ylim([0,0.4])
   plt.yticks([0,0.2])
   plt.xlim([-10,15])
   #plt.title('$\kappa$ ='+str(kappa))
   plt.bar(xaxis,height_float[:,1,0]/playing_with_data.trap(height_float[nb_points/2:-1,1,0],width),width,color='lightsage',edgecolor='seagreen')#,width),width)
   plt.plot(xaxis,p[:,1,0]/playing_with_data.trap(p[:,1,0],width),linewidth=2.5, label='$\kappa$='+str("%+02d" %kappa[0]))
   plt.plot(xaxis,p[:,3,0]/playing_with_data.trap(p[:,3,0],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   #plt.legend(prop={'size':14},bbox_to_anchor=(1.1, 1.05))
   
   
   ax = plt.subplot(gs[1, 0]) 
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)   
   plt.ylim([0,0.4])
   plt.yticks([0,0.2])
   plt.xlim([-10,15])
   #plt.title('$\kappa$ ='+str(kappa))
   plt.bar(xaxis,height_float[:,1,1]/playing_with_data.trap(height_float[nb_points/2:-1,1,1],width),width,color='peachpuff',edgecolor='burlywood')#,width),width)
   plt.plot(xaxis,p[:,1,1]/playing_with_data.trap(p[:,1,1],width),linewidth=2.5, color='darkorange',label='$\kappa$='+str("%+02d" %kappa[1]))
   plt.plot(xaxis,p[:,3,1]/playing_with_data.trap(p[:,3,1],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   #plt.legend(prop={'size':14},bbox_to_anchor=(1.1, 1.05))
   '''

   ax = plt.subplot(gs[1, 0]) 
   tick_locs = [-5,1,10]
   tick_locs = [-5,0,5,10]
   tick_lbls = [r'5$^{o}$S',r'{\fontfamily{lmss}\selectfont EQ}',r'5$^{o}$N',r'10$^{o}$N']
   plt.xticks(tick_locs, tick_lbls,size=15)  
   plt.yticks([0,0.2])
   plt.ylim([0,0.34])
   plt.xlim([-10,15])
   plt.bar(xaxis,height_float[:,1,2]/playing_with_data.trap(height_float[nb_points/2:-1,1,2],width),width,color='#F0E68C',edgecolor='#FFD700')#,width),width)
   plt.plot(xaxis,p[:,1,2]/playing_with_data.trap(p[:,1,2],width),linewidth=2.5,color='red', label='$\kappa$='+str("%+02d" %kappa[2]))
   plt.plot(xaxis,p[:,3,2]/playing_with_data.trap(p[:,3,2],width),'--k',linewidth=0.75, label='$\kappa$='+str("%+02d" %kappa[3]))
   #plt.legend(prop={'size':14},bbox_to_anchor=(1.1, 1.05))
   
   plt.savefig('plots_final/fit_gaussian_at_'+str(t_day)+'_days_starting_at_'+str(lat_cross)+'1.pdf',format='pdf')

   
   nb_figure=nb_figure+1
   '''
   plt.figure(nb_figure)
   plt.plot(P)
   plt.savefig('plots_gaussian/KS_proba_'+str(lat_cross)+'_t'+str(t_day)+'days.png')
   nb_figure = nb_figure+1
   '''






