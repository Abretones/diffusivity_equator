import numpy as np

import looking_latitudes
import playing_with_data


def  diffusivity(drifters_array, nb_drifters, nb_data_per_drifters, nb_max, lat_cross, t_min, t_away_from_eq, t_asymptote, long_e_of_study, long_w_of_study  ):


   cross_drift_wrong_size, nb_cross = looking_latitudes.crossing_drifters(drifters_array[:,:,0], drifters_array[:,:,1], \
                                                                           nb_drifters, nb_data_per_drifters,            \
                                                                           nb_max, lat_cross, t_min, long_e_of_study,    \
   				                                           long_w_of_study)
   cross_drift = cross_drift_wrong_size[0:nb_cross, :, :]   


   print 'we are going to compute the diffusivity at '+str(lat_cross)+' degres thanks to ', nb_cross,                    \
           ' drifters that crossed this latitude and stayed in our domain for around ',int(t_min*6/24),' days'

   drift_north_pool, nb_north, drift_south_pool, nb_south = playing_with_data.seperate_drifters_at_eq( cross_drift,  \
                                                                                       nb_cross, t_min, t_away_from_eq )

   var_each_time = np.zeros((t_min))
   diff_each_time = np.zeros((t_min))
   
   if lat_cross == 0.:
        var_each_time_n = np.zeros((t_min))
        var_each_time_s = np.zeros((t_min))

        for i in range(1,t_min):
          var_each_time_n[i] = playing_with_data.variance( drift_north_pool[0:nb_north[i],i,0], drift_north_pool[0:nb_north[i],0,0], int(nb_north[i]) )
          var_each_time_s[i] = playing_with_data.variance( drift_south_pool[0:nb_south[i],i,0], drift_south_pool[0:nb_south[i],0,0], int(nb_south[i]) )
          var_each_time[i] = (var_each_time_n[i] + var_each_time_s[i])/2
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)

   elif lat_cross > 0.:

        for i in range(1,t_min):
          var_each_time[i] = playing_with_data.variance( drift_north_pool[0:nb_north[i],i,0], drift_north_pool[0:nb_north[i],0,0], int(nb_north[i]) )
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)
    
   elif lat_cross < 0.:

        for i in range(1,t_min):
          var_each_time[i] = playing_with_data.variance( drift_south_pool[0:nb_south[i],i,0], drift_south_pool[0:nb_south[i],0,0], int(nb_south[i]) )
          diff_each_time[i] = var_each_time[i]/(2*i*6*3600)

   return np.mean(diff_each_time[t_asymptote:t_min-1])
