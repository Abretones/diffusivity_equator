import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.switch_backend('agg')


from mpl_toolkits.basemap import Basemap
from matplotlib.ticker import NullFormatter,MultipleLocator, FormatStrFormatter
import matplotlib.gridspec as gridspec
import scipy 
import pylab as pl
from netCDF4 import Dataset
from scipy.interpolate import Rbf
import sys
import os
from matplotlib.lines import Line2D


#____________________________________________________________________
def fc(a,b,y,dy):
 f = np.zeros((200))
 coeff = (b-a)/(y[-1]-y[0])
 f[0]=a
 for i in range(1,200):
  f[i] = f[i-1]+coeff*dy
 return f

#____________________________________________________________________
def reading_distrib(lat_cross,n_index,factor,width,nb_points,nb_distrib):

   height_float = np.zeros((nb_points+1,nb_distrib))

   if width == 0.1:
      folder = '../master_project/plots_distrib/'
   elif width == 0.5:
      folder = '../master_project/plots_distrib05/'

   for j in range(0,nb_distrib):
   
     txt_file = str(folder)+'real_distribution_at_'+str(int(n_index[j]*6/(24*factor)))+'_days_starting_at_'+str(lat_cross)+'.txt'

     with open(txt_file, "r") as f:
       tab = []

       for line in range(0,nb_points+1):
         tab.append(f.readline().split())
 
       height = np.array([x[0] for x in tab])
     
       for i in range(0,nb_points+1):
         height_float[i,j] = str(height[i])

   return height_float






#____________________________________________________________________
def plotting_distrib_plus(p0_0,p0_4, y_p0, height_0,height_4,y, index_lat_max, width, kappa, velocity, index_t, dt, starting_lat, nb_figure,nb_test,KS_prob,day):

 
   fig=plt.subplots(1,2,figsize=(7,13))
   plt.rc('text', usetex=True)
   plt.rc('font', family='serif')


   plt.title('t='+str(index_t*dt)+' days',fontsize=10)

    
   plt.subplot(311)
   plt.xlim([-15,15])
   plt.yticks([0,0.1,0.2,0.3])
   pl.ylim([0,0.35])

   plt.bar(y, height_4/trap(height_4,width), width, color='#F0E68C',edgecolor='#FFD700')
   #plt.bar(y, height_0/trap(height_0,width), width, color='none',edgecolor='#FF8C00',hatch='//')
   plt.plot(y_p0,p0_4/trap(p0_4,1./200),'m' )
   #plt.plot(y_p0,p0_0/trap(p0_0,1./200),'--m' )

   #plt.legend(prop={'size':6},bbox_to_anchor=(1.1, 1.05))

   plt.subplot(312)
   tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
   tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
   plt.xticks(tick_locs, tick_lbls)
   plt.yticks([0,10000,20000,30000,40000])
   plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
   pl.xlim([-15,15]) 
   plt.plot(y_p0, kappa,'m')  

   plt.subplot(313)
   tick_locs = [10-1,20-1,30-1,40-1]
   tick_lbls = [r'10',r'20',r'30',r'40']
   plt.xticks(tick_locs, tick_lbls)
   plt.plot(KS_prob)
   plt.plot([day-1,day-1],[0,np.max(KS_prob)],'r',linewidth=2)  
   plt.xlabel(r'days')


   plt.subplots_adjust(top=0.95, bottom=0.05, left=0.15, right=0.9, hspace=0.2, wspace=0.7)
   plt.savefig('tests'+str(nb_test)+'/distrib_'+str(index_t*dt)+'_starting_at_'+str(starting_lat)+'_try'+str(nb_test)+'.pdf',format='pdf')




   nb_figure = nb_figure+2
   return nb_figure

#____________________________________________________________________
def trap(A,delx):

  n = len(A);

  v = .5*(A[0:n-1]+A[1:n])*delx
  yo = np.sum(v)

  return yo


#____________________________________________________________________
def adrhs(p,v,k,y,dy):
#
# RHS of the 1D Adv-Diff equation for absolute dispersion
#
  gp=np.gradient(p,dy)
  y=-np.gradient(v*p,dy) + np.gradient(k*gp,dy)

  return y

#____________________________________________________________________
def distribution_6_different_times(y, dy, ys, py, nb_points, nb_time, dt, factor, n_index, v, kappa,nb_distrib):
 p0 = np.zeros((nb_points))


 p0 = (1/(np.sqrt(2*np.pi)*py))*np.exp(-(((y-ys)/py)**2)/2) #initial PDF
 mean=np.zeros((nb_distrib))
 velocity=np.zeros((nb_distrib))

 p0_saved = np.zeros((nb_distrib,nb_points))
 p2 = np.zeros((nb_points))
 cnt =0
 
 for j in range(0,int(nb_time)):
   k1 = adrhs(p0,v,kappa,y,dy)
   k2 = adrhs(p0+k1*dt/2,v,kappa,y,dy)
   k3 = adrhs(p0+k2*dt/2,v,kappa,y,dy)
   k4 = adrhs(p0+k3*dt,v,kappa,y,dy)
   p0=p0+dt/6*(k1+2*k2+2*k3+k4)   

   if cnt <nb_distrib and j == int(n_index[cnt]-1):
     p0_saved[cnt,:] = np.copy(p0) #/trap(p2,dy)
     if cnt ==0:
        p2= np.copy(p0_saved[cnt,:])
        p2[np.where(y<0)]=0
        mean[cnt]=trap(y*p2,dy)/trap(p2,dy) 
        velocity[cnt]=(mean[cnt]-ys)*1E3/(24*3600)
     else:
        p2= np.copy(p0_saved[cnt,:])
        p2[np.where(y<0)]=0
        mean[cnt]=trap(y*p2,dy)/trap(p2,dy) 
        velocity[cnt]=(mean[cnt]-mean[cnt-1])*1E3/(24*3600)
     cnt=cnt+1
     print 'hey'
   



 return p0_saved, mean, velocity

#____________________________________________________________________

def KS_stat(height_float, p0_saved, n_index, dt , nb_points, M, y,y2, width):


   delta_N = np.zeros((M))
   F_p = np.zeros((M))
   diff = np.zeros((M))


   if width == 0.1:
    delta_N[0:19]=(height_float[0])/(trap(height_float[:],1.))
    for i in range(1,nb_points+1):
      delta_N[20*i:20*i+20]=delta_N[20*(i-1)]+(height_float[i])/trap(height_float,1.)

   elif width ==0.5:
    delta_N[0:99]=(height_float[0])/(trap(height_float[:],1.))
    for i in range(1,nb_points+1):
      delta_N[100*i:100*i+100]=delta_N[100*(i-1)]+(height_float[i])/trap(height_float,1.)



   F_p[0]=p0_saved[0]/trap(p0_saved,1.)
   diff[0] = abs(delta_N[0]-F_p[0])

   for i in range(1,M):
      F_p[i]=F_p[i-1]+p0_saved[i]/trap(p0_saved,1.)
      diff[i] = abs(delta_N[i]-F_p[i])
   '''
   plt.figure(nb_figure)
   plt.plot(y,F_p,y,delta_N)
   plt.savefig('plots_KS_test/diff_accumulative_distribution_fc_'+str(n_index*dt)+'days.png')
   nb_figure=nb_figure+1
   '''
   return np.max(diff)

#____________________________________________________________________
def Q_KS(Lambda):
  

  Q = 0

  
  for j in range(1,300):
 
       Q = Q + ((-1)**(j-1))*np.exp(-2*(j*Lambda)**2)


  return 2*Q


#//////////////////////////////////////////////////////////////////////////////////////
#/////////////////////////////////////////////////////////////////////////////////////
#____________________________________________________________________
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

def distrib_at_n( nb_test,starting_lat, index_snapshot, option_testing_different_diff, option_superimpose_real_distrib, lat_limit_south, width, nb_distrib,nb_figure):

 deg=110.567

 dy = 1./200        # grid spacing, in degres
 y_max = 20.
 y  = np.linspace(-y_max, y_max, 2*y_max/dy+1)
 print len(y)
 dy = dy*deg
 y_max = y_max*deg
 y = y*deg
 M = len(y)

 t0 = 0 					# starting time
 dt = 0.0001				# time step in day
 factor = 2500#150/(600*dt)
 N = 4*(nb_distrib+1)*factor#4*3*factor#501*factor			# total integration (N*dt)
 

 
 #////////////////////////////////////////////////////////////////////////////////////////////
 # #-------------------------------------------------------------------------------------------
 v = np.zeros((M))


 #Eulerian velocity

 
 #v=0.02*np.tanh(y/50)+0.08*np.exp(-((y-300)/400)**2)-0.08*np.exp(-((y+300)/400)**2)
 
 # fit velocity drogued and undrogued drifters
 
 v[0:int(M/2)-1] = -(0.01*pl.tanh((-y[0:int(M/2)-1]/deg-0.3)/0.2)+0.02*np.exp(-((-y[0:int(M/2)-1]/deg-1)/4)**2)-0.02*np.tanh((-y[0:int(M/2)-1]/deg-13)/12) +0.023*pl.tanh((-y[0:int(M/2)-1]/deg)/0.2) )
 

 v[int(M/2):M] = (0.03*pl.tanh(y[int(M/2):M]/(deg*0.5))+0.035*np.exp(-((y[int(M/2):M]/deg-3)/2.)**2)-0.001*np.exp(-((y[int(M/2):M]/deg+1.5)/1.5)**3) )*1.1+0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.2)- 0.08*pl.tanh((y[int(M/2):M]/deg-1.8)/0.21) +0.011*pl.tanh((y[int(M/2):M]/deg-9.5)/1.5) - 0.029*pl.tanh((y[int(M/2):M]/deg-14.5)/3.3)  -0.008 #- 0.04*pl.tanh((y[int(M/2)+1:M-1]/deg-1)/1.6)+0.04



 
 v=v*86.4 #km/day


 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 kappa = np.zeros((M,12))
 kappa_mean_at_1 = np.zeros((M))
 error_at_1 = np.zeros((M))
 kappa_mean_at_2 = np.zeros((M))
 error_at_2 = np.zeros((M))
 kappa_mean_at_3 = np.zeros((M))
 error_at_3 = np.zeros((M))
 kappa_mean_at_6 = np.zeros((M))
 error_at_6 = np.zeros((M))

 kappa_mean_all = np.zeros((M))
 error_all = np.zeros((M))
 nb_points_between_kappa_values = int(200)
 equator_index = int(M/2)

 # 1N - 1st
 kappa_values2=np.array([28802, 13915, 16145, 4007, 36590, 39953, 41472, 8854, 13916, 35681, 46754, 41669, 35904, 15853, 7301, 7301])
 kappa[0:equator_index-7*nb_points_between_kappa_values,0]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),0] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,0]=kappa_values2[-1]


 # 2N -1st
 kappa_values2=np.array([41046, 18210, 24097, 10664, 41423, 36475, 43742, 44652, 4741, 4637, 44440, 48635, 46008, 3379, 4312, 4312])
 kappa[0:equator_index-7*nb_points_between_kappa_values,1]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),1] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,1]=kappa_values2[-1]

 # 3N - 1st

 # 3N - 2nd
 kappa_values2=np.array([4069, 20130, 43032, 42915, 31256, 40104, 41974, 44726, 49430, 4215, 6768, 43488, 21587, 4111, 3777, 3777])
 kappa[0:equator_index-7*nb_points_between_kappa_values,2]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),2] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,2]=kappa_values2[-1]


 # 6N - 1st
 kappa_values2=np.array([12244, 17696, 41209, 36916, 34247, 40169, 9909, 3878, 3145, 4400, 20615, 12368, 17115, 4073, 4292, 4292])
 kappa[0:equator_index-7*nb_points_between_kappa_values,3]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),3] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,3]=kappa_values2[-1]



 # 1N - 2nd 
 kappa_values2=np.array([43375, 39822, 3748, 4339, 43554, 34148, 30281, 17588, 4131, 41939, 35845, 43728, 39170, 12306, 25364, 25364])
 kappa[0:equator_index-7*nb_points_between_kappa_values,4]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),4] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,4]=kappa_values2[-1]

 # 1N - 3rd 
 kappa_values2=np.array([23415, 14140, 7543, 20707, 16965, 45840, 21414, 18058, 4611, 44209, 49788, 31792, 40244, 12291, 3449, 3449])
 kappa[0:equator_index-7*nb_points_between_kappa_values,5]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),5] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,5]=kappa_values2[-1]


 # 2N - 2nd 
 kappa_values2=np.array([13570, 30550, 46505, 41929, 40723, 39569, 43808, 44702, 4468, 3641, 48999, 44032, 25480, 3181, 3060, 3060])
 kappa[0:equator_index-7*nb_points_between_kappa_values,6]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),6] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,6]=kappa_values2[-1]

 # 2N - 3rd 
 kappa_values2=np.array([26804, 41743, 44516, 33531, 42508, 33936, 5515, 33727, 3907, 4039, 40935, 47344, 28088, 4266, 5304, 5304])
 kappa[0:equator_index-7*nb_points_between_kappa_values,7]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),7] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,7]=kappa_values2[-1]

 # 3N - 3rd
 kappa_values2=np.array([25599, 9041, 30689, 21057, 44341, 30010, 31443, 40702, 42717, 6508, 5124, 37364, 22263, 4340, 5159, 5159])
 kappa[0:equator_index-7*nb_points_between_kappa_values,8]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),8] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,8]=kappa_values2[-1]


 # 3N - 4
 kappa_values2=np.array([40925, 21252, 13433, 37399, 29629, 36257, 43121, 38318, 44383, 5240, 3793, 44979, 15542, 8618, 12865, 12865])
 kappa[0:equator_index-7*nb_points_between_kappa_values,9]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),9] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,9]=kappa_values2[-1]



 # 6N - 2 exp 
 kappa_values2=np.array([45747, 32673, 43957, 27464, 23065, 28164, 4350, 4105, 4105, 3164, 21545, 34222, 8036, 8394, 3846, 3846])
 kappa[0:equator_index-7*nb_points_between_kappa_values,10]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),10] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,10]=kappa_values2[-1]

 # 6N - "3rd"
 kappa_values2=np.array([17897, 14171, 11925, 9148, 25361, 15401, 3347, 4326, 36054, 6491, 40168, 15730, 14184, 4958, 3664, 3664])
 kappa[0:equator_index-7*nb_points_between_kappa_values,11]=kappa_values2[0]
 for i in range(0,14):
   kappa[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7),11] = fc(kappa_values2[i],kappa_values2[i+1],y[equator_index+ nb_points_between_kappa_values*(i-7):equator_index+ nb_points_between_kappa_values*(i+1-7)],dy)
 
 kappa[equator_index+7*nb_points_between_kappa_values:M,11]=kappa_values2[-1]

 for i in range(0,M):
    kappa_mean_at_1[i] = (kappa[i,0]+kappa[i,5]+kappa[i,4])/3
    kappa_mean_at_2[i] = (kappa[i,1]+kappa[i,6]+kappa[i,7])/3
    kappa_mean_at_3[i] = (kappa[i,2]+kappa[i,8]+kappa[i,9])/3
    kappa_mean_at_6[i] = (kappa[i,3]+kappa[i,10]+kappa[i,11])/3
    kappa_mean_all[i] = (kappa[i,1]+kappa[i,6]+kappa[i,7]+kappa[i,0]+kappa[i,5]+kappa[i,4]+kappa[i,2]+kappa[i,8])/8
    #kappa_mean[i] = (kappa[i,0]+kappa[i,4])/2
    error_at_1[i] = np.sqrt((kappa[i,0]**2+kappa[i,5]**2+kappa[i,4]**2)/3-kappa_mean_at_1[i]**2)
    error_at_2[i] = np.sqrt((kappa[i,1]**2+kappa[i,6]**2+kappa[i,7]**2)/3-kappa_mean_at_2[i]**2)
    error_at_3[i] = np.sqrt((kappa[i,2]**2+kappa[i,8]**2+kappa[i,9]**2)/3-kappa_mean_at_3[i]**2)
    error_at_6[i] = np.sqrt((kappa[i,3]**2+kappa[i,10]**2+kappa[i,11]**2)/3-kappa_mean_at_6[i]**2)
    error_all[i] = np.sqrt((kappa[i,1]**2+kappa[i,6]**2+kappa[i,7]**2+kappa[i,0]**2+kappa[i,5]**2+kappa[i,4]**2+kappa[i,2]**2+kappa[i,8]**2)/8-kappa_mean_all[i]**2)
    #error = np.sqrt((kappa[i,0]**2+kappa[i,4]**2)/2-kappa_mean[i]**2


 plt.figure(nb_figure)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 ax = plt.subplot(111)
 plt.title(r'1$^{o}$N',size=17)
 tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
 tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls,size=15)
 plt.xlim([-12,12])
 plt.ylim([3500,4500])
 plt.yticks([0,10000,20000,30000,40000,50000])
 ax.fill_between(y/deg, kappa_mean_at_1+error_at_1, kappa_mean_at_1-error_at_1, color = 'lightgrey',label = 'standard deviation')
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 units = r'm$^{2}$.s$^{-1}$'
 plt.ylabel('Diffusivity ('+str(units)+')',size=14)
 ax.plot(y/deg,kappa_mean_at_1,linewidth=2)

 plt.savefig('tests1/kappa_with_1.pdf',format='pdf')
 nb_figure=nb_figure+1


 plt.figure(nb_figure)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 plt.title(r'2$^{o}$N',size=17)
 ax = plt.subplot(111)
 tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
 tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls,size=15)
 plt.xlim([-12,12])
 plt.ylim([3500,4500])
 plt.yticks([0,10000,20000,30000,40000,50000])
 ax.fill_between(y/deg, kappa_mean_at_2+error_at_2, kappa_mean_at_2-error_at_2, color = 'lightgrey',label = 'standard deviation')
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 units = r'm$^{2}$.s$^{-1}$'
 plt.ylabel('Diffusivity ('+str(units)+')',size=14)
 ax.plot(y/deg,kappa_mean_at_2,linewidth=2)

 plt.savefig('tests1/kappa_with_2.pdf',format='pdf')
 nb_figure=nb_figure+1

 plt.figure(nb_figure)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 ax = plt.subplot(111)
 plt.title(r'3$^{o}$N')
 tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
 tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls,size=15)
 plt.xlim([-12,12])
 plt.ylim([3500,4500])
 plt.yticks([0,10000,20000,30000,40000,50000])
 ax.fill_between(y/deg, kappa_mean_at_3+error_at_3, kappa_mean_at_3-error_at_3, color = 'lightgrey',label = 'standard deviation')
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 units = r'm$^{2}$.s$^{-1}$'
 plt.ylabel('Diffusivity ('+str(units)+')',size=14)
 ax.plot(y/deg,kappa_mean_at_3,linewidth=2)

 plt.savefig('tests1/kappa_with_3.pdf',format='pdf')
 nb_figure=nb_figure+1


 plt.figure(nb_figure)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 ax = plt.subplot(111)
 tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
 tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls,size=15)
 plt.xlim([-12,12])
 plt.ylim([3500,4500])
 plt.yticks([0,10000,20000,30000,40000,50000])
 ax.fill_between(y/deg, kappa_mean_at_6+error_at_6, kappa_mean_at_6-error_at_6, color = 'lightgrey',label = 'standard deviation')
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 units = r'm$^{2}$.s$^{-1}$'
 plt.ylabel('Diffusivity ('+str(units)+')',size=14)
 ax.plot(y/deg,kappa_mean_at_6,linewidth=2)

 plt.savefig('tests1/kappa_with_6.pdf',format='pdf')
 nb_figure=nb_figure+1


 plt.figure(nb_figure)
 plt.rc('text', usetex=True)
 plt.rc('font', family='serif')
 ax = plt.subplot(111)
 tick_locs = [-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
 tick_lbls = [r'$-10^{o}$S','','','','','','','','','',r'{\fontfamily{lmss}\selectfont EQ}','','','','','','','','','',r'$10^{o}$N']
 plt.xticks(tick_locs, tick_lbls)
 plt.xlim([-12,12])
 plt.ylim([3500,4500])
 plt.yticks([0,10000,20000,30000,40000,50000])
 ax.fill_between(y/deg, kappa_mean_all+error_all, kappa_mean_all-error_all, color = 'lightgrey',label = 'standard deviation')
 plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
 units = r'm$^{2}$.s$^{-1}$'
 plt.ylabel('Diffusivity ('+str(units)+')')
 ax.plot(y/deg,kappa_mean_all,linewidth=2)

 plt.savefig('tests1/kappa_all.png')
 nb_figure=nb_figure+1




 plt.figure(figsize=(15,5))
 plt.subplot(131)
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,0],'b')
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,1],'g')
 plt.subplot(132)
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,1],'g')
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,2],'r')
 plt.subplot(133)
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,2],'r')
 plt.plot(y[M/4:M*3/4]/deg,kappa[M/4:M*3/4,3],'k')
 plt.savefig('diffs_opti.png')


 kappa = kappa*0.0864

  


 #///////////////////////////////////////////////////////////////////////////////////////////
 #-------------------------------------------------------------------------------------------
 
 ys = starting_lat*deg                    # positon of initial PDF (degres)
 py = 0.2*deg				  # width of initial PDF


 #n_index = [4*5*factor, 4*15*factor, 4*30*factor, 4*50*factor, 4*80*factor, 4*125*factor]
 n_index=np.arange(4*factor,4*nb_distrib*factor+1,4*factor)
 print starting_lat
 nb_points = 2*int(-lat_limit_south/width)


 p0_saved = np.zeros((nb_distrib,M,4))
      
 lat_cross0=np.array([1.,2.,3.,6.])
 height_float = np.zeros((nb_points+1,nb_distrib,4))   
 height_float05 = np.zeros((nb_points/5+1,nb_distrib,4))
 xaxis = np.linspace( lat_limit_south, -lat_limit_south, nb_points+1 )
 xaxis2 = np.linspace( lat_limit_south, -lat_limit_south, nb_points/5+1 )
 D = np.zeros((nb_distrib,4))
 D_star = np.zeros((nb_distrib,4))
 P = np.zeros((nb_distrib,4))
 p0_in_bin = np.zeros((nb_points+1,nb_distrib,4))


 mean_n = np.zeros((nb_distrib,4))
 velocity_n = np.zeros((nb_distrib,4))

 for k in range(0,1):
   print lat_cross0[k]
   p0_saved[:,:,k],mean_n[:,k],velocity_n[:,k] = distribution_6_different_times(y,dy,lat_cross0[k]*deg,py,M,N,dt,factor, n_index,v,kappa[:,nb_test],nb_distrib)
   #p0_saved[:,:,1] = distribution_6_different_times(y,dy,-6.0*deg,py,M,N,dt,factor, n_index,v,kappa[:,nb_test],nb_distrib)  
          

   if lat_cross0[k]>0:     
       nb_floats = np.array(np.array([0,391,537,671,0,0,692,0,529,0,0,0,0,0,0,0,0,0]))
       
   else:
       nb_floats = np.array([0,374,409,487,0,0,489,0,0,0,0,0,0,0])

   Ne =nb_floats[int(abs(lat_cross0[k]))]

   height_float[:,:,k] = reading_distrib(lat_cross0[k],n_index,factor,width,nb_points,nb_distrib)
   height_float05[:,:,k] = reading_distrib(lat_cross0[k],n_index,factor,0.5,nb_points/5,nb_distrib)

 
   for j in range(0,nb_distrib):
     D[j,k] = KS_stat(height_float[:,j,k], p0_saved[j,:,k], n_index[j], dt, nb_points, M, y/deg,xaxis,width )
    
     P[j,k] = Q_KS((np.sqrt(Ne)+0.12+0.11/np.sqrt(Ne))*D[j,k])

    
     

 
   plt.figure(nb_figure)
   plt.plot(P[:,k])
   plt.savefig('tests'+str(nb_test)+'/KS_proba_'+str(lat_cross0[k])+'_try'+str(nb_test)+'.png')
   nb_figure = nb_figure+1


   nb_figure = plotting_distrib_plus(p0_saved[index_snapshot-1,:,0],p0_saved[index_snapshot-1,:,k], y/deg,  height_float05[:,index_snapshot-1,0],  height_float05[:,index_snapshot-1,k], xaxis2, -lat_limit_south, 0.5, kappa[:,nb_test]/0.0864, v/86.4, n_index[nb_distrib-1], dt, lat_cross0[k], nb_figure, nb_test,P[:,0], index_snapshot)
   


 '''
 print mean_n[0:5,:]
 print velocity_n[0:5,:]*1E2
 plt.figure(nb_figure)
 for i in range(0,4):
   plt.plot(mean_n[:,i]/deg,velocity_n[:,i]*1E2)
 plt.plot(y/deg,v)
 plt.savefig('lagrangian_velocity_model_'+str(lat_cross0[0])+'_try'+str(nb_test)+'.png')
 nb_figure=nb_figure+1
 '''
   
   
 return p0_saved*1E-3, nb_figure

nb_figure=1
width = 0.1
index_snapshot = 20
lat_cross0=np.array([1.,2.,3.,4.])

   
nb_test=np.array([0,1,2,3,4])
nb_diff_distrib = 30

for i in range(0,3):
   print nb_test[0]
   p0,nb_figure = distrib_at_n(nb_test[0],lat_cross0[i],index_snapshot, 0,0, -20, width, nb_diff_distrib,nb_figure)



