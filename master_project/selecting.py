import numpy as np

#----------------------------------------------------------------------------

def latitudes( Lat, Long, velocity,nb_drifters, nb_data_per_drifters, nb_max, lat_cross, t_min, long_e_of_study,            \
                      long_w_of_study, flag, month, month_min, month_max ):

  drift_cross=np.zeros((nb_drifters,t_min,3))

  a=0
  b=0
  cond=0
  index = 0

  for i in range(0,nb_drifters):
     
     while b<= nb_data_per_drifters[i]-1-t_min and cond==0:  # we want at least t_min values
         if flag[i,b]==1.0 and month[i,b]>=month_min:
          if (flag[i,b+t_min-1]==1.0 and month[i,b+t_min-1]<=month_max and Lat[i,b]>=lat_cross and Lat[i,b+1]<=lat_cross and Long[i,b]>long_w_of_study and Long[i,b]<long_e_of_study) or \
            (flag[i,b+t_min-1]==1.0 and month[i,b+t_min-1]<=month_max and Lat[i,b]<=lat_cross and Lat[i,b+1]>=lat_cross and Long[i,b]>long_w_of_study and Long[i,b]<long_e_of_study):

             if np.absolute(Lat[i,b+1]-lat_cross) < np.absolute(Lat[i,b]-lat_cross):
                index = b+1
             else :
                index = b
             for j in range(0,t_min):
                drift_cross[a,j,0] = Lat[i,index+j]
                drift_cross[a,j,1] = Long[i,index+j]
                drift_cross[a,j,2] = velocity[i,index+j]
             a=a+1
             cond=1        
         b=b+1             
     b=0
     cond=0

  nb_cross = a
  print 'we kept',nb_cross ,'drifters (the drogued drifters that crossed the latitude ', lat_cross,' between ',long_e_of_study, ' and ', long_e_of_study,')'

  return drift_cross, nb_cross


def inverse_latitudes( Lat, Long, nb_drifters, nb_data_per_drifters, nb_max, lat_cross, t_min, long_e_of_study,            \
                      long_w_of_study ):

  drift_cross=np.zeros((nb_drifters,t_min,2))

  a=0
  b=t_min
  cond=0
  index = 0
  for i in range(0,nb_drifters):

     while b<= nb_data_per_drifters[i]-2 and cond==0:  # we want at least t_min values
         if (Lat[i,b]>=lat_cross and Lat[i,b+1]<=lat_cross and Lat[i,b-t_min]>=0 and Long[i,b]>long_w_of_study and Long[i,b]<long_e_of_study) or \
            (Lat[i,b]<=lat_cross and Lat[i,b+1]>=lat_cross and Lat[i,b-t_min]>=0 and Long[i,b]>long_w_of_study and Long[i,b]<long_e_of_study):
             if np.absolute(Lat[i,b+1]-lat_cross) < np.absolute(Lat[i,b]-lat_cross):
                index = b+1
             else :
                index = b
             for j in range(0,t_min):
                drift_cross[a,j,0] = Lat[i,index-j]
                drift_cross[a,j,1] = Long[i,index-j]
             a=a+1
             cond=1            
         b=b+1             
     b=0
     cond=0

  nb_cross = a

  return drift_cross, nb_cross




def longitudes( Lat, Long, nb_drifters, nb_data_per_drifters, t_max, long_e_of_study, long_w_of_study ):


  new_array=np.zeros((nb_drifters,t_max,2))
  new_nb_data_per_drifter_ws = np.zeros((nb_drifters))
  a=0
  b=0
  i=0
  j = 0
  
  for a in range(0,nb_drifters):

     while b<= nb_data_per_drifters[i]-1:
         while Long[a,b]>long_w_of_study and Long[a,b]<long_e_of_study:
                new_array[i,j,0] = Lat[a,b]
                new_array[i,j,1] = Long[a,b]
                j = j+1
                b = b+1
                if Long[a,b+1]<long_w_of_study or Long[a,b+1]>long_e_of_study:
                   new_array[i,j,0] = Lat[a,b+1]
                   new_array[i,j,1] = Long[a,b+1]
                   j = j+1
                   b = b+1
         b = b+1
     if j>=2:
       new_nb_data_per_drifter_ws[i]=j
       i =i+1
     j = 0
         
  return new_array, new_nb_data_per_drifter_ws, i
