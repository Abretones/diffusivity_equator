import numpy as np
   
import selecting
import playing_with_data_north

def lagrangian_subset(Lat, Long, nb_drifters, nb_data_per_drifters, nb_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study, option):

   #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   # Array of the positions of drifters from the moment they cross 'lat_cross' until the 't_min'th time-steps
   # --> cross_drift[ nb_cross, t_min, 2 ] : [ number of drifters, number of time_step, latitude or longitude (in degres)]
   cross_drift_wrong_size, nb_cross = selecting.latitudes(Lat, Long, nb_drifters, nb_data_per_drifters,   \
                                                                           nb_max, lat_cross, t_min, long_e_of_study,   \
   				                                           long_w_of_study)
   cross_drift = cross_drift_wrong_size[0:nb_cross, :, :] 

   for i in range(0,t_min):     
       cross_drift[:,i,0]=cross_drift[:,i,0]-cross_drift[:,0,0]+lat_cross

   if option == 'same_group':
     drift_northward_pool, nb_n_pool_one_val = playing_with_data_north.seperate_drifters( cross_drift, nb_cross, t_min )
     nb_n_pool = np.zeros((t_min))
     nb_n_pool[:] = nb_n_pool_one_val

   #:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
   # Array of positions of drifters (minus initial positions) 'standing' at the North of the equator
   # --> drift_northward_pool[ nb_n_pool[:], t_min, 2 ] : [ number of drifters at time t, number of time-steps, 
   #                                                       latitude or longitude (in degres)]
   else :
     drift_northward_pool, nb_n_pool, drift_southward_pool, nb_s_pool = playing_with_data_north.seperate_drifters_at_eq( cross_drift,  \
                                                                                                          nb_cross, t_min )

   return drift_northward_pool, nb_n_pool
